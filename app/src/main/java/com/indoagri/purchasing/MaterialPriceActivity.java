package com.indoagri.purchasing;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.purchasing.Common.DateLibs;
import com.indoagri.purchasing.Dialog.ProgressDialogs;
import com.indoagri.purchasing.Fragment.SearchingMaterialize;
import com.indoagri.purchasing.Retrofit.Adapter.Top20MaterialPriceAdapter;
import com.indoagri.purchasing.Retrofit.ApiServices;
import com.indoagri.purchasing.Retrofit.DB.DatabaseQuery;
import com.indoagri.purchasing.Retrofit.MaterialResponse;
import com.indoagri.purchasing.Retrofit.Model.MaterialPrice;
import com.indoagri.purchasing.Retrofit.NetClient;
import com.indoagri.purchasing.Retrofit.SharePreference;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaterialPriceActivity extends AppCompatActivity implements SearchingMaterialize.OnFragmentInteractionListener,SearchingMaterialize.OnAreaListener,
SearchingMaterialize.OnGroupListener,SearchingMaterialize.OnYearListener{
    int months = Calendar.getInstance().get(Calendar.MONTH);
    int years = Calendar.getInstance().get(Calendar.YEAR);
    private static final String TAGACTIVITY = "GET DATA MATERIAL";
    SharePreference sharePreference;
    Toolbar toolbar;
    TextView mTextToolbar;
    DatabaseQuery query;
    RecyclerView recyclerView;
    MaterialResponse materialResponse;
    ArrayList<MaterialPrice> topRankModelList = new ArrayList<MaterialPrice>();
    MaterialPrice materialPrice;
    Top20MaterialPriceAdapter adapter;
    ApiServices apiServices;
    ImageView imgReload, imgSort, imgSearch;
    TextView ReloadData;
    boolean sort = true;
    LinearLayoutManager layoutManager;
    DividerItemDecoration dividerItemDecoration;
    String QueryString = null;
    FrameLayout frame_display, searchFragment;
    boolean ShowSearch = false;
    TableLayout tableLayout,tableLayoutfooter;
    TextView txtInfoArea,txtInfoGroupReport,txtYear;
    public FragmentRefreshListener getFragmentRefreshListener() {
        return fragmentRefreshListener;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener fragmentRefreshListener) {
        this.fragmentRefreshListener = fragmentRefreshListener;
    }
    private FragmentRefreshListener fragmentRefreshListener;
    public static int AREA = 11;
    public static int MATGROUP = 22;
    public static int YEAR = 44;
    ProgressDialogs Progressalert;
    Dialog Pushdialog;
    List<Object> LASTORDER;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_price);
        Pushdialog = new Dialog(MaterialPriceActivity.this);
        Progressalert= new ProgressDialogs();
        Progressalert.ProgressDialogs(Pushdialog,MaterialPriceActivity.this,"Please Wait");
        query = new DatabaseQuery(MaterialPriceActivity.this);
        frame_display = (findViewById(R.id.frame_view_display));
        searchFragment = (findViewById(R.id.fragment_container));
        FloatingActionButton fab = findViewById(R.id.fab);
        recyclerView = (findViewById(R.id.recycler_view));
        recyclerView.setHasFixedSize(true);
        imgReload = (findViewById(R.id.imgReload));
        imgSort = (findViewById(R.id.imgSort));
        ReloadData = (findViewById(R.id.Reload));
        ReloadData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toList();
                showYearDialogReload();
            }
        });
        imgSort.setVisibility(View.GONE);
        imgSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(MaterialPriceActivity.this, "Sortir", Toast.LENGTH_SHORT).show();
            }
        });
        imgSearch = (findViewById(R.id.imgSearch));
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ShowSearch){
                    toList();
                }else{
                    toSearch();
                }
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        apiServices = NetClient.DataProduction().create(ApiServices.class);
        tableLayout = (TableLayout) findViewById(R.id.main_table);
        tableLayoutfooter = (TableLayout) findViewById(R.id.main_tableFooter);
        tableLayout.bringToFront();
        tableLayout.setStretchAllColumns(true);
        setUpToolbar();
        initActivity();
    }

    private void initActivity() {
        String year = DateLibs.getTahunIni();
        if (deletetable(year)) {
            GetMaterialPrice(year);
        }
    }

    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText("Material Top of 20");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                if(ShowSearch){
                    toList();
                }else{
                    new SharePreference(MaterialPriceActivity.this).setFormAreaMaterialPrice("");
                    new SharePreference(MaterialPriceActivity.this).setFormMatgroup("");
                    new SharePreference(MaterialPriceActivity.this).setFormYear("");

                    onBackPressed();
                }
            }
        });
    }

    private boolean deletetable(String year) {
        Progressalert.showDialog();
        boolean result = false;
        query.openTransaction();
        int rowID = 0;
        int rowID2 = 0;
        try {
            String[] ys = new String[1];
            ys[0] = year;
            query.deleteDataTemporary(MaterialPrice.TABLE_NAME, "substr(LSTPRD,1,4) =?", ys);
//            if (rowID > 0 || rowID == 0) {
//                query.deleteDataTable(MaterialPrice.TABLE_NAME,"substr(LSTPRD,1,4) =?", ys);
//            }
        } finally {
           Progressalert.showDialog();
            query.commitTransaction();
            query.closeTransaction();
            result = true;
        }
        return result;
    }


    public void toSearch(){
        searchFragment.setVisibility(View.VISIBLE);
        frame_display.setVisibility(View.GONE);
        Fragment fragment = new SearchingMaterialize();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
        ShowSearch = true;
    }
    public void toList(){
        searchFragment.setVisibility(View.GONE);
        frame_display.setVisibility(View.VISIBLE);
        ShowSearch = false;
    }


    // GET MATERIAL PRICE //

    private void GetMaterialPrice(String year) {
       Progressalert.showDialog();
        Log.d(TAGACTIVITY, "LOADPAGE: ");

        GetMaterialPrices(year).enqueue(new Callback<MaterialResponse>() {
            @Override
            public void onResponse(Call<MaterialResponse> call, Response<MaterialResponse> response) {
                // Got data. Send it to adapter
                Progressalert.dismissDialog();
                if (response.code() == 200) {
                    materialResponse = fetchMaterialPrices(response);
                    topRankModelList = materialResponse.getMaterialPrice();
                    insertMaterials();
                } else {
                    Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MaterialResponse> call, Throwable t) {
                t.printStackTrace();
                Progressalert.dismissDialog();
                // TODO: 08/11/16 handle failure
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private Call<MaterialResponse> GetMaterialPrices(String year) {
        return apiServices.GETDATAMATPRC(year);
    }

    private MaterialResponse fetchMaterialPrices(Response<MaterialResponse> response) {
        MaterialResponse materialResponse = response.body();
        return materialResponse;
    }

    private void insertMaterials() {
        query.openTransaction();
        for (int i = 0; i < topRankModelList.size(); i++) {
            MaterialPrice materialModel = topRankModelList.get(i);
            ContentValues values = new ContentValues();
            values.put(MaterialPrice.XML_INSERTDATE, materialModel.getInsertdate());
            values.put(MaterialPrice.XML_MATGRP, materialModel.getMatgrp());
            values.put(MaterialPrice.XML_AREA, materialModel.getArea());
            values.put(MaterialPrice.XML_MATNR, materialModel.getMatnr());
            values.put(MaterialPrice.XML_MANDT, materialModel.getMandt());
            values.put(MaterialPrice.XML_MATGRPDESC, materialModel.getMatgrpdesc());
            values.put(MaterialPrice.XML_LSTPRD, materialModel.getLstprd());
            values.put(MaterialPrice.XML_TXZ01, materialModel.getTxZ01());
            values.put(MaterialPrice.XML_LSTPO, materialModel.getLstpo());
            values.put(MaterialPrice.XML_LSTPOITEM, materialModel.getLstpoitem());
            values.put(MaterialPrice.XML_WAERS, materialModel.getWaers());
            values.put(MaterialPrice.XML_LSTUOM, materialModel.getLstuom());
            if(materialModel.getLstprc()==null){
                values.put(MaterialPrice.XML_LSTPRC, "0");
            }
            if(materialModel.getLstprc()!=null){
                values.put(MaterialPrice.XML_LSTPRC, materialModel.getLstprc());
            }

            if(materialModel.getTotval()==null){
                values.put(MaterialPrice.XML_TOTVAL, "0");
            }
            if(materialModel.getTotval()!=null){
                values.put(MaterialPrice.XML_TOTVAL, materialModel.getTotval());
            }
            query.insertDataSQL(MaterialPrice.TABLE_NAME, values);
        }
        query.commitTransaction();
        query.closeTransaction();
        //GetDataTopRank();
        toSearch();
    }


    // END GET MATERIAL PRICE //

    void InitForm(){
        txtInfoArea = findViewById(R.id.txtArea);
        txtInfoGroupReport = findViewById(R.id.txtGroupReport);
        txtYear = findViewById(R.id.txtYear);
        if(new SharePreference(MaterialPriceActivity.this).isMaterialGroup().equalsIgnoreCase("ALL")) {
            txtInfoGroupReport.setText("ALL");
        }else{
            txtInfoGroupReport.setText(new SharePreference(MaterialPriceActivity.this).isMaterialGroup());
        }
        if(new SharePreference(MaterialPriceActivity.this).isFormArea().equalsIgnoreCase("ALL")) {
            txtInfoArea.setText("ALL");
        }else{
            txtInfoArea.setText(new SharePreference(MaterialPriceActivity.this).isFormArea());

        }
        if(new SharePreference(MaterialPriceActivity.this).isFormYear().equalsIgnoreCase("ALL")) {
            txtYear.setText("ALL");
        }else{
            txtYear.setText(new SharePreference(MaterialPriceActivity.this).isFormYear());

        }
    }
    // INIT FIRST LIST //
    private void INITVIEWFIRST() {
        new SharePreference(MaterialPriceActivity.this).setFormAreaMaterialPrice("");
        new SharePreference(MaterialPriceActivity.this).setFormMatgroup("");
        query.openTransaction();
        if (topRankModelList.size() > 0 || topRankModelList != null) {
            topRankModelList.clear();
        }

        String Year = DateLibs.getTahunIni();
        String[] aa = new String[1];
        aa[0] = Year;
        List<Object> listObject;
        String sqldb_query2 = "select * from MATERIAL_PRICE where SUBSTR(LSTPRD,1,4) = ?  order by LSTPRD DESC limit 0,1";
        LASTORDER = query.getListDataRawQuery(sqldb_query2, MaterialPrice.TABLE_NAME, aa);
        MaterialPrice materialPriceLast = (MaterialPrice)LASTORDER.get(0);
        String[] ab = new String[1];
        ab[0] = materialPriceLast.getLstprd();
        String sqldb_query ="SELECT * " +
                "FROM  MATERIAL_PRICE " +
                "WHERE LSTPRD IN " +
                "    ( " +
                "select LSTPRD from MATERIAL_PRICE where LSTPRD= ? GROUP BY LSTPRD ORDER BY LSTPRD DESC LIMIT 1 " +
                "    ) " +
                " GROUP BY TXZ01 " +
                " Order by CAST(totval AS INTEGER) desc LIMIT 20";
        listObject = query.getListDataRawQuery(sqldb_query, MaterialPrice.TABLE_NAME, ab);
        query.closeTransaction();
        List<MaterialPrice> listTemp = new ArrayList<MaterialPrice>();
        if(listObject==null){
            // tableLayout.removeAllViews();
        }
        else{
            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    MaterialPrice materialPrice= (MaterialPrice) listObject.get(i);
                    listTemp.add(materialPrice);
                }
                //startLoadData();
                AsyncDetail asyncDetail = new AsyncDetail(listObject,MaterialPriceActivity.this);
                asyncDetail.execute();
            }else{
                tableLayout.removeAllViews();
                tableLayoutfooter.removeAllViews();
            }
        }

        InitForm();
        toSearch();
    }

    private void INITVIEWWITHAREA() {
        query.openTransaction();
        if (topRankModelList.size() > 0 || topRankModelList != null) {
            topRankModelList.clear();
        }
        String Area = new SharePreference(MaterialPriceActivity.this).isFormArea();
        String Year = new SharePreference(MaterialPriceActivity.this).isFormYear();
        String[] aa = new String[2];
        aa[0] = Area;
        aa[1] = Year;
        List<Object> listObject;
        String sqldb_query2 = "select * from MATERIAL_PRICE where AREA=? AND SUBSTR(LSTPRD,1,4) = ?  order by LSTPRD DESC limit 0,1";
        LASTORDER = query.getListDataRawQuery(sqldb_query2, MaterialPrice.TABLE_NAME, aa);
        MaterialPrice materialPriceLast = (MaterialPrice)LASTORDER.get(0);
        String[] ab = new String[2];
        ab[0] = Area;
        ab[1] = materialPriceLast.getLstprd();
        String sqldb_query ="SELECT * " +
                "FROM  MATERIAL_PRICE " +
                "WHERE AREA=? AND LSTPRD IN " +
                "    ( " +
                "select LSTPRD from MATERIAL_PRICE where LSTPRD = ? GROUP BY LSTPRD ORDER BY LSTPRD DESC LIMIT 1 " +
                "    ) " +
                " GROUP BY TXZ01 " +
                " Order by CAST(totval AS INTEGER) desc LIMIT 20";
            listObject = query.getListDataRawQuery(sqldb_query, MaterialPrice.TABLE_NAME, ab);
        query.closeTransaction();
        List<MaterialPrice> listTemp = new ArrayList<MaterialPrice>();
        if(listObject==null){
            // tableLayout.removeAllViews();
        }
        else{
            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    MaterialPrice materialPrice= (MaterialPrice) listObject.get(i);
                    listTemp.add(materialPrice);
                }
                //startLoadData();
                AsyncDetail asyncDetail = new AsyncDetail(listObject,MaterialPriceActivity.this);
                asyncDetail.execute();
            }else{
                tableLayout.removeAllViews();
                tableLayoutfooter.removeAllViews();
            }
        }
        InitForm();
    }

    private void INITVIEWWITHAREAANDGROUP() {
        query.openTransaction();
        if (topRankModelList.size() > 0 || topRankModelList != null) {
            topRankModelList.clear();
        }
        String Area = new SharePreference(MaterialPriceActivity.this).isFormArea();
        String Group = new SharePreference(MaterialPriceActivity.this).isMaterialGroup();
        String Year = new SharePreference(MaterialPriceActivity.this).isFormYear();
        String[] aa = new String[3];
        aa[0] = Group;
        aa[1] = Area;
        aa[2] = Year;
        List<Object> listObject;
        String sqldb_query2 = "select * from MATERIAL_PRICE where  MATGRP=? AND AREA=? AND SUBSTR(LSTPRD,1,4) = ?  order by LSTPRD DESC limit 0,1";
        LASTORDER = query.getListDataRawQuery(sqldb_query2, MaterialPrice.TABLE_NAME, aa);
        MaterialPrice materialPriceLast = (MaterialPrice)LASTORDER.get(0);
        String[] ab = new String[3];
        ab[0] = Group;
        ab[1] = Area;
        ab[2] = materialPriceLast.getLstprd();
        String sqldb_query ="SELECT * " +
                "FROM  MATERIAL_PRICE " +
                "WHERE  MATGRP=? AND AREA=? AND LSTPRD IN " +
                "    ( " +
                "select LSTPRD from MATERIAL_PRICE where LSTPRD = ? GROUP BY LSTPRD ORDER BY LSTPRD DESC LIMIT 1 " +
                "    ) " +
                " GROUP BY TXZ01 " +
                " Order by CAST(totval AS INTEGER) desc LIMIT 0,20";
            listObject = query.getListDataRawQuery(sqldb_query, MaterialPrice.TABLE_NAME, ab);

        query.closeTransaction();
        List<MaterialPrice> listTemp = new ArrayList<MaterialPrice>();
        if(listObject==null){
            // tableLayout.removeAllViews();
        }
        else{
            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    MaterialPrice materialPrice= (MaterialPrice) listObject.get(i);
                    listTemp.add(materialPrice);
                }
                //startLoadData();
                AsyncDetail asyncDetail = new AsyncDetail(listObject,MaterialPriceActivity.this);
                asyncDetail.execute();
            }else{
                tableLayout.removeAllViews();
                tableLayoutfooter.removeAllViews();
            }
        }
        InitForm();
    }

    private void INITVIEWWITHGROUP() {
        query.openTransaction();
        if (topRankModelList.size() > 0 || topRankModelList != null) {
            topRankModelList.clear();
        }
        String Group = new SharePreference(MaterialPriceActivity.this).isMaterialGroup();
        String Year = new SharePreference(MaterialPriceActivity.this).isFormYear();
        String[] aa = new String[2];
        aa[0] = Group;
        aa[1] = Year;
        List<Object> listObject;
        String sqldb_query ="SELECT * " +
                "FROM  MATERIAL_PRICE " +
                "WHERE  MATGRP=? AND LSTPRD IN " +
                "    ( " +
                "select LSTPRD from MATERIAL_PRICE where substr(LSTPRD,1,4) = ? GROUP BY LSTPRD ORDER BY LSTPRD DESC LIMIT 1 " +
                "    ) " +
                " GROUP BY TXZ01 " +
                " Order by CAST(totval AS INTEGER) desc";
        listObject = query.getListDataRawQuery(sqldb_query, MaterialPrice.TABLE_NAME, aa);
        query.closeTransaction();
        List<MaterialPrice> listTemp = new ArrayList<MaterialPrice>();
        if(listObject==null){
            // tableLayout.removeAllViews();
        }
        else{
            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    MaterialPrice materialPrice= (MaterialPrice) listObject.get(i);
                    listTemp.add(materialPrice);
                }
                //startLoadData();
                AsyncDetail asyncDetail = new AsyncDetail(listObject,MaterialPriceActivity.this);
                asyncDetail.execute();
            }else{
                tableLayout.removeAllViews();
                tableLayoutfooter.removeAllViews();
            }
        }
        InitForm();
    }

    @Override
    public void onFragmentInteraction() {
        if(new SharePreference(MaterialPriceActivity.this).isFormArea().equalsIgnoreCase("ALL") &&
                new SharePreference(MaterialPriceActivity.this).isMaterialGroup().equalsIgnoreCase("ALL")){
            INITVIEWFIRST();
           // Toast.makeText(this, "1", Toast.LENGTH_SHORT).show();
        }if(!new SharePreference(MaterialPriceActivity.this).isFormArea().equalsIgnoreCase("ALL") &&
                new SharePreference(MaterialPriceActivity.this).isMaterialGroup().equalsIgnoreCase("ALL")){
            INITVIEWWITHAREA();
          //  Toast.makeText(this, "2", Toast.LENGTH_SHORT).show();
        }if(!new SharePreference(MaterialPriceActivity.this).isFormArea().equalsIgnoreCase("ALL") &&
                !new SharePreference(MaterialPriceActivity.this).isMaterialGroup().equalsIgnoreCase("ALL")){
            INITVIEWWITHAREAANDGROUP();
      //      Toast.makeText(this, "3", Toast.LENGTH_SHORT).show();
        }if(new SharePreference(MaterialPriceActivity.this).isFormArea().equalsIgnoreCase("ALL") &&
                !new SharePreference(MaterialPriceActivity.this).isMaterialGroup().equalsIgnoreCase("ALL")){
            INITVIEWWITHGROUP();
            //      Toast.makeText(this, "3", Toast.LENGTH_SHORT).show();
        }
        frame_display.setVisibility(View.VISIBLE);
        searchFragment.setVisibility(View.GONE);
        ShowSearch = false;
    }
    @Override
    public void onFragmentInteractionArea() {
        Intent newIntent = new Intent(MaterialPriceActivity.this,ListAreaActivity.class);
        newIntent.putExtra("AREA","AREA");
        startActivityForResult(newIntent, AREA);
    }

    @Override
    public void onFragmentInteractionGroup() {
        Intent newIntent = new Intent(MaterialPriceActivity.this,ListMatGroupActivity.class);
        newIntent.putExtra("GROUP","GROUP");
        startActivityForResult(newIntent, MATGROUP);
     //   Toast.makeText(this, "TES MAT GROUP", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFragmentInteractionYear() {
        query.openTransaction();
        List<Object> listObject;
        ArrayList<MaterialPrice>sendYear = new ArrayList<MaterialPrice>();
        String sqldb_query ="SELECT * FROM  MATERIAL_PRICE GROUP BY substr(LSTPRD,1,4)";
        listObject = query.getListDataRawQuery(sqldb_query, MaterialPrice.TABLE_NAME, null);
        query.closeTransaction();
                for(int i = 0; i < listObject.size(); i++){
                    MaterialPrice materialPrice= (MaterialPrice) listObject.get(i);
                    sendYear.add(materialPrice);
                }
        Intent newIntent = new Intent(MaterialPriceActivity.this,ListYearActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("table", MaterialPrice.TABLE_NAME);
        bundle.putParcelableArrayList("data", sendYear);
        newIntent.putExtras(bundle);
        startActivityForResult(newIntent, YEAR);
    }
    public void showYearDialog() {

        final Dialog d = new Dialog(MaterialPriceActivity.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_year);
        Button set = (Button) d.findViewById(R.id.btnOk);
        Button cancel = (Button) d.findViewById(R.id.btnCancel);
        TextView year_text = (TextView) d.findViewById(R.id.year_text);
        year_text.setText("Set Year");
        final NumberPicker yearpicker = (NumberPicker) d.findViewById(R.id.picker_year);

        yearpicker.setMinValue(2016);
        yearpicker.setMaxValue(2025);
        yearpicker.setWrapSelectorWheel(false);
        yearpicker.setValue(years);

        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Years = String.valueOf(yearpicker.getValue());
                new SharePreference(MaterialPriceActivity.this).setFormYear(Years);
                d.dismiss();
                if(getFragmentRefreshListener()!=null){
                    getFragmentRefreshListener().onRefresh();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    public interface FragmentRefreshListener{
        void onRefresh();
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == AREA) {
            if(resultCode == RESULT_OK) {
                String CODE=data.getStringExtra("CODE");
                String DESC=data.getStringExtra("DESC");
                if(CODE==null){

                }else{
                    new SharePreference(MaterialPriceActivity.this).setFormAreaMaterialPrice(CODE);
                    if(getFragmentRefreshListener()!=null){
                        getFragmentRefreshListener().onRefresh();
                    }
                }
            }
        }
        if (requestCode == MATGROUP) {
            if(resultCode == RESULT_OK) {
                String CODE=data.getStringExtra("CODE");
                String DESC=data.getStringExtra("DESC");
                if(CODE==null){

                }else{
                    new SharePreference(MaterialPriceActivity.this).setFormMatgroup(CODE);
                    if(getFragmentRefreshListener()!=null){
                        getFragmentRefreshListener().onRefresh();
                    }
                }
            }
        }
        if (requestCode == YEAR) {
            if(resultCode == RESULT_OK) {
                String CODE=data.getStringExtra("CODE");
                String DESC=data.getStringExtra("DESC");
                if(CODE==null){

                }else{
                    new SharePreference(MaterialPriceActivity.this).setFormYear(CODE);
                    if(getFragmentRefreshListener()!=null){
                        getFragmentRefreshListener().onRefresh();
                    }
                }
            }
        }
    }

    private class AsyncDetail extends AsyncTask<Void, Integer, Void> {
        List<Object> objectList;
        MaterialPrice materialPrice;
        public AsyncDetail(List<Object> objects, Context context) {

            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            Progressalert.showDialog();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void result) {
            Progressalert.dismissDialog();
            loadData(objectList);
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 0; i<objectList.size(); i++) {
                publishProgress(i);
                materialPrice = (MaterialPrice) objectList.get(i);
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }
    }

    public void loadData(List<Object> objectList) {

        int leftRowMargin=0;
        int topRowMargin=0;
        int rightRowMargin=0;
        int bottomRowMargin = 0;
        int textSize = 0, smallTextSize =0, mediumTextSize = 0;

        textSize = (int) getResources().getDimension(R.dimen.font_size_medium);
        smallTextSize = (int) getResources().getDimension(R.dimen.font_size_normal);
        mediumTextSize = (int) getResources().getDimension(R.dimen.font_size_medium);

        int totalrows = objectList.size();
        int rows = objectList.size();
        int rowsIden = 0;
        getSupportActionBar().setTitle("Price (" + String.valueOf(rows) + ")");
        TextView textSpacer = null;

        tableLayout.removeAllViews();

        NumberFormat PriceFormat = new DecimalFormat("#,###,###,###,###,###.##");
        double priceTotalValue = 0;
        double  priceFormat = 0 ;

        String DataTotalValueFormattedNumber = null;
        String DatapriceFormattedNumber = null;
        String Datamaterial = null;
        String DataDescription = null;
        String DataUOM = null;
        String DataCurrency = null;
        String DataTotalValue = null;
        // -1 means heading row
        for (rowsIden= -1; rowsIden < objectList.size(); rowsIden++) {
            if (rowsIden > -1) {
                materialPrice = (MaterialPrice) objectList.get(rowsIden);

                if(materialPrice.getLstprc()== null || materialPrice.getLstprc().equals(null) || materialPrice.getLstprc().isEmpty()){
                    priceFormat= 0;
                    DatapriceFormattedNumber = PriceFormat.format(priceFormat);

                }else{
                    priceFormat= Double.parseDouble(materialPrice.getLstprc());
                    DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                }
                if(materialPrice.getTotval()== null || materialPrice.getTotval().equals(null) || materialPrice.getTotval().isEmpty()){
                    priceTotalValue= 0;
                    DataTotalValueFormattedNumber = PriceFormat.format(priceTotalValue);

                }else{
                    priceTotalValue= Double.parseDouble(materialPrice.getTotval())/1000000;
                    DataTotalValueFormattedNumber = PriceFormat.format(priceTotalValue);
                }

                if (materialPrice.getTxZ01().length() >= 50) {
                    DataDescription = materialPrice.getTxZ01().substring(0, 40)+ "...";
                } else {
                    DataDescription = materialPrice.getTxZ01();
                }
                Datamaterial = materialPrice.getMatnr().substring(10);
                DataUOM = materialPrice.getLstuom();
                DataCurrency = materialPrice.getWaers();



            }else {
                textSpacer = new TextView(this);
                textSpacer.setText("");
            }

            final TextView tvPeriode = new TextView(this);
            if (rowsIden == -1) {
                tvPeriode.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                tvPeriode.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            } else {
                tvPeriode.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvPeriode.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }

            tvPeriode.setGravity(Gravity.LEFT);

            tvPeriode.setPadding(15, 15, 0, 15);
            if (rowsIden == -1) {
                tvPeriode.setText("Description");
                tvPeriode.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvPeriode.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            }else {
                tvPeriode.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                tvPeriode.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvPeriode.setText(DataDescription);
            }
            final LinearLayout layQty = new LinearLayout(this);
            layQty.setOrientation(LinearLayout.VERTICAL);
            layQty.setGravity(Gravity.RIGHT);
            layQty.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            final TextView tvQty = new TextView(this);
            tvQty.setPadding(5, 15, 15, 15);
            if (rowsIden == -1) {
                tvQty.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvQty.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            }
            else {
                tvQty.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvQty.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }
            tvQty.setGravity(Gravity.RIGHT);
            if (rowsIden == -1) {
                tvQty.setText("Material");
                tvQty.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvQty.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            }
            else {
                tvQty.setBackgroundColor(getResources().getColor(R.color.colorWhiteinit));
                tvQty.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvQty.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
                tvQty.setText(Datamaterial);
            }
            layQty.addView(tvQty);



            final LinearLayout layValue = new LinearLayout(this);
            layValue.setOrientation(LinearLayout.VERTICAL);
            layValue.setGravity(Gravity.RIGHT);
            layValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));



            final TextView tvValue = new TextView(this);
            tvValue.setPadding(5, 15, 15, 15);
            if (rowsIden == -1) {
                tvValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                layValue.setBackgroundColor(Color.parseColor("#f7f7f7"));
            } else {
                tvValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                layValue.setBackgroundColor(Color.parseColor("#ffffff"));
            }
            tvValue.setGravity(Gravity.RIGHT);
            if (rowsIden == -1) {
                tvValue.setText("UOM");
                tvValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                tvValue.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvValue.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            } else {
                tvValue.setBackgroundColor(getResources().getColor(R.color.colorWhiteinit));
                tvValue.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvValue.setText(DataUOM);
                tvValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }

            layValue.addView(tvValue);

            final LinearLayout layPrices = new LinearLayout(this);
            layPrices.setOrientation(LinearLayout.VERTICAL);
            layPrices.setGravity(Gravity.RIGHT);
            layPrices.setBackgroundColor(Color.parseColor("#f8f8f8"));

            final TextView tvPrice = new TextView(this);

            tvPrice.setPadding(5, 15, 15, 15);
            if (rowsIden == -1) {
                tvPrice.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

            } else {
                tvPrice.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);

            }

            tvPrice.setGravity(Gravity.RIGHT);

            if (rowsIden == -1) {
                tvPrice.setText("LastPrice");
                tvPrice.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvPrice.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            } else {
                tvPrice.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                tvPrice.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvPrice.setText(DataCurrency+" "+DatapriceFormattedNumber);
                tvPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }
            layPrices.addView(tvPrice);

            final LinearLayout layTotalValue = new LinearLayout(this);
            layTotalValue.setOrientation(LinearLayout.VERTICAL);
            layTotalValue.setGravity(Gravity.RIGHT);
            layTotalValue.setBackgroundColor(Color.parseColor("#f8f8f8"));

            final TextView tvTotalValue = new TextView(this);

            tvTotalValue.setPadding(5, 15, 15, 15);
            if (rowsIden == -1) {
                tvTotalValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvTotalValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            } else {
                tvTotalValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvTotalValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }

            tvTotalValue.setGravity(Gravity.RIGHT);

            if (rowsIden == -1) {
                tvTotalValue.setText("Total Value (IDR Mio)");
                tvTotalValue.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvTotalValue.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            } else {
                tvTotalValue.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                tvTotalValue.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvTotalValue.setText(DataTotalValueFormattedNumber);
                tvTotalValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }
            layTotalValue.addView(tvTotalValue);

            // add table row
            final TableRow tr = new TableRow(this);
            tr.setId(rowsIden + 1);
            TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
            tr.setPadding(0,0,0,0);
            tr.setLayoutParams(trParams);

            //tr.addView(tv);
            tr.addView(tvPeriode);
            tr.addView(layQty);
            tr.addView(layValue);
            tr.addView(layPrices);
            tr.addView(layTotalValue);
            if (rowsIden > -1) {
                tr.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        TableRow tr = (TableRow) v;
                        //do whatever action is needed
                        //   Toast.makeText(SalesBilling.this,"Test Toast",Toast.LENGTH_SHORT).show();
                    }
                });
            }
            tableLayout.addView(tr, trParams);
            if (rowsIden > -1) {
                // add separator row
                final TableRow trSep = new TableRow(this);
                TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                        TableLayout.LayoutParams.WRAP_CONTENT);
                trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

                trSep.setLayoutParams(trParamsSep);
                TextView tvSep = new TextView(this);
                TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT);
                tvSepLay.span = 4;
                tvSep.setLayoutParams(tvSepLay);
                tvSep.setBackgroundColor(Color.parseColor("#d9d9d9"));
                tvSep.setHeight(1);
                trSep.addView(tvSep);
                tableLayout.addView(trSep, trParamsSep);
            }
        }
    }


    public void showYearDialogReload() {
        final Dialog d = new Dialog(MaterialPriceActivity.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_year);
        Button set = (Button) d.findViewById(R.id.btnOk);
        Button cancel = (Button) d.findViewById(R.id.btnCancel);
        TextView year_text = (TextView) d.findViewById(R.id.year_text);
        year_text.setText("Set Year");
        final NumberPicker yearpicker = (NumberPicker) d.findViewById(R.id.picker_year);

        yearpicker.setMinValue(2016);
        yearpicker.setMaxValue(2025);
        yearpicker.setWrapSelectorWheel(false);
        yearpicker.setValue(years);

        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Years = String.valueOf(yearpicker.getValue());
                new SharePreference(MaterialPriceActivity.this).setRefreshYear(Years);
                String y = new SharePreference(MaterialPriceActivity.this).isRefreshYear();
                if (deletetable(y)) {
                    GetMaterialPrice(y);
                }
                d.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }




}
