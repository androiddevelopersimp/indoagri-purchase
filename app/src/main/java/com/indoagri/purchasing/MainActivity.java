package com.indoagri.purchasing;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.purchasing.Common.Constants;
import com.indoagri.purchasing.Common.DeviceUtils;
import com.indoagri.purchasing.Retrofit.ApiServices;
import com.indoagri.purchasing.Retrofit.Model.UserModel;
import com.indoagri.purchasing.Retrofit.NetClient;
import com.indoagri.purchasing.Retrofit.SharePreference;
import com.indoagri.purchasing.Retrofit.UsersLoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    SharePreference sharedPreferences;
    boolean isShowing;
    ImageView imgInfoVersion;
    UserModel userModel;
    UsersLoginResponse usersLoginResponse;
    ApiServices apiServices;
    ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pDialog = new ProgressDialog(MainActivity.this);
        isShowing = pDialog.isShowing();
        sharedPreferences = new SharePreference(MainActivity.this);
        apiServices = NetClient.DataProduction().create(ApiServices.class);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        CardView cardMenu1 = (findViewById(R.id.cardHome1));
        cardMenu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(MainActivity.this, MaterialPriceActivity.class);
                startActivity(a);
            }
        });
        CardView cardMenu2 = (findViewById(R.id.cardHome2));
        cardMenu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent YTD = new Intent(MainActivity.this,YTDPurchaseActivity.class);
                startActivity(YTD);
            }
        });
        CardView cardMenu3 = (findViewById(R.id.cardHome3));
        cardMenu3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent b = new Intent(MainActivity.this,ProjectOnGoingActivity.class);
//                startActivity(b);
                Toast.makeText(MainActivity.this, "Coming soon, Under Construction", Toast.LENGTH_SHORT).show();
            }
        });
        CardView cardMenu4 = (findViewById(R.id.cardHome4));
        cardMenu4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Coming soon, Under Construction", Toast.LENGTH_SHORT).show();
            }
        });
        TextView txtVersion = (findViewById(R.id.txtVersion));
        txtVersion.setText("Vers. "+Constants.versionApps);
        TextView txtAppsName = (findViewById(R.id.txtappsName));
        txtAppsName.setText(getResources().getString(R.string.app_name));
        TextView txtUsername = (findViewById(R.id.txt_profile));
        String email = sharedPreferences.isEmail();
        String nama = sharedPreferences.isFullname();
        txtUsername.setText("Welcome, "+nama);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        ImageView imgLogout= (findViewById(R.id.imgLogout));
        imgLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = sharedPreferences.isUserName();
                String email = sharedPreferences.isEmail();
                String Domain = sharedPreferences.isDomain();
                String password = sharedPreferences.isPassword();
                String DeviceId = DeviceUtils.getDeviceID(MainActivity.this);
                AsyncLogout(MainActivity.this, email, "Logout Process Initialitation", password, username, Domain);
            }
        });
        imgInfoVersion = (findViewById(R.id.imgInfoVersion));
        if(sharedPreferences.isConfirmationUpdate()){
            imgInfoVersion.setVisibility(View.GONE);
        }else{
            imgInfoVersion.setVisibility(View.VISIBLE);
        }
        imgInfoVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogInfoVersion();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void Logout(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        sharedPreferences.setLogin(false);
                        Intent a = new Intent(MainActivity.this, LoginActivity.class);
                        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finish();
                        startActivity(a);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void showDialogInfoVersion() {
        final ArrayAdapter adapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.information, R.layout.item_spinner);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Informasi Pembaruan v."+BuildConfig.VERSION_NAME +" ");
        dialogBuilder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                new SharePreference(MainActivity.this).setKonfirmationUpdate(true);
            }
        });
        dialogBuilder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();
    }

    private void AsyncLogout(Context context, final String email, String DialogMessage, String password, final String username, String domain){
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(DialogMessage);
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        Log.d("LOGOUT", "loadFirstPage: ");

        getUsersMobile(email,password,username,domain).enqueue(new Callback<UsersLoginResponse>() {
            @Override
            public void onResponse(Call<UsersLoginResponse> call, Response<UsersLoginResponse> response) {
                pDialog.dismiss();
                if(response.code()==200){
                    usersLoginResponse = fetchResults(response);
                    userModel = usersLoginResponse.getUserLogout();
                    Logout();
                }else{
                    Log.e("Error","Logout Error");
                }
            }

            @Override
            public void onFailure(Call<UsersLoginResponse> call, Throwable t) {
                t.printStackTrace();
                pDialog.dismiss();

            }
        });

    }

    private UsersLoginResponse fetchResults(Response<UsersLoginResponse> response) {
        UsersLoginResponse loginResponse = response.body();
        return loginResponse;
    }
    private Call<UsersLoginResponse> getUsersMobile(String email, String password, String username, String domain) {
        return apiServices.LogoutPermission(Constants.APIKEY,username,password,domain,"939328493284983");
    }
}
