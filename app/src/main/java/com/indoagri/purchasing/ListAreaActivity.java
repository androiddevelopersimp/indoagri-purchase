package com.indoagri.purchasing;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.purchasing.Retrofit.Adapter.ListAreaAdapter;
import com.indoagri.purchasing.Retrofit.ApiServices;
import com.indoagri.purchasing.Retrofit.DB.DatabaseQuery;
import com.indoagri.purchasing.Retrofit.MaterialResponse;
import com.indoagri.purchasing.Retrofit.Model.MaterialPrice;
import com.indoagri.purchasing.Retrofit.SharePreference;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListAreaActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, TextWatcher {
    Bundle extras;
    String extrasString;
    Toolbar toolbar;
    TextView mTextToolbar, txtSetAll;
    ListView listView;
    EditText etSearch;
    DatabaseQuery query;
    SharePreference sharedPreferences;
    List<MaterialPrice> areaList;
    ListAreaAdapter listAreaAdapter;
    ProgressBar progressBar;
    private static final String TAGACTIVITY = "GET DATA MATERIAL";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_area);
        setUpToolbar();
        sharedPreferences = new SharePreference(ListAreaActivity.this);
        query = new DatabaseQuery(ListAreaActivity.this);
        listView = (ListView) findViewById(R.id.listView);
        etSearch = findViewById(R.id.et_search);
        etSearch.setVisibility(View.VISIBLE);
        progressBar = (findViewById(R.id.progress_bar));
        txtSetAll = (findViewById(R.id.txt_setAll));
        txtSetAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code="ALL";
                String description="";
                Intent intent=new Intent();
                intent.putExtra("CODE",code);
                intent.putExtra("DESC",description);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        extras = getIntent().getExtras();
        if (extras != null) {
            init();
        } else {

        }

    }

    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText(getResources().getString(R.string.activity_list_name_area));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    void init() {
        areaList=new ArrayList<MaterialPrice>();
        etSearch.addTextChangedListener(this);
        listView.setOnItemClickListener(this);
        listAreaAdapter = new ListAreaAdapter(this, R.layout.item_list_area, areaList);
        listView.setAdapter(listAreaAdapter);
        listView.setScrollingCacheEnabled(false);
        setHalaman();

    }


    // GET MATERIAL PRICE //


    private void setHalaman() {
        etSearch.addTextChangedListener(this);
        List<Object> listObject;
        query.openTransaction();
        String sqldb_query = null;

        if(new SharePreference(ListAreaActivity.this).isFormYear().equalsIgnoreCase("")) {
            sqldb_query = "select * from MATERIAL_PRICE group by AREA";
            listObject = query.getListDataRawQuery(sqldb_query, MaterialPrice.TABLE_NAME, null);
        }else{
            String Year = new SharePreference(ListAreaActivity.this).isFormYear();
            String[] a = new String[1];
            a[0] = Year;
            sqldb_query = "select * from  MATERIAL_PRICE where substr(LSTPRD,1,4) = ? group by AREA";
            listObject = query.getListDataRawQuery(sqldb_query, MaterialPrice.TABLE_NAME, a);
        }
        query.closeTransaction();
        if (listObject.size() > 0) {
            for (int i = 0; i < listObject.size(); i++) {
                MaterialPrice hasil = (MaterialPrice) listObject.get(i);
                areaList.add(hasil);
            }
        }
        for (int i = 0; i < areaList.size(); i++) {
            MaterialPrice areaModel = (MaterialPrice) areaList.get(i);
            listAreaAdapter.addData(areaModel);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }


    @Override
    public void afterTextChanged(Editable s) {
        listAreaAdapter.getFilter().filter(s);
        listAreaAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
        MaterialPrice materialPrice = (MaterialPrice) listAreaAdapter.getItem(i);
        String code=materialPrice.getArea();
        String description=materialPrice.getArea();
        Intent intent=new Intent();
        intent.putExtra("CODE",code);
        intent.putExtra("DESC",description);
        setResult(RESULT_OK, intent);
        finish();
    }

}
