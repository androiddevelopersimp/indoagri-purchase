package com.indoagri.purchasing;

import android.content.ContentValues;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.purchasing.Fragment.FormSearching;
import com.indoagri.purchasing.Retrofit.Adapter.TopRankAdapter;
import com.indoagri.purchasing.Retrofit.ApiServices;
import com.indoagri.purchasing.Retrofit.DB.DatabaseQuery;
import com.indoagri.purchasing.Retrofit.MaterialResponse;
import com.indoagri.purchasing.Retrofit.Model.MaterialModel;
import com.indoagri.purchasing.Retrofit.Model.MaterialPrice;
import com.indoagri.purchasing.Retrofit.Model.TopRankModel;
import com.indoagri.purchasing.Retrofit.NetClient;
import com.indoagri.purchasing.Retrofit.SharePreference;
import com.indoagri.purchasing.Retrofit.TopRankResponse;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaterialPriceActivityOri extends AppCompatActivity implements FormSearching.OnFragmentInteractionListener {
    private static final String TAGACTIVITY = "GET DATA MATERIAL";
    SharePreference sharePreference;
    Toolbar toolbar;
    TextView mTextToolbar;
    DatabaseQuery query;
    TopRankAdapter topRankAdapter;
    RecyclerView recyclerView;
    List<TopRankModel> topRankModelList;
    TopRankResponse topRankResponse;

    ArrayList<MaterialModel> materialModelList = new ArrayList<MaterialModel>();
    MaterialResponse materialResponse;
    ApiServices apiServices;
    ImageView imgReload, imgSort, imgSearch;
    ProgressBar progressBar;
    boolean sort = true;
    LinearLayoutManager layoutManager;
    DividerItemDecoration dividerItemDecoration;
    String QueryString = null;
    FrameLayout frame_display, searchFragment;
    boolean ShowSearch = false;
    public FragmentRefreshListener getFragmentRefreshListener() {
        return fragmentRefreshListener;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener fragmentRefreshListener) {
        this.fragmentRefreshListener = fragmentRefreshListener;
    }
    private FragmentRefreshListener fragmentRefreshListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_price);
        query = new DatabaseQuery(MaterialPriceActivityOri.this);
        topRankModelList = new ArrayList<>();
        progressBar = (findViewById(R.id.progress_bar));
        frame_display = (findViewById(R.id.frame_view_display));
        searchFragment = (findViewById(R.id.fragment_container));
        FloatingActionButton fab = findViewById(R.id.fab);
        recyclerView = (findViewById(R.id.recycler_view));
        recyclerView.setHasFixedSize(true);
        imgReload = (findViewById(R.id.imgReload));
        imgReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (deletetable()) {
                    GetDataMaterials();
                }
            }
        });
        imgSort = (findViewById(R.id.imgSort));
        imgSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sort) {
                    topRankModelList.clear();
//                    QueryString = "SELECT * FROM PURCHASE_TOPRANKS where COMPANYGROUP = '" + new SharePreference(MaterialPriceActivity.this).isFormGroupCompany() + "'" + "order by CAST(TOPRANK AS INTEGER) asc";
                    Toast.makeText(getApplicationContext(), "Descending", Toast.LENGTH_SHORT).show();
                    Ascending(QueryString, false);
                } else {
                    topRankModelList.clear();
                    Toast.makeText(getApplicationContext(), "Ascending", Toast.LENGTH_SHORT).show();
//                    QueryString = "SELECT * FROM PURCHASE_TOPRANKS where COMPANYGROUP = '" + new SharePreference(MaterialPriceActivity.this).isFormGroupCompany() + "'" + "order by CAST(TOPRANK AS INTEGER) desc";
                    Ascending(QueryString, true);
                }
            }
        });
        imgSearch = (findViewById(R.id.imgSearch));
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ShowSearch) {
                    onBackPressed();
                } else {
                    toSearch();
                }
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        apiServices = NetClient.DataProduction().create(ApiServices.class);
        setUpToolbar();
        initActivity();
    }

    private void initActivity() {
        if (deletetable()) {
            GetDataMaterials();
        }

    }

    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText("Material Top of 20");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private boolean deletetable() {
        progressBar.setVisibility(View.VISIBLE);
        boolean result = false;
        query.openTransaction();
        int rowID = 0;
        int rowID2 = 0;
        try {
            rowID = query.deleteDataTemporary(MaterialModel.TABLE_NAME, null, null);
            if (rowID > 0 || rowID == 0) {
                rowID2 = query.deleteDataTemporary(TopRankModel.TABLE_NAME, null, null);
                if (rowID2 > 0 || rowID2 == 0) {
//                    query.deleteDataTable(MaterialModel.TABLE_NAME,null,null);
//                    query.deleteDataTable(TopRankModel.TABLE_NAME,null,null);
                }
            }
        } finally {
            query.commitTransaction();
            query.closeTransaction();
            result = true;
        }
        return result;
    }

    private void GetDataTopRank() {
        if (progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
        Log.d(TAGACTIVITY, "LOADPAGE: ");

        GetTopRanks().enqueue(new Callback<TopRankResponse>() {
            @Override
            public void onResponse(Call<TopRankResponse> call, Response<TopRankResponse> response) {
                // Got data. Send it to adapter
                progressBar.setVisibility(View.GONE);
                if (response.code() == 200) {
                    topRankResponse = fetchResultsTopRanks(response);
                    topRankModelList = topRankResponse.getTopRanks();
                    insertTopRanks();

                } else {
                    Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TopRankResponse> call, Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                // TODO: 08/11/16 handle failure
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    // GET TOP RANKS //
    private TopRankResponse fetchResultsTopRanks(Response<TopRankResponse> response) {
        TopRankResponse fleetsResponse = response.body();
        return fleetsResponse;
    }

    private Call<TopRankResponse> GetTopRanks() {
        return apiServices.getRanks();
    }

    private void insertTopRanks() {
        query.openTransaction();
        for (int i = 0; i < topRankModelList.size(); i++) {
            TopRankModel topRankModel = topRankModelList.get(i);
            ContentValues values = new ContentValues();
            values.put(TopRankModel.XML_ROWID, topRankModel.getRowID());
            values.put(TopRankModel.XML_COMPANYGROUP, topRankModel.getCompanyGroup().trim());
            values.put(TopRankModel.XML_COMPANYCODE, topRankModel.getCompanyCode().trim());
            values.put(TopRankModel.XML_MATERIALGROUP, topRankModel.getMaterialGroup().trim());
            values.put(TopRankModel.XML_MATERIALCODE, topRankModel.getMaterialCode().trim());
            values.put(TopRankModel.XML_TOPRANK, topRankModel.getTopRank());
            query.insertDataSQL(TopRankModel.TABLE_NAME, values);
        }
        query.commitTransaction();
        query.closeTransaction();
    }


    private void GetDataMaterials() {
        if (progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
        Log.d(TAGACTIVITY, "LOADPAGE: ");

        GetMaterials().enqueue(new Callback<MaterialResponse>() {
            @Override
            public void onResponse(Call<MaterialResponse> call, Response<MaterialResponse> response) {
                // Got data. Send it to adapter
                progressBar.setVisibility(View.GONE);
                if (response.code() == 200) {
                    materialResponse = fetchResultMaterials(response);
                    materialModelList = materialResponse.getMaterials();
                    insertMaterials();
                    if (ShowSearch) {
                        onBackPressed();
                    } else {
                        toSearch();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MaterialResponse> call, Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                // TODO: 08/11/16 handle failure
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    // GET TOP RANKS //
    private MaterialResponse fetchResultMaterials(Response<MaterialResponse> response) {
        MaterialResponse materialResponse = response.body();
        return materialResponse;
    }

    private Call<MaterialResponse> GetMaterials() {
        return apiServices.getMaterials();
    }

    private void insertMaterials() {
        query.openTransaction();
        for (int i = 0; i < materialModelList.size(); i++) {
            MaterialModel materialModel = materialModelList.get(i);
            ContentValues values = new ContentValues();
            values.put(MaterialModel.XML_ROWID, materialModel.getRowID());
            values.put(MaterialModel.XML_COMPANYGROUP, materialModel.getCompanyGroup().trim());
            values.put(MaterialModel.XML_DIVISI, materialModel.getDivisi().trim());
            values.put(MaterialModel.XML_COMPANYCODE, materialModel.getCompanyCode().trim());
            values.put(MaterialModel.XML_COMPANYDESCRIPTION, materialModel.getCompanyDescription().trim());
            values.put(MaterialModel.XML_BA, materialModel.getBA());
            values.put(MaterialModel.XML_BADESCRIPTION, materialModel.getBADescription());
            values.put(MaterialModel.XML_GROUPREPORT, materialModel.getGroupReport());
            values.put(MaterialModel.XML_MATERIALGROUPCODE, materialModel.getMaterialGroupCode());
            values.put(MaterialModel.XML_MATERIALGROUPDESCRIPTION, materialModel.getMaterialGroupDescription());
            values.put(MaterialModel.XML_MATERIALCODE, materialModel.getMaterialCode());
            values.put(MaterialModel.XML_MATERIALDESCRIPTION, materialModel.getMaterialDescription());
            values.put(MaterialModel.XML_LASTPRICE, materialModel.getLastPrice());
            values.put(MaterialModel.XML_UOM, materialModel.getUom());
            values.put(MaterialModel.XML_LASTUPDATE, materialModel.getLastUpdated());
            query.insertDataSQL(MaterialModel.TABLE_NAME, values);
        }
        query.commitTransaction();
        query.closeTransaction();
        GetDataTopRank();

    }


    private void Ascending(String queryString, boolean status) {
        List<Object> listObject = null;
//        query.openTransaction();
//        listObject = query.getListDataRawQuery(queryString, TopRankModel.TABLE_NAME, null);
//        query.closeTransaction();

//        if (listObject.size() > 0) {
        sort = status;
        String BA = new SharePreference(MaterialPriceActivityOri.this).isBA();
        if (BA == null || BA.equalsIgnoreCase("")) {
            CARIDATAWITHOUTBA(sort ? "asc" : "desc");
        } else {
            CARIDATAWithBA(sort ? "asc" : "desc");
        }
//            for (int i = 0; i < listObject.size(); i++) {
//                TopRankModel blk = (TopRankModel) listObject.get(i);
//                topRankModelList.add(blk);
//            }
//            topRankAdapter = new TopRankAdapter(MaterialPriceActivity.this, topRankModelList);
//            recyclerView.setAdapter(topRankAdapter);
//            topRankAdapter.notifyDataSetChanged();
//        topRankAdapter.setOnItemClickListener(new TopRankAdapter.ClickListener() {
//            @Override
//            public void onItemClick(int position, View v) {
//                TopRankModel topRankModel = topRankModelList.get(position);
//                query.openTransaction();
//                String[] a = new String[1];
//                a[0] = topRankModel.getMaterialCode();
//                String queryString = "select * from PURCHASE_MATERIAL where MATERIALCODE IN(select  MATERIALCODE from PURCHASE_TOPRANKS where MATERIALCODE=?)";
//                Object object = query.getDataFirstRaw(queryString, MaterialModel.TABLE_NAME, a);
//                query.closeTransaction();
//                MaterialModel materialModel = (MaterialModel) object;
//                NumberFormat formatter = new DecimalFormat("#,###,###,###,###,###");
//                double myNumber = Double.parseDouble((materialModel.getLastPrice()));
//                String formattedNumber = formatter.format(myNumber);
//                Toast.makeText(MaterialPriceActivity.this, materialModel.getMaterialDescription() + " - Rp. " + formattedNumber, Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onItemLongClick(int position, View v) {
//                TopRankModel topRankModel = topRankModelList.get(position);
//                query.openTransaction();
//                String[] a = new String[1];
//                a[0] = topRankModel.getMaterialCode();
//                String queryString = "select * from PURCHASE_MATERIAL where MATERIALCODE IN(select  MATERIALCODE from PURCHASE_TOPRANKS where MATERIALCODE=?)";
//                Object object = query.getDataFirstRaw(queryString, MaterialModel.TABLE_NAME, a);
//                query.closeTransaction();
//                MaterialModel materialModel = (MaterialModel) object;
//                NumberFormat formatter = new DecimalFormat("#,###,###,###,###,###");
//                double myNumber = Double.parseDouble((materialModel.getLastPrice()));
//                String formattedNumber = formatter.format(myNumber);
//                Toast.makeText(MaterialPriceActivity.this, materialModel.getMaterialDescription() + " - Rp. " + formattedNumber, Toast.LENGTH_SHORT).show();
//
//            }
//        });
//        }

    }

    public void toSearch() {
        searchFragment.setVisibility(View.VISIBLE);
        frame_display.setVisibility(View.GONE);
        Fragment fragment = new FormSearching();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
        ShowSearch = true;
    }

    @Override
    public void onBackPressed() {
        if (searchFragment.getVisibility() == View.VISIBLE) {
            searchFragment.setVisibility(View.GONE);
            frame_display.setVisibility(View.VISIBLE);
        }
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            new SharePreference(MaterialPriceActivityOri.this).setFormGroupcompany("");
            new SharePreference(MaterialPriceActivityOri.this).setFormCompany("");
            new SharePreference(MaterialPriceActivityOri.this).setBA("");
            new SharePreference(MaterialPriceActivityOri.this).setFormYear("");
        } else {
            getSupportFragmentManager().popBackStack();
        }
        ShowSearch = false;
    }

    @Override
    public void onFragmentInteraction(String year) {
        //     Toast.makeText(SalesBilling.this,year, Toast.LENGTH_SHORT).show();
        frame_display.setVisibility(View.VISIBLE);
        searchFragment.setVisibility(View.GONE);
        String BA = new SharePreference(MaterialPriceActivityOri.this).isBA();

        if (BA == null || BA.equalsIgnoreCase("")) {
            CARIDATAWITHOUTBA("");
        } else {
            CARIDATAWithBA("");
        }


    }

    private void CARIDATAWITHOUTBA(String sort) {
        query.openTransaction();
        if (topRankModelList.size() > 0 || topRankModelList != null) {
            topRankModelList.clear();
        }
        String GroupCompany = new SharePreference(MaterialPriceActivityOri.this).isFormGroupCompany();
        String PT = new SharePreference(MaterialPriceActivityOri.this).isFormCompany();
        String Divisi = new SharePreference(MaterialPriceActivityOri.this).isFormDIVISI();
        String GroupReport = new SharePreference(MaterialPriceActivityOri.this).isMaterialGroup();
        List<Object> listObject2;
        String[] a = new String[12];
        a[0] = GroupCompany;
        a[1] = PT;
        a[2] = Divisi;
        a[3] = GroupReport;
        a[4] = GroupCompany;
        a[5] = PT;
        a[6] = Divisi;
        a[7] = GroupReport;
        a[8] = GroupCompany;
        a[9] = PT;
        a[10] = Divisi;
        a[11] = GroupReport;
        String sqldb_query = "SELECT * FROM PURCHASE_TOPRANKS where COMPANYGROUP IN (select COMPANYGROUP from PURCHASE_MATERIAL where COMPANYGROUP=? AND COMPANYCODE=? AND DIVISI=? AND GROUPREPORT=?) and " +
                " COMPANYCODE IN (select COMPANYCODE from PURCHASE_MATERIAL where COMPANYGROUP=? AND COMPANYCODE=? AND DIVISI=? AND GROUPREPORT=?) and " +
                " MATERIALGROUP IN (select GROUPREPORT from PURCHASE_MATERIAL where COMPANYGROUP=? AND COMPANYCODE=? AND DIVISI=? AND GROUPREPORT=?) order by CAST(TOPRANK AS INTEGER) " + sort;
        listObject2 = query.getListDataRawQuery(sqldb_query, TopRankModel.TABLE_NAME, a);
        query.closeTransaction();
        if (listObject2.size() > 0) {
            for (int i = 0; i < listObject2.size(); i++) {
                TopRankModel blk = (TopRankModel) listObject2.get(i);
                topRankModelList.add(blk);
            }
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            topRankAdapter = new TopRankAdapter(MaterialPriceActivityOri.this, topRankModelList);
            recyclerView.setAdapter(topRankAdapter);
            topRankAdapter.notifyDataSetChanged();
//            topRankAdapter.setOnItemClickListener(new TopRankAdapter.ClickListener() {
//                @Override
//                public void onItemClick(int position, View v) {
//                    TopRankModel topRankModel = topRankModelList.get(position);
//                    Log.d(TAGACTIVITY, "onItemClick position: " + topRankModel.getMaterialCode());
//                }
//
//                @Override
//                public void onItemLongClick(int position, View v) {
//                    TopRankModel topRankModel = topRankModelList.get(position);
//                    Log.d(TAGACTIVITY, "onItemClick position: " + topRankModel.getMaterialCode());
//                }
//            });
            topRankAdapter.setOnItemClickListener(new TopRankAdapter.ClickListener() {
                @Override
                public void onItemClick(int position, View v) {
                    TopRankModel topRankModel = topRankModelList.get(position);
                    query.openTransaction();
                    String[] a = new String[1];
                    a[0] = topRankModel.getMaterialCode();
                    String queryString = "select * from PURCHASE_MATERIAL where MATERIALCODE IN(select  MATERIALCODE from PURCHASE_TOPRANKS where MATERIALCODE=?)";
                    Object object = query.getDataFirstRaw(queryString, MaterialModel.TABLE_NAME, a);
                    query.closeTransaction();
                    MaterialModel materialModel = (MaterialModel) object;
                    NumberFormat formatter = new DecimalFormat("#,###,###,###,###,###");
                    double myNumber = Double.parseDouble((materialModel.getLastPrice()));
                    String formattedNumber = formatter.format(myNumber);
                    Toast.makeText(MaterialPriceActivityOri.this, materialModel.getMaterialDescription() + " - Rp. " + formattedNumber, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onItemLongClick(int position, View v) {
                    TopRankModel topRankModel = topRankModelList.get(position);
                    query.openTransaction();
                    String[] a = new String[1];
                    a[0] = topRankModel.getMaterialCode();
                    String queryString = "select * from PURCHASE_MATERIAL where MATERIALCODE IN(select  MATERIALCODE from PURCHASE_TOPRANKS where MATERIALCODE=?)";
                    Object object = query.getDataFirstRaw(queryString, MaterialModel.TABLE_NAME, a);
                    query.closeTransaction();
                    MaterialModel materialModel = (MaterialModel) object;
                    NumberFormat formatter = new DecimalFormat("#,###,###,###,###,###");
                    double myNumber = Double.parseDouble((materialModel.getLastPrice()));
                    String formattedNumber = formatter.format(myNumber);
                    Toast.makeText(MaterialPriceActivityOri.this, materialModel.getMaterialDescription() + " - Rp. " + formattedNumber, Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            topRankAdapter = new TopRankAdapter(MaterialPriceActivityOri.this, topRankModelList);
            recyclerView.setAdapter(topRankAdapter);
            topRankAdapter.notifyDataSetChanged();
            Toast.makeText(MaterialPriceActivityOri.this, " DATA TIDAK ADA ", Toast.LENGTH_SHORT).show();
        }
    }

    private void CARIDATAWithBA(String sort) {
        query.openTransaction();
        if (topRankModelList.size() > 0 || topRankModelList != null) {
            topRankModelList.clear();
        }
        String GroupCompany = new SharePreference(MaterialPriceActivityOri.this).isFormGroupCompany();
        String PT = new SharePreference(MaterialPriceActivityOri.this).isFormCompany();
        String Divisi = new SharePreference(MaterialPriceActivityOri.this).isFormDIVISI();
        String BA = new SharePreference(MaterialPriceActivityOri.this).isBA();
        String GroupReport = new SharePreference(MaterialPriceActivityOri.this).isMaterialGroup();
        List<Object> listObject2;
        String[] a = new String[15];
        a[0] = GroupCompany;
        a[1] = PT;
        a[2] = Divisi;
        a[3] = GroupReport;
        a[4] = BA;
        a[5] = GroupCompany;
        a[6] = PT;
        a[7] = Divisi;
        a[8] = GroupReport;
        a[9] = BA;
        a[10] = GroupCompany;
        a[11] = PT;
        a[12] = Divisi;
        a[13] = GroupReport;
        a[14] = BA;
        String sqldb_query = "SELECT * FROM PURCHASE_TOPRANKS where COMPANYGROUP IN (select COMPANYGROUP from PURCHASE_MATERIAL where COMPANYGROUP=? AND COMPANYCODE=? AND DIVISI=? AND GROUPREPORT=? and BA=?) and " +
                "                COMPANYCODE IN (select COMPANYCODE from PURCHASE_MATERIAL where COMPANYGROUP=? AND COMPANYCODE=? AND DIVISI=? AND GROUPREPORT=? and BA=?) and " +
                "                MATERIALGROUP IN (select GROUPREPORT from PURCHASE_MATERIAL where COMPANYGROUP=? AND COMPANYCODE=? AND DIVISI=? AND GROUPREPORT=? and BA=?) order by CAST(TOPRANK AS INTEGER) " + sort;
        listObject2 = query.getListDataRawQuery(sqldb_query, TopRankModel.TABLE_NAME, a);
        query.closeTransaction();
        if (listObject2.size() > 0) {
            for (int i = 0; i < listObject2.size(); i++) {
                TopRankModel blk = (TopRankModel) listObject2.get(i);
                topRankModelList.add(blk);
            }
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            topRankAdapter = new TopRankAdapter(MaterialPriceActivityOri.this, topRankModelList);
            recyclerView.setAdapter(topRankAdapter);
            topRankAdapter.notifyDataSetChanged();
//            topRankAdapter.setOnItemClickListener(new TopRankAdapter.ClickListener() {
//                @Override
//                public void onItemClick(int position, View v) {
//                    TopRankModel topRankModel = topRankModelList.get(position);
//                    Log.d(TAGACTIVITY, "onItemClick position: " + topRankModel.getMaterialCode());
//                }
//
//                @Override
//                public void onItemLongClick(int position, View v) {
//                    TopRankModel topRankModel = topRankModelList.get(position);
//
//                }
//            });
            topRankAdapter.setOnItemClickListener(new TopRankAdapter.ClickListener() {
                @Override
                public void onItemClick(int position, View v) {
                    TopRankModel topRankModel = topRankModelList.get(position);
                    query.openTransaction();
                    String[] a = new String[1];
                    a[0] = topRankModel.getMaterialCode();
                    String queryString = "select * from PURCHASE_MATERIAL where MATERIALCODE IN(select  MATERIALCODE from PURCHASE_TOPRANKS where MATERIALCODE=?)";
                    Object object = query.getDataFirstRaw(queryString, MaterialModel.TABLE_NAME, a);
                    query.closeTransaction();
                    MaterialModel materialModel = (MaterialModel) object;
                    NumberFormat formatter = new DecimalFormat("#,###,###,###,###,###");
                    double myNumber = Double.parseDouble((materialModel.getLastPrice()));
                    String formattedNumber = formatter.format(myNumber);
                    Toast.makeText(MaterialPriceActivityOri.this, materialModel.getMaterialDescription() + " - Rp. " + formattedNumber, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onItemLongClick(int position, View v) {
                    TopRankModel topRankModel = topRankModelList.get(position);
                    query.openTransaction();
                    String[] a = new String[1];
                    a[0] = topRankModel.getMaterialCode();
                    String queryString = "select * from PURCHASE_MATERIAL where MATERIALCODE IN(select  MATERIALCODE from PURCHASE_TOPRANKS where MATERIALCODE=?)";
                    Object object = query.getDataFirstRaw(queryString, MaterialModel.TABLE_NAME, a);
                    query.closeTransaction();
                    MaterialModel materialModel = (MaterialModel) object;
                    NumberFormat formatter = new DecimalFormat("#,###,###,###,###,###");
                    double myNumber = Double.parseDouble((materialModel.getLastPrice()));
                    String formattedNumber = formatter.format(myNumber);
                    Toast.makeText(MaterialPriceActivityOri.this, materialModel.getMaterialDescription() + " - Rp. " + formattedNumber, Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            topRankAdapter = new TopRankAdapter(MaterialPriceActivityOri.this, topRankModelList);
            recyclerView.setAdapter(topRankAdapter);
            topRankAdapter.notifyDataSetChanged();
            Toast.makeText(MaterialPriceActivityOri.this, " DATA TIDAK ADA ", Toast.LENGTH_SHORT).show();
        }
    }
    public interface FragmentRefreshListener{
        void onRefresh();
    }


}
