package com.indoagri.purchasing;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.purchasing.Retrofit.Adapter.ListAreaAdapter;
import com.indoagri.purchasing.Retrofit.Adapter.ListYearAdapter;
import com.indoagri.purchasing.Retrofit.ApiServices;
import com.indoagri.purchasing.Retrofit.DB.DatabaseQuery;
import com.indoagri.purchasing.Retrofit.MaterialResponse;
import com.indoagri.purchasing.Retrofit.Model.MaterialPrice;
import com.indoagri.purchasing.Retrofit.Model.YTDPurchase;
import com.indoagri.purchasing.Retrofit.Model.YearModel;
import com.indoagri.purchasing.Retrofit.SharePreference;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListYearActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, TextWatcher {
    Bundle bundle;
    Toolbar toolbar;
    TextView mTextToolbar, txtSetAll;
    ListView listView;
    EditText etSearch;
    DatabaseQuery query;
    SharePreference sharedPreferences;
    ProgressBar progressBar;
    List<YearModel>yearModelList = new ArrayList<YearModel>();
    ListYearAdapter listYearAdapter;
    private static final String TAGACTIVITY = "GET DATA MATERIAL";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_area);
        setUpToolbar();
        sharedPreferences = new SharePreference(ListYearActivity.this);
        query = new DatabaseQuery(ListYearActivity.this);
        listView = (ListView) findViewById(R.id.listView);
        etSearch = findViewById(R.id.et_search);
        etSearch.setVisibility(View.VISIBLE);
        progressBar = (findViewById(R.id.progress_bar));
        txtSetAll = (findViewById(R.id.txt_setAll));
        txtSetAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code="ALL";
                String description="";
                Intent intent=new Intent();
                intent.putExtra("CODE",code);
                intent.putExtra("DESC",description);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        bundle = getIntent().getExtras();
       // ArrayList<Student> arraylist = bundle.getParcelableArrayList("mylist");
     //   getDataIntent = getIntent();
        if (bundle != null) {
            init();
        } else {

        }

    }

    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText(getResources().getString(R.string.activity_list_name_area));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    void init() {
        String tableName = bundle.getString("table");
        etSearch.addTextChangedListener(this);
        listView.setOnItemClickListener(this);
        listYearAdapter = new ListYearAdapter(this, R.layout.item_list_area, yearModelList);
        listView.setAdapter(listYearAdapter);
        listView.setScrollingCacheEnabled(false);
        setHalaman(tableName);
    }


    // GET MATERIAL PRICE //

    void setHalaman(String table) {
        if (table.equalsIgnoreCase(MaterialPrice.TABLE_NAME)) {
            List<Object> listObject;
            ArrayList<MaterialPrice> arraylist = bundle.getParcelableArrayList("data");
            query.openTransaction();
            String sqldb_query = null;
            sqldb_query = "select * from MATERIAL_PRICE group by substr(LSTPRD,1,4)";
            listObject = query.getListDataRawQuery(sqldb_query, MaterialPrice.TABLE_NAME, null);
            query.closeTransaction();
            for (int i = 0; i < listObject.size(); i++) {
                MaterialPrice hasil = (MaterialPrice) listObject.get(i);
                YearModel yearModel = new YearModel();
                yearModel.setLstprd(hasil.getLstprd());
                yearModelList.add(yearModel);
            }
        }
        if (table.equalsIgnoreCase(YTDPurchase.TABLE_NAME)) {
            List<Object> listObject;
            ArrayList<YTDPurchase> arraylist = bundle.getParcelableArrayList("data");
            query.openTransaction();
            String sqldb_query = null;
            sqldb_query = "select * from YTD_PURCHASE group by substr(LSTPRD,1,4)";
            listObject = query.getListDataRawQuery(sqldb_query, YTDPurchase.TABLE_NAME, null);
            query.closeTransaction();
            for (int i = 0; i < listObject.size(); i++) {
                YTDPurchase hasil = (YTDPurchase) listObject.get(i);
                YearModel yearModel = new YearModel();
                yearModel.setLstprd(hasil.getLstprd());
                yearModelList.add(yearModel);
            }
        }
    }

//    private void setHalaman(String table, ArrayList data) {
//        etSearch.addTextChangedListener(this);
//        List<Object> listObject;
//        query.openTransaction();
//        String sqldb_query = null;
//        sqldb_query = "select LSTPRD from MATERIAL_PRICE group by substr(LSTPRD,1,4)";
//            listObject = query.getListDataRawQuery(sqldb_query, MaterialPrice.TABLE_NAME, null);
//        query.closeTransaction();
//        if (listObject.size() > 0) {
//            for (int i = 0; i < listObject.size(); i++) {
//                MaterialPrice hasil = (MaterialPrice) listObject.get(i);
//                YearModel yearModel = new YearModel();
//                yearModel.setLstprd(hasil.getLstprd());
//                yearModelList.add(yearModel);
//            }
//        }
////        for (int i = 0; i < yearModelList.size(); i++) {
////            YearModel yearModel = (YearModel) yearModelList.get(i);
////            listYearAdapter.addData(yearModel);
////        }
//    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }


    @Override
    public void afterTextChanged(Editable s) {
        listYearAdapter.getFilter().filter(s);
        listYearAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
        YearModel yearModel = (YearModel) listYearAdapter.getItem(i);
        String code=yearModel.getLstprd().substring(0,4);
        String description=yearModel.getLstprd().substring(0,4);
        Intent intent=new Intent();
        intent.putExtra("CODE",code);
        intent.putExtra("DESC",description);
        setResult(RESULT_OK, intent);
        finish();
    }

}
