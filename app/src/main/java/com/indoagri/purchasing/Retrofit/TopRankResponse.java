package com.indoagri.purchasing.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.purchasing.Retrofit.Model.MaterialModel;
import com.indoagri.purchasing.Retrofit.Model.TopRankModel;

import java.util.List;

public class TopRankResponse {
    @SerializedName("purchasingRank")
    @Expose
    List<TopRankModel> topRankModelList;

    public List getTopRanks() {
        return topRankModelList;
    }
}
