package com.indoagri.purchasing.Retrofit.Model;


import com.google.gson.annotations.SerializedName;

public class MaterialModel {
    public static String TABLE_NAME = "PURCHASE_MATERIAL";
    public static String XML_ROWID= "ROWID";
    public static String XML_COMPANYGROUP= "COMPANYGROUP";
    public static String XML_DIVISI= "DIVISI";
    public static String XML_COMPANYCODE= "COMPANYCODE";
    public static String XML_COMPANYDESCRIPTION= "COMPANYDESCRIPTION";
    public static String XML_BA= "BA";
    public static String XML_BADESCRIPTION= "BADESCRIPTION";
    public static String XML_GROUPREPORT= "GROUPREPORT";
    public static String XML_MATERIALGROUPCODE= "MATERIALGROUPCODE";
    public static String XML_MATERIALGROUPDESCRIPTION= "MATERIALGROUPDESCRIPTION";
    public static String XML_MATERIALCODE= "MATERIALCODE";
    public static String XML_MATERIALDESCRIPTION= "MATERIALDESCRIPTION";
    public static String XML_LASTPRICE= "LASTPRICE";
    public static String XML_UOM= "UOM";
    public static String XML_LASTUPDATE= "LASTUPDATED";

    @SerializedName("rowID")
    private int rowID;
    @SerializedName("companyGroup")
    private String CompanyGroup;
    @SerializedName("divisi")
    private String Divisi;
    @SerializedName("companyCode")
    private String CompanyCode;
    @SerializedName("companyDescription")
    private String CompanyDescription;
    @SerializedName("ba")
    private String BA;
    @SerializedName("baDescription")
    private String BADescription;
    @SerializedName("materialCode")
    private String MaterialCode;
    @SerializedName("materialGroupCode")
    private String MaterialGroupCode;
    @SerializedName("materialGroupDescription")
    private String MaterialGroupDescription;
    @SerializedName("materialDescription")
    private String MaterialDescription;
    @SerializedName("lastUpdated")
    private String LastUpdated;
    @SerializedName("groupReport")
    private String GroupReport;
    @SerializedName("lastPrice")
    private String LastPrice;
    @SerializedName("uom")
    private String uom;

    public MaterialModel() {
    }

    public int getRowID() {
        return rowID;
    }

    public void setRowID(int rowID) {
        this.rowID = rowID;
    }

    public String getCompanyGroup() {
        return CompanyGroup;
    }

    public void setCompanyGroup(String companyGroup) {
        CompanyGroup = companyGroup;
    }

    public String getDivisi() {
        return Divisi;
    }

    public void setDivisi(String divisi) {
        Divisi = divisi;
    }

    public String getCompanyCode() {
        return CompanyCode;
    }

    public void setCompanyCode(String companyCode) {
        CompanyCode = companyCode;
    }

    public String getCompanyDescription() {
        return CompanyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        CompanyDescription = companyDescription;
    }

    public String getBA() {
        return BA;
    }

    public void setBA(String BA) {
        this.BA = BA;
    }

    public String getBADescription() {
        return BADescription;
    }

    public void setBADescription(String BADescription) {
        this.BADescription = BADescription;
    }

    public String getMaterialCode() {
        return MaterialCode;
    }

    public void setMaterialCode(String materialCode) {
        MaterialCode = materialCode;
    }

    public String getMaterialGroupCode() {
        return MaterialGroupCode;
    }

    public void setMaterialGroupCode(String materialGroupCode) {
        MaterialGroupCode = materialGroupCode;
    }

    public String getMaterialGroupDescription() {
        return MaterialGroupDescription;
    }

    public void setMaterialGroupDescription(String materialGroupDescription) {
        MaterialGroupDescription = materialGroupDescription;
    }

    public String getMaterialDescription() {
        return MaterialDescription;
    }

    public void setMaterialDescription(String materialDescription) {
        MaterialDescription = materialDescription;
    }

    public String getLastUpdated() {
        return LastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        LastUpdated = lastUpdated;
    }

    public String getGroupReport() {
        return GroupReport;
    }

    public void setGroupReport(String groupReport) {
        GroupReport = groupReport;
    }

    public String getLastPrice() {
        return LastPrice;
    }

    public void setLastPrice(String lastPrice) {
        LastPrice = lastPrice;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }
}
