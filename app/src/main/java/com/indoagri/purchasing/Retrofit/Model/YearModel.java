package com.indoagri.purchasing.Retrofit.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YearModel {
    public static String XML_LSTPRD= "LSTPRD";
    @SerializedName("lstprd")
    @Expose
    private String lstprd;
    public YearModel() {
    }

    public String getLstprd() {
        return lstprd;
    }

    public void setLstprd(String lstprd) {
        this.lstprd = lstprd;
    }
}
