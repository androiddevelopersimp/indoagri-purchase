package com.indoagri.purchasing.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.purchasing.Retrofit.Model.AppDetailModel;

public class AppDetailResponse {
    @SerializedName("appDetail")
    @Expose
    AppDetailModel appDetailModel;

    public AppDetailModel getAPP() {
        return appDetailModel;
    }
}
