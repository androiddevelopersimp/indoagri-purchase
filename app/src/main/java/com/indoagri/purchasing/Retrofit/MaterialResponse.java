package com.indoagri.purchasing.Retrofit;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.purchasing.Retrofit.Model.MaterialModel;
import com.indoagri.purchasing.Retrofit.Model.MaterialPrice;

import java.util.ArrayList;
import java.util.List;

public class MaterialResponse {
    @SerializedName("purchasingMaterial")
    @Expose
    ArrayList<MaterialModel> materialModelList;

    @SerializedName("materialPrice")
    @Expose
    ArrayList<MaterialPrice> materialPriceList;

    public ArrayList<MaterialModel> getMaterials() {
        return materialModelList;
    }
    public ArrayList<MaterialPrice> getMaterialPrice() {
        return materialPriceList;
    }
}
