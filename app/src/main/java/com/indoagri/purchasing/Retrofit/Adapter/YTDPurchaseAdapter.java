package com.indoagri.purchasing.Retrofit.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indoagri.purchasing.R;
import com.indoagri.purchasing.Retrofit.DB.DatabaseQuery;
import com.indoagri.purchasing.Retrofit.Model.YTDPurchase;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class YTDPurchaseAdapter extends RecyclerView.Adapter<YTDPurchaseAdapter.MaterialPriceViewHolder> {

    private static ClickListener clickListener;

    Context mContext;
    private List<YTDPurchase> dataList;
    DatabaseQuery databaseQuery;

    public YTDPurchaseAdapter(Context mContext, List<YTDPurchase> dataList) {
        databaseQuery = new DatabaseQuery(mContext);
        this.mContext = mContext;
        this.dataList = dataList;
    }

    @Override
    public MaterialPriceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_ytd, parent, false);
        return new MaterialPriceViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    @Override
    public void onBindViewHolder(MaterialPriceViewHolder holder, int position) {
        YTDPurchase ytdPurchase = dataList.get(position);
        NumberFormat formatter = new DecimalFormat("#,###,###,###,###,###");
//        double myNumber = Double.parseDouble((materialPrice.getLastPrice()));
//        String formattedNumber = formatter.format(myNumber);
//        holder.txtCompanyGroup.setText(dataList.get(position).getMatgrp());
//        holder.txtCompanyCode.setText(dataList.get(position).getMatgrp());
        holder.txtCompany.setText(ytdPurchase.getComPGRP());
        holder.txtGroup.setText(ytdPurchase.getMatgrpdesc());
        if(ytdPurchase.getLstprdtot()!=null){
            double myNumber = Double.parseDouble((ytdPurchase.getLstprdtot()));
            String formattedNumber = formatter.format(myNumber/1000000);
            holder.txtLastPriod.setText("Rp. "  + formattedNumber);
        }else{
            holder.txtLastPriod.setText("--");
        }
        if(ytdPurchase.getYtdtot()!=null){
            double myNumber = Double.parseDouble((ytdPurchase.getYtdtot()));
            String formattedNumber = formatter.format(myNumber/1000000);
            holder.txtYTD.setText("Rp. "  + formattedNumber);
        }else{
            holder.txtYTD.setText("--");
        }

        holder.txtTopRank.setText("# "+String.valueOf(position+1));
    }

    public class MaterialPriceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView txtCompany, txtGroup, txtLastPriod, txtYTD,txtTopRank;

        public MaterialPriceViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            txtCompany = (TextView) itemView.findViewById(R.id.txt_Company);
            txtGroup = (TextView) itemView.findViewById(R.id.txt_groupdesc);
            txtLastPriod = (TextView) itemView.findViewById(R.id.txt_lastprd);
            txtYTD = (TextView) itemView.findViewById(R.id.txt_ytd);
            txtTopRank = (TextView) itemView.findViewById(R.id.txt_Rank);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

        void onItemLongClick(int position, View v);
    }

}
