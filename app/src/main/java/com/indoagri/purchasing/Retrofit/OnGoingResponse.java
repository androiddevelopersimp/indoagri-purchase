package com.indoagri.purchasing.Retrofit;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.purchasing.Retrofit.Model.MaterialModel;
import com.indoagri.purchasing.Retrofit.Model.ProjectOnGoingImagesModel;
import com.indoagri.purchasing.Retrofit.Model.ProjectOnGoingModel;
import com.indoagri.purchasing.Retrofit.Model.ProjectOnGoingProgressModel;

import java.util.List;

public class OnGoingResponse {
    @SerializedName("purchaseOnGoing")
    @Expose
    List<ProjectOnGoingModel> projectOnGoingModels;

    @SerializedName("purchaseOnGoingImages")
    @Expose
    List<ProjectOnGoingImagesModel> projectOnGoingImagesModels;

    @SerializedName("purchaseOnGoingProgress")
    @Expose
    List<ProjectOnGoingProgressModel> projectOnGoingProgressModels;

    public List getOnGoing() {
        return projectOnGoingModels;
    }
    public List getOnGoingImages() {
        return projectOnGoingImagesModels;
    }
    public List getOnGoingProgress() {
        return projectOnGoingProgressModels;
    }
}
