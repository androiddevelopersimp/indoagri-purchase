package com.indoagri.purchasing.Retrofit.Adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.indoagri.purchasing.R;
import com.indoagri.purchasing.Retrofit.Model.MaterialPrice;
import com.indoagri.purchasing.Retrofit.Model.YearModel;

import java.util.ArrayList;
import java.util.List;

public class ListYearAdapter extends ArrayAdapter<YearModel> {

    List<YearModel> yearModels,lst_temp;
    Context context;
    int layout;

    public ListYearAdapter(Context context, int layout, List<YearModel> yearModels) {
        super(context, layout, yearModels);
        this.context=context;
        this.layout=layout;
        this.yearModels=yearModels;
    }


    @Override
    public View getView(int position, View v, ViewGroup parent) {
        YearModel mat=yearModels.get(position);
        StudentHolder holder;
        if(v==null){
            LayoutInflater vi=((Activity)context).getLayoutInflater();
            //  v=vi.inflate(layout, parent,false);
            v = vi.inflate(layout, null);
            holder=new StudentHolder();
            holder.imageView=(ImageView) v.findViewById(R.id.imgClick);
            holder.code=(TextView) v.findViewById(R.id.txtItemCode);
            holder.code.setVisibility(View.GONE);
            holder.name=(TextView) v.findViewById(R.id.txtItemName);
            v.setTag(holder);
        }
        else{
            holder=(StudentHolder) v.getTag();
            holder.imageView=(ImageView) v.findViewById(R.id.imgClick);
            holder.code=(TextView) v.findViewById(R.id.txtItemCode);
            holder.code.setVisibility(View.GONE);
            holder.name=(TextView) v.findViewById(R.id.txtItemName);
        }
        holder.code.setText(String.valueOf(yearModels.get(position)));
        holder.name.setText(mat.getLstprd().substring(0,4));
        return v;
    }

    static class StudentHolder{
        ImageView imageView;
        TextView code;
        TextView name;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    yearModels = lst_temp;
                } else {
                    if(lst_temp == null){
                        lst_temp = new ArrayList<YearModel>(yearModels);
                    }
                    List<YearModel> filteredList = new ArrayList<>();
                    for (YearModel row : lst_temp) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getLstprd().toLowerCase().contains(charString.toLowerCase()) || row.getLstprd().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    yearModels = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = yearModels;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                //materialPriceList = (ArrayList<MaterialPrice>) filterResults.values;
                yearModels = (ArrayList<YearModel>)filterResults.values;
                notifyDataSetChanged();
                clear();
                int count = yearModels.size();
                for(int i = 0; i<count; i++){
                    add(yearModels.get(i));
                    notifyDataSetInvalidated();
                }
            }
        };
    }
    public void addData(YearModel estateModel){
        boolean found = false;

        for(int i = 0; i < yearModels.size(); i++){
            if(estateModel.getLstprd().equalsIgnoreCase(yearModels.get(i).getLstprd())){
                found = true;
                break;
            }
        }

        if(!found){
            yearModels.add(estateModel);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return yearModels.size();
    }


    @Override
    public long getItemId(int pos) {
        return pos;
    }

}