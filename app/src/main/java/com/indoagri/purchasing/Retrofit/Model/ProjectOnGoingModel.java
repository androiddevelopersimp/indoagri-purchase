package com.indoagri.purchasing.Retrofit.Model;

import com.google.gson.annotations.SerializedName;

public class ProjectOnGoingModel {

    public static String TABLE_NAME = "PURCHASE_ONGOING";
    public static String XML_PROJECTID= "PROJECTID";
    public static String XML_DESCRIPTION= "DESCRIPTION";
    public static String XML_COMPANYGROUP= "COMPANYGROUP";
    public static String XML_DIVISI= "DIVISI";
    public static String XML_COMPANYCODE= "COMPANYCODE";
    public static String XML_COMPANYDESCRIPTION= "COMPANYDESCRIPTION";
    public static String XML_BA= "BA";
    public static String XML_BADESCRIPTION= "BADESCRIPTION";
    public static String XML_PO= "PO";
    public static String XML_CAPACITY= "CAPACITY";
    public static String XML_VENDORCODE= "VENDORCODE";
    public static String XML_VENDORNAME= "VENDORNAME";
    public static String XML_LOCATION= "LOCATION";
    public static String XML_QTY= "QTY";
    public static String XML_VALUE= "VALUE";
    public static String XML_PROGRESS= "PROGRESS";
    public static String XML_STARTED= "STARTED";
    public static String XML_STATUS= "STATUS";
    public static String XML_ENDED= "ENDED";
    public static String XML_LASTUPDATE= "LASTUPDATE";

    @SerializedName("projectID")
    private String projectID;
    @SerializedName("description")
    private String description;
    @SerializedName("companyGroup")
    private String companyGroup;
    @SerializedName("divisi")
    private String divisi;
    @SerializedName("companyCode")
    private String companyCode;
    @SerializedName("companyDescription")
    private String companyDescription;
    @SerializedName("ba")
    private String ba;
    @SerializedName("baDescription")
    private String baDescription;
    @SerializedName("po")
    private String poNumber;
    @SerializedName("qty")
    private String Qty;
    @SerializedName("value")
    private String Value;
    @SerializedName("capacity")
    private String Capacity;
    @SerializedName("vendorCode")
    private String VendorCode;
    @SerializedName("vendorName")
    private String VendorName;
    @SerializedName("location")
    private String Location;
    @SerializedName("progress")
    private String Progress;
    @SerializedName("started")
    private String Started;
    @SerializedName("status")
    private String Status;
    @SerializedName("ended")
    private String Ended;
    @SerializedName("lastUpdated")
    private String LastUpdated;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProjectOnGoingModel() {
    }

    public String getVendorCode() {
        return VendorCode;
    }

    public void setVendorCode(String vendorCode) {
        VendorCode = vendorCode;
    }

    public String getVendorName() {
        return VendorName;
    }

    public void setVendorName(String vendorName) {
        VendorName = vendorName;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getCapacity() {
        return Capacity;
    }

    public void setCapacity(String capacity) {
        Capacity = capacity;
    }

    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public String getCompanyGroup() {
        return companyGroup;
    }

    public void setCompanyGroup(String companyGroup) {
        this.companyGroup = companyGroup;
    }

    public String getDivisi() {
        return divisi;
    }

    public void setDivisi(String divisi) {
        this.divisi = divisi;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    public String getBa() {
        return ba;
    }

    public void setBa(String ba) {
        this.ba = ba;
    }

    public String getBaDescription() {
        return baDescription;
    }

    public void setBaDescription(String baDescription) {
        this.baDescription = baDescription;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getProgress() {
        return Progress;
    }

    public void setProgress(String progress) {
        Progress = progress;
    }

    public String getStarted() {
        return Started;
    }

    public void setStarted(String started) {
        Started = started;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getEnded() {
        return Ended;
    }

    public void setEnded(String ended) {
        Ended = ended;
    }

    public String getLastUpdated() {
        return LastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        LastUpdated = lastUpdated;
    }
}
