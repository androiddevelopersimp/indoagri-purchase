package com.indoagri.purchasing.Retrofit.DB;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.widget.Toast;


import com.indoagri.purchasing.Retrofit.Model.AppDetailModel;
import com.indoagri.purchasing.Retrofit.Model.MaterialModel;
import com.indoagri.purchasing.Retrofit.Model.MaterialPrice;
import com.indoagri.purchasing.Retrofit.Model.ProjectOnGoingImagesModel;
import com.indoagri.purchasing.Retrofit.Model.ProjectOnGoingModel;
import com.indoagri.purchasing.Retrofit.Model.ProjectOnGoingProgressModel;
import com.indoagri.purchasing.Retrofit.Model.TopRankModel;
import com.indoagri.purchasing.Retrofit.Model.UserModel;
import com.indoagri.purchasing.Retrofit.Model.YTDPurchase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DatabaseQuery {
    private Context context;
    private SQLiteDatabase db;
    private DatabaseHelper helper;

    public DatabaseQuery(Context context) {
        this.context = context;
        this.helper = new DatabaseHelper(context);
    }

    public void openTransaction() throws SQLException {
        try {
            db = helper.getWritableDatabase();
            db.beginTransaction();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void closeTransaction() {
        try {
            if (db.isOpen()) {
                db.endTransaction();
                helper.close();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void commitTransaction() throws SQLException {
        try {
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public int COUNT(String TABLE, String statement) {
        int count = 0;
        db = helper.getReadableDatabase();
        Cursor cursor = null;
        String query = statement;

        cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        count = cursor.getCount();
        cursor.close();

        return count;
    }

    public void UPDATEQUERY(String TABLE, String statement) {
        db = helper.getWritableDatabase();
        db.beginTransaction();
        String query = statement;
        try {
            db.execSQL(query);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            //Error in between database transaction
        } finally {
            db.endTransaction();
        }

    }

    public void INSERTQUERY(String TABLE, String statement) {
        db = helper.getWritableDatabase();
        db.beginTransaction();
        String query = statement;
        try {
            db.execSQL(query);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.getMessage();
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            //Error in between database transaction
        } finally {
            db.endTransaction();
        }
    }

    public int insertDataSQL(String tableName, ContentValues values) {

        int result = 0;
        try {
            /*sqliteDatabase.rawQuery(query,bindArgs);*/
            db.insertOrThrow(tableName, null, values);
            result = 1;
        } catch (Exception e) {
            result = 0;
            e.printStackTrace();
        }

        return result;
    }

    public boolean DELETEEXISTING(String TABLE, String statement) {
        //openTransaction();
        db = helper.getWritableDatabase();
        db.beginTransaction();
        boolean deleteStatus = false;
        String query = statement;
        try {
            db.execSQL(query);
            deleteStatus = true;
        } catch (Exception e) {
            deleteStatus = false;
            e.getMessage();
        }
        try {
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        try {
            if (db.isOpen()) {
                db.endTransaction();
                helper.close();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return deleteStatus;
    }

    public boolean DELETETABLE(String TABLE, String statement) {
        db = helper.getWritableDatabase();
        db.beginTransaction();
        boolean in = false;
        String query = statement;
        try {
            db.execSQL(query);
            in = true;
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
            in = false;
        }
        try {
            if (db.isOpen()) {
                db.endTransaction();
                helper.close();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return in;
    }

    public int deleteDataTemporary(String tableName, String whereClause, String[] whereArgs) {

        try {
            int rowId = db.delete(tableName, whereClause, whereArgs);
            return rowId;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public void deleteDataTable(String tableName, String whereClause, String[] whereArgs) {

        try {
//            db.execSQL("DELETE FROM sqlite_sequence WHERE name= '"+tableName+"'");
            db.execSQL("DELETE FROM " + tableName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashMap<String, String> GetUserField() {
        HashMap<String, String> hard = new HashMap<String, String>();
        db = helper.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + UserModel.TABLE_NAME;
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            hard.put(UserModel.XML_ID, cursor.getString(0));
            hard.put(UserModel.XML_USERID, cursor.getString(1));
            hard.put(UserModel.XML_USERNAME, cursor.getString(2));
            hard.put(UserModel.XML_SURNAME, cursor.getString(3));
            hard.put(UserModel.XML_EMAIL, cursor.getString(4));
            hard.put(UserModel.XML_LDAP, cursor.getString(5));
            hard.put(UserModel.XML_DOMAIN, cursor.getString(6));
            hard.put(UserModel.XML_USERDESCRIPTION, cursor.getString(7));
            hard.put(UserModel.XML_COMPANY, cursor.getString(8));
            hard.put(UserModel.XML_PROVINCE, cursor.getString(9));
            hard.put(UserModel.XML_DEPARTMENT, cursor.getString(10));
            hard.put(UserModel.XML_EMPLOYEEID, cursor.getString(11));
            hard.put(UserModel.XML_USERFULLNAME, cursor.getString(12));
            hard.put(UserModel.XML_USERPHONE, cursor.getString(13));
            hard.put(UserModel.XML_USERPASSWORD, cursor.getString(14));
            hard.put(UserModel.XML_USERKEY, cursor.getString(15));
        }
        cursor.close();
        return hard;
    }


    public Object getDataFirstRaw(String sqldb_query, String tableName, String[] a) {

        List<Object> listObject = getListDataRawQuery(sqldb_query, tableName, a);

        if (listObject.size() > 0) {
            return listObject.get(0);
        } else {
            return null;
        }
    }

    public List<Object> getListDataRawQuery(String sqldb_query, String tableName, String[] a) {

        Cursor cursor = null;
        List<Object> listObject = new ArrayList<Object>();

        try {
            cursor = db.rawQuery(sqldb_query, a);
            if (tableName.equals(MaterialModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        MaterialModel sb = new MaterialModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_ROWID)) {
                                sb.setRowID(Integer.parseInt(cursor.getString(i)));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_COMPANYGROUP)) {
                                sb.setCompanyGroup(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_DIVISI)) {
                                sb.setDivisi(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_COMPANYCODE)) {
                                sb.setCompanyCode(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_COMPANYDESCRIPTION)) {
                                sb.setCompanyDescription(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_BA)) {
                                sb.setBA(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_BADESCRIPTION)) {
                                sb.setBADescription(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_MATERIALCODE)) {
                                sb.setMaterialCode(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_MATERIALGROUPCODE)) {
                                sb.setMaterialGroupCode(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_MATERIALGROUPDESCRIPTION)) {
                                sb.setMaterialGroupDescription(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_MATERIALDESCRIPTION)) {
                                sb.setMaterialDescription(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_LASTUPDATE)) {
                                sb.setLastUpdated(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_GROUPREPORT)) {
                                sb.setGroupReport(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_LASTPRICE)) {
                                sb.setLastPrice(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialModel.XML_UOM)) {
                                sb.setUom(cursor.getString(i));
                            }


                        }

                        listObject.add(sb);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(TopRankModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        TopRankModel tr = new TopRankModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(TopRankModel.XML_ROWID)) {
                                tr.setRowID(Integer.parseInt(cursor.getString(i)));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(TopRankModel.XML_COMPANYGROUP)) {
                                tr.setCompanyGroup(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(TopRankModel.XML_COMPANYCODE)) {
                                tr.setCompanyCode(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(TopRankModel.XML_MATERIALGROUP)) {
                                tr.setMaterialGroup(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(TopRankModel.XML_MATERIALCODE)) {
                                tr.setMaterialCode(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(TopRankModel.XML_TOPRANK)) {
                                tr.setTopRank(Integer.parseInt(cursor.getString(i)));
                            }
                        }
                        listObject.add(tr);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(ProjectOnGoingModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        ProjectOnGoingModel tr = new ProjectOnGoingModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_PROJECTID)) {
                                tr.setProjectID(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_DESCRIPTION)) {
                                tr.setDescription(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_COMPANYGROUP)) {
                                tr.setCompanyGroup(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_DIVISI)) {
                                tr.setDivisi(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_COMPANYCODE)) {
                                tr.setCompanyCode(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_COMPANYDESCRIPTION)) {
                                tr.setCompanyDescription(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_BA)) {
                                tr.setBa(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_BADESCRIPTION)) {
                                tr.setBaDescription(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_PO)) {
                                tr.setBaDescription(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_CAPACITY)) {
                                tr.setCapacity(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_VENDORCODE)) {
                                tr.setVendorCode(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_VENDORNAME)) {
                                tr.setVendorName(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_LOCATION)) {
                                tr.setLocation(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_QTY)) {
                                tr.setQty(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_VALUE)) {
                                tr.setValue(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_PROGRESS)) {
                                tr.setProgress(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_STARTED)) {
                                tr.setStarted(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_STATUS)) {
                                tr.setStatus(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_ENDED)) {
                                tr.setEnded(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingModel.XML_LASTUPDATE)) {
                                tr.setLastUpdated(cursor.getString(i));
                            }
                        }

                        listObject.add(tr);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(ProjectOnGoingImagesModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        ProjectOnGoingImagesModel tr = new ProjectOnGoingImagesModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingImagesModel.XML_ROWID)) {
                                tr.setRowID(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingImagesModel.XML_PROJECTID)) {
                                tr.setProjectID(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingImagesModel.XML_IMAGEURL)) {
                                tr.setImageUrl(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingImagesModel.XML_IMAGEDESCRIPTION)) {
                                tr.setImageDescription(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingImagesModel.XML_CREATEDDATE)) {
                                tr.setCreatedDate(cursor.getString(i));
                            }
                        }

                        listObject.add(tr);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(ProjectOnGoingProgressModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        ProjectOnGoingProgressModel tr = new ProjectOnGoingProgressModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingProgressModel.XML_ROWID)) {
                                tr.setRowID(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingProgressModel.XML_PROJECTID)) {
                                tr.setProjectID(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingProgressModel.XML_DESCRIPTION)) {
                                tr.setDescription(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingProgressModel.XML_BOBOT)) {
                                tr.setBobot(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingProgressModel.XML_SCHEDULE)) {
                                tr.setSchedue(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingProgressModel.XML_ACTUALWEEK84)) {
                                tr.setActualWeek84(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingProgressModel.XML_DEVIASI)) {
                                tr.setDeviasi(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(ProjectOnGoingProgressModel.XML_CREATEDDATE)) {
                                tr.setCreatedDate(cursor.getString(i));
                            }
                        }

                        listObject.add(tr);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(AppDetailModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        AppDetailModel sb = new AppDetailModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APPID)) {
                                sb.setID(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APPNAME)) {
                                sb.setAPPNAME(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APPDESC)) {
                                sb.setAPPDESCRIPTION(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APPPACKAGE)) {
                                sb.setAPPPACKAGE(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_VERSIONCODE)) {
                                sb.setVERSIONCODE(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_VERSIONNAME)) {
                                sb.setVERSIONNAME(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APKURL)) {
                                sb.setAPKURL(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_ASSETSICON)) {
                                sb.setASSETSICON(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_ASSETSICONURL)) {
                                sb.setASSETSICONURL(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_ACTIVE)) {
                                sb.setACTIVE(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_GROUPLEVEL)) {
                                sb.setGROUPLEVEL(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_USERAD)) {
                                sb.setUSERAD(cursor.getString(i));
                            }
                        }

                        listObject.add(sb);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(UserModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        UserModel sb = new UserModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERID)) {
                                sb.setUSERID(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERNAME)) {
                                sb.setUSERNAME(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_SURNAME)) {
                                sb.setSURNAME(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_EMAIL)) {
                                sb.setEMAIL(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_LDAP)) {
                                sb.setLDAP(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_DOMAIN)) {
                                sb.setDOMAIN(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERDESCRIPTION)) {
                                sb.setDESCRIPTION(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_COMPANY)) {
                                sb.setCOMPANY(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_PROVINCE)) {
                                sb.setPROVINCE(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_DEPARTMENT)) {
                                sb.setDEPARTMENT(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_EMPLOYEEID)) {
                                sb.setEMPLOYEEID(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERFULLNAME)) {
                                sb.setFULLNAME(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERPHONE)) {
                                sb.setPHONE(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERPASSWORD)) {
                                sb.setPASSWORD(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERKEY)) {
                                sb.setKEY(cursor.getString(i));
                            }
                        }

                        listObject.add(sb);
                    } while (cursor.moveToNext());
                }
            }else if (tableName.equals(MaterialPrice.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        MaterialPrice mp = new MaterialPrice();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(MaterialPrice.XML_INSERTDATE)) {
                                mp.setInsertdate(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialPrice.XML_MATGRP)) {
                                mp.setMatgrp(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialPrice.XML_AREA)) {
                                mp.setArea(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialPrice.XML_MATNR)) {
                                mp.setMatnr(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialPrice.XML_MANDT)) {
                                mp.setMandt(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialPrice.XML_MATGRPDESC)) {
                                mp.setMatgrpdesc(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialPrice.XML_LSTPRD)) {
                                mp.setLstprd(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialPrice.XML_TXZ01)) {
                                mp.setTxZ01(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialPrice.XML_LSTPO)) {
                                mp.setLstpo(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialPrice.XML_LSTPOITEM)) {
                                mp.setLstpoitem(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialPrice.XML_WAERS)) {
                                mp.setWaers(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialPrice.XML_LSTPRC)) {
                                mp.setLstprc(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialPrice.XML_LSTUOM)) {
                                mp.setLstuom(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(MaterialPrice.XML_TOTVAL)) {
                                mp.setTotval(cursor.getString(i));
                            }
                        }

                        listObject.add(mp);
                    } while (cursor.moveToNext());
                }
            }else if (tableName.equals(YTDPurchase.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        YTDPurchase ytd = new YTDPurchase();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(YTDPurchase.XML_INSERTDATE)) {
                                ytd.setInsertdate(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(YTDPurchase.XML_COMP_GRP)) {
                                ytd.setComPGRP(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(YTDPurchase.XML_MATGRP)) {
                                ytd.setMatgrp(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(YTDPurchase.XML_MANDT)) {
                                ytd.setMandt(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(YTDPurchase.XML_MATGRPDESC)) {
                                ytd.setMatgrpdesc(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(YTDPurchase.XML_LSTPRD)) {
                                ytd.setLstprd(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(YTDPurchase.XML_LSTPRDTOT)) {
                                ytd.setLstprdtot(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(YTDPurchase.XML_YTDTOT)) {
                                ytd.setYtdtot(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(YTDPurchase.XML_WAERS)) {
                                ytd.setWaers(cursor.getString(i));
                            }
                        }

                        listObject.add(ytd);
                    } while (cursor.moveToNext());
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }

        return listObject;
    }


    public long setData(Object object) {
        String tableName = null;
        ContentValues values = new ContentValues();
        if (object.getClass().getName().equals(ProjectOnGoingProgressModel.class.getName())) {
            ProjectOnGoingProgressModel mt = (ProjectOnGoingProgressModel) object;
            tableName = ProjectOnGoingProgressModel.TABLE_NAME;
            values.put(ProjectOnGoingProgressModel.XML_ROWID, mt.getRowID());
            values.put(ProjectOnGoingProgressModel.XML_PROJECTID, mt.getProjectID());
            values.put(ProjectOnGoingProgressModel.XML_DESCRIPTION, mt.getDescription());
            values.put(ProjectOnGoingProgressModel.XML_BOBOT, mt.getBobot());
            values.put(ProjectOnGoingProgressModel.XML_SCHEDULE, mt.getSchedule());
            values.put(ProjectOnGoingProgressModel.XML_ACTUALWEEK84, mt.getActualWeek84());
            values.put(ProjectOnGoingProgressModel.XML_DEVIASI, mt.getDeviasi());
            values.put(ProjectOnGoingProgressModel.XML_CREATEDDATE, mt.getCreatedDate());
        }

        long rowId = 0;

        try {
            rowId = db.insertOrThrow(tableName, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rowId;
    }


}