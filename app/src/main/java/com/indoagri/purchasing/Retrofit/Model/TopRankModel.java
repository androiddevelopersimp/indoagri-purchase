package com.indoagri.purchasing.Retrofit.Model;

import com.google.gson.annotations.SerializedName;

public class TopRankModel {
    public static String TABLE_NAME = "PURCHASE_TOPRANKS";
    public static String XML_ROWID= "ROWID";
    public static String XML_COMPANYGROUP= "COMPANYGROUP";
    public static String XML_COMPANYCODE= "COMPANYCODE";
    public static String XML_MATERIALGROUP= "MATERIALGROUP";
    public static String XML_MATERIALCODE= "MATERIALCODE";
    public static String XML_TOPRANK= "TOPRANK";


    @SerializedName("rowID")
    private int rowID;
    @SerializedName("companyGroup")
    private String CompanyGroup;
    @SerializedName("companyCode")
    private String CompanyCode;
    @SerializedName("materialGroup")
    private String MaterialGroup;
    @SerializedName("materialCode")
    private String MaterialCode;
    @SerializedName("topRank")
    private int topRank;

    public TopRankModel() {
    }

    public TopRankModel(int rowID, String companyGroup, String companyCode, String materialGroup, String materialCode, int topRank) {
        this.rowID = rowID;
        CompanyGroup = companyGroup;
        CompanyCode = companyCode;
        MaterialGroup = materialGroup;
        MaterialCode = materialCode;
        this.topRank = topRank;
    }

    public int getRowID() {
        return rowID;
    }

    public void setRowID(int rowID) {
        this.rowID = rowID;
    }

    public String getCompanyGroup() {
        return CompanyGroup;
    }

    public void setCompanyGroup(String companyGroup) {
        CompanyGroup = companyGroup;
    }

    public String getCompanyCode() {
        return CompanyCode;
    }

    public void setCompanyCode(String companyCode) {
        CompanyCode = companyCode;
    }

    public String getMaterialGroup() {
        return MaterialGroup;
    }

    public void setMaterialGroup(String materialGroup) {
        MaterialGroup = materialGroup;
    }

    public String getMaterialCode() {
        return MaterialCode;
    }

    public void setMaterialCode(String materialCode) {
        MaterialCode = materialCode;
    }

    public int getTopRank() {
        return topRank;
    }

    public void setTopRank(int topRank) {
        this.topRank = topRank;
    }
}
