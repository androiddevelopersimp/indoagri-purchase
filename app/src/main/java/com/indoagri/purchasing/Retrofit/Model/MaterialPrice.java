package com.indoagri.purchasing.Retrofit.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaterialPrice implements Parcelable {
    public static String TABLE_NAME = "MATERIAL_PRICE";
    public static String XML_INSERTDATE= "INSERTDATE";
    public static String XML_MATGRP= "MATGRP";
    public static String XML_AREA= "AREA";
    public static String XML_MATNR= "MATNR";
    public static String XML_MANDT= "MANDT";
    public static String XML_MATGRPDESC= "MATGRPDESC";
    public static String XML_LSTPRD= "LSTPRD";
    public static String XML_TXZ01= "TXZ01";
    public static String XML_LSTPO= "LSTPO";
    public static String XML_LSTPOITEM= "LSTPOITEM";
    public static String XML_WAERS= "WAERS";
    public static String XML_LSTPRC= "LSTPRC";
    public static String XML_LSTUOM= "LSTUOM";
    public static String XML_TOTVAL= "TOTVAL";

    @SerializedName("insertdate")
    @Expose
    private String insertdate;
    @SerializedName("matgrp")
    @Expose
    private String matgrp;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("matnr")
    @Expose
    private String matnr;
    @SerializedName("mandt")
    @Expose
    private String mandt;
    @SerializedName("matgrpdesc")
    @Expose
    private String matgrpdesc;
    @SerializedName("lstprd")
    @Expose
    private String lstprd;
    @SerializedName("txZ01")
    @Expose
    private String txZ01;
    @SerializedName("lstpo")
    @Expose
    private String lstpo;
    @SerializedName("lstpoitem")
    @Expose
    private String lstpoitem;
    @SerializedName("waers")
    @Expose
    private String waers;
    @SerializedName("lstprc")
    @Expose
    private String lstprc;
    @SerializedName("lstuom")
    @Expose
    private String lstuom;
    @SerializedName("totval")
    @Expose
    private String totval;


    public MaterialPrice() {
    }

    public String getInsertdate() {
        return insertdate;
    }

    public void setInsertdate(String insertdate) {
        this.insertdate = insertdate;
    }

    public String getMatgrp() {
        return matgrp;
    }

    public void setMatgrp(String matgrp) {
        this.matgrp = matgrp;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getMatnr() {
        return matnr;
    }

    public void setMatnr(String matnr) {
        this.matnr = matnr;
    }

    public String getMandt() {
        return mandt;
    }

    public void setMandt(String mandt) {
        this.mandt = mandt;
    }

    public String getMatgrpdesc() {
        return matgrpdesc;
    }

    public void setMatgrpdesc(String matgrpdesc) {
        this.matgrpdesc = matgrpdesc;
    }

    public String getLstprd() {
        return lstprd;
    }

    public void setLstprd(String lstprd) {
        this.lstprd = lstprd;
    }

    public String getTxZ01() {
        return txZ01;
    }

    public void setTxZ01(String txZ01) {
        this.txZ01 = txZ01;
    }

    public String getLstpo() {
        return lstpo;
    }

    public void setLstpo(String lstpo) {
        this.lstpo = lstpo;
    }

    public String getLstpoitem() {
        return lstpoitem;
    }

    public void setLstpoitem(String lstpoitem) {
        this.lstpoitem = lstpoitem;
    }

    public String getWaers() {
        return waers;
    }

    public void setWaers(String waers) {
        this.waers = waers;
    }

    public String getLstprc() {
        return lstprc;
    }

    public void setLstprc(String lstprc) {
        this.lstprc = lstprc;
    }

    public String getLstuom() {
        return lstuom;
    }

    public void setLstuom(String lstuom) {
        this.lstuom = lstuom;
    }

    public String getTotval() {
        return totval;
    }

    public void setTotval(String totval) {
        this.totval = totval;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.insertdate);
        dest.writeString(this.matgrp);
        dest.writeString(this.area);
        dest.writeString(this.matnr);
        dest.writeString(this.mandt);
        dest.writeString(this.matgrpdesc);
        dest.writeString(this.lstprd);
        dest.writeString(this.txZ01);
        dest.writeString(this.lstpo);
        dest.writeString(this.lstpoitem);
        dest.writeString(this.waers);
        dest.writeString(this.lstprc);
        dest.writeString(this.lstuom);
        dest.writeString(this.totval);
    }

    protected MaterialPrice(Parcel in) {
        this.insertdate = in.readString();
        this.matgrp = in.readString();
        this.area = in.readString();
        this.matnr = in.readString();
        this.mandt = in.readString();
        this.matgrpdesc = in.readString();
        this.lstprd = in.readString();
        this.txZ01 = in.readString();
        this.lstpo = in.readString();
        this.lstpoitem = in.readString();
        this.waers = in.readString();
        this.lstprc = in.readString();
        this.lstuom = in.readString();
        this.totval = in.readString();
    }

    public static final Parcelable.Creator<MaterialPrice> CREATOR = new Parcelable.Creator<MaterialPrice>() {
        @Override
        public MaterialPrice createFromParcel(Parcel source) {
            return new MaterialPrice(source);
        }

        @Override
        public MaterialPrice[] newArray(int size) {
            return new MaterialPrice[size];
        }
    };
}