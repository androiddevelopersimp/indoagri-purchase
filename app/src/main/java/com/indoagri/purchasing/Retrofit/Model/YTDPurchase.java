package com.indoagri.purchasing.Retrofit.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YTDPurchase implements Parcelable {
    public static String TABLE_NAME = "YTD_PURCHASE";
    public static String XML_INSERTDATE= "INSERTDATE";
    public static String XML_COMP_GRP= "COMP_GRP";
    public static String XML_MATGRP= "MATGRP";
    public static String XML_MANDT= "MANDT";
    public static String XML_MATGRPDESC= "MATGRPDESC";
    public static String XML_LSTPRD= "LSTPRD";
    public static String XML_LSTPRDTOT= "LSTPRDTOT";
    public static String XML_YTDTOT= "YTDTOT";
    public static String XML_WAERS= "WAERS";


    @SerializedName("insertdate")
    @Expose
    private String insertdate;
    @SerializedName("comP_GRP")
    @Expose
    private String comPGRP;
    @SerializedName("matgrp")
    @Expose
    private String matgrp;
    @SerializedName("mandt")
    @Expose
    private String mandt;
    @SerializedName("matgrpdesc")
    @Expose
    private String matgrpdesc;
    @SerializedName("lstprd")
    @Expose
    private String lstprd;
    @SerializedName("lstprdtot")
    @Expose
    private String lstprdtot;
    @SerializedName("ytdtot")
    @Expose
    private String ytdtot;
    @SerializedName("waers")
    @Expose
    private String waers;

    public String getInsertdate() {
        return insertdate;
    }

    public void setInsertdate(String insertdate) {
        this.insertdate = insertdate;
    }

    public String getComPGRP() {
        return comPGRP;
    }

    public void setComPGRP(String comPGRP) {
        this.comPGRP = comPGRP;
    }

    public String getMatgrp() {
        return matgrp;
    }

    public void setMatgrp(String matgrp) {
        this.matgrp = matgrp;
    }

    public String getMandt() {
        return mandt;
    }

    public void setMandt(String mandt) {
        this.mandt = mandt;
    }

    public String getMatgrpdesc() {
        return matgrpdesc;
    }

    public void setMatgrpdesc(String matgrpdesc) {
        this.matgrpdesc = matgrpdesc;
    }

    public String getLstprd() {
        return lstprd;
    }

    public void setLstprd(String lstprd) {
        this.lstprd = lstprd;
    }

    public String getLstprdtot() {
        return lstprdtot;
    }

    public void setLstprdtot(String lstprdtot) {
        this.lstprdtot = lstprdtot;
    }

    public String getYtdtot() {
        return ytdtot;
    }

    public void setYtdtot(String ytdtot) {
        this.ytdtot = ytdtot;
    }

    public String getWaers() {
        return waers;
    }

    public void setWaers(String waers) {
        this.waers = waers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.insertdate);
        dest.writeString(this.comPGRP);
        dest.writeString(this.matgrp);
        dest.writeString(this.mandt);
        dest.writeString(this.matgrpdesc);
        dest.writeString(this.lstprd);
        dest.writeString(this.lstprdtot);
        dest.writeString(this.ytdtot);
        dest.writeString(this.waers);
    }

    public YTDPurchase() {
    }

    protected YTDPurchase(Parcel in) {
        this.insertdate = in.readString();
        this.comPGRP = in.readString();
        this.matgrp = in.readString();
        this.mandt = in.readString();
        this.matgrpdesc = in.readString();
        this.lstprd = in.readString();
        this.lstprdtot = in.readString();
        this.ytdtot = in.readString();
        this.waers = in.readString();
    }

    public static final Parcelable.Creator<YTDPurchase> CREATOR = new Parcelable.Creator<YTDPurchase>() {
        @Override
        public YTDPurchase createFromParcel(Parcel source) {
            return new YTDPurchase(source);
        }

        @Override
        public YTDPurchase[] newArray(int size) {
            return new YTDPurchase[size];
        }
    };
}
