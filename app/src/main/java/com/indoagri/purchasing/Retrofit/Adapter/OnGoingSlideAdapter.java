package com.indoagri.purchasing.Retrofit.Adapter;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import java.util.ArrayList;
import java.util.List;

public class OnGoingSlideAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;
    public OnGoingSlideAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    public void refresh()
    {
        this.notifyDataSetChanged();
    }
    public void updatePAger(List<Fragment> newlist) {
        fragments.clear();
        fragments.addAll(newlist);
        this.notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return this.fragments.size();
    }

}
