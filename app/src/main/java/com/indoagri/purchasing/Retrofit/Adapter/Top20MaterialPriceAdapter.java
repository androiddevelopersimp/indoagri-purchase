package com.indoagri.purchasing.Retrofit.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indoagri.purchasing.R;
import com.indoagri.purchasing.Retrofit.DB.DatabaseQuery;
import com.indoagri.purchasing.Retrofit.Model.MaterialPrice;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class Top20MaterialPriceAdapter extends RecyclerView.Adapter<Top20MaterialPriceAdapter.MaterialPriceViewHolder> {

    private static ClickListener clickListener;

    Context mContext;
    private List<MaterialPrice> dataList;
    DatabaseQuery databaseQuery;

    public Top20MaterialPriceAdapter(Context mContext, List<MaterialPrice> dataList) {
        databaseQuery = new DatabaseQuery(mContext);
        this.mContext = mContext;
        this.dataList = dataList;
    }

    @Override
    public MaterialPriceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_material, parent, false);
        return new MaterialPriceViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    @Override
    public void onBindViewHolder(MaterialPriceViewHolder holder, int position) {
        MaterialPrice materialPrice = dataList.get(position);
        NumberFormat formatter = new DecimalFormat("#,###,###,###,###,###");
//        double myNumber = Double.parseDouble((materialPrice.getLastPrice()));
//        String formattedNumber = formatter.format(myNumber);
//        holder.txtCompanyGroup.setText(dataList.get(position).getMatgrp());
//        holder.txtCompanyCode.setText(dataList.get(position).getMatgrp());
        holder.txtMaterialGroup.setText(materialPrice.getMatgrpdesc());
        holder.txtMaterialCode.setText("Material : "+materialPrice.getMatnr().substring(10));
        if(materialPrice.getLstprc()!=null){
            double myNumber = Double.parseDouble((materialPrice.getLstprc()));
            String formattedNumber = formatter.format(myNumber);
            holder.txtMaterialPrice.setText("Rp. "  + formattedNumber+" / "+materialPrice.getLstuom());
        }else{
            holder.txtMaterialPrice.setText("--");
        }

        holder.txtTopRank.setText("# "+String.valueOf(position+1));
    }

    public class MaterialPriceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView txtCompanyGroup, txtCompanyCode, txtMaterialCode, txtMaterialGroup, txtMaterialPrice, txtTopRank;

        public MaterialPriceViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            txtCompanyGroup = (TextView) itemView.findViewById(R.id.txt_CompanyGroup);
            txtCompanyCode = (TextView) itemView.findViewById(R.id.txt_CompanyCode);
            txtMaterialGroup = (TextView) itemView.findViewById(R.id.txt_MaterialGroup);
            txtMaterialCode = (TextView) itemView.findViewById(R.id.txt_MaterialCode);
            txtMaterialPrice = (TextView) itemView.findViewById(R.id.txt_MaterialPrice);
            txtTopRank = (TextView) itemView.findViewById(R.id.txt_Rank);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

        void onItemLongClick(int position, View v);
    }

}
