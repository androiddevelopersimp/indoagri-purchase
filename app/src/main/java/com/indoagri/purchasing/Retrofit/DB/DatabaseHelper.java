package com.indoagri.purchasing.Retrofit.DB;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.indoagri.purchasing.Common.Constants;
import com.indoagri.purchasing.Retrofit.Model.AppDetailModel;
import com.indoagri.purchasing.Retrofit.Model.MaterialModel;
import com.indoagri.purchasing.Retrofit.Model.MaterialPrice;
import com.indoagri.purchasing.Retrofit.Model.ProjectOnGoingImagesModel;
import com.indoagri.purchasing.Retrofit.Model.ProjectOnGoingModel;
import com.indoagri.purchasing.Retrofit.Model.ProjectOnGoingProgressModel;
import com.indoagri.purchasing.Retrofit.Model.TopRankModel;
import com.indoagri.purchasing.Retrofit.Model.UserModel;
import com.indoagri.purchasing.Retrofit.Model.YTDPurchase;


/**
 * Created by Dwi.Fajar on 4/2/2018.
 */


public class DatabaseHelper extends SQLiteOpenHelper {

    public final static int dbVersion = Constants.db_version;
    public final static String dbName = Constants.db_name;
    Context context;
    SQLiteDatabase db;
    public DatabaseHelper(Context context) {
        super(context, dbName, null, dbVersion);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query;
        query = "create table " + UserModel.TABLE_NAME+
                "(" +
                UserModel.XML_ID+" text PRIMARY KEY, " +
                UserModel.XML_USERID+ " text, "+
                UserModel.XML_USERNAME+ " text, "+
                UserModel.XML_SURNAME+ " text, "+
                UserModel.XML_EMAIL+ " text, "+
                UserModel.XML_LDAP+ " text, "+
                UserModel.XML_DOMAIN+ " text, "+
                UserModel.XML_USERDESCRIPTION+ " text, "+
                UserModel.XML_COMPANY+ " text, "+
                UserModel.XML_PROVINCE+ " text, "+
                UserModel.XML_DEPARTMENT+ " text, "+
                UserModel.XML_EMPLOYEEID+ " text, "+
                UserModel.XML_USERFULLNAME+ " text,"+
                UserModel.XML_USERPHONE+ " text, "+
                UserModel.XML_USERPASSWORD+ " text, "+
                UserModel.XML_USERKEY+ " text "+
                ")";
        db.execSQL(query);

        query = "create table " + TopRankModel.TABLE_NAME+
                "(" +
                TopRankModel.XML_ROWID+" int PRIMARY KEY, " +
                TopRankModel.XML_COMPANYGROUP+ " text, "+
                TopRankModel.XML_COMPANYCODE+ " text, "+
                TopRankModel.XML_MATERIALGROUP+ " text, "+
                TopRankModel.XML_MATERIALCODE+ " text, "+
                TopRankModel.XML_TOPRANK+ " text "+
                ")";
        db.execSQL(query);

        query = "create table " + MaterialModel.TABLE_NAME+
                "(" +
                MaterialModel.XML_ROWID+" int PRIMARY KEY, " +
                MaterialModel.XML_COMPANYGROUP+ " text, "+
                MaterialModel.XML_DIVISI+ " text, "+
                MaterialModel.XML_COMPANYCODE+ " text, "+
                MaterialModel.XML_COMPANYDESCRIPTION+ " text, "+
                MaterialModel.XML_BA+ " text, "+
                MaterialModel.XML_BADESCRIPTION+ " text, "+
                MaterialModel.XML_MATERIALCODE+ " text, "+
                MaterialModel.XML_MATERIALGROUPCODE+ " text, "+
                MaterialModel.XML_MATERIALGROUPDESCRIPTION+ " text, "+
                MaterialModel.XML_MATERIALDESCRIPTION+ " text, "+
                MaterialModel.XML_LASTUPDATE+ " text, "+
                MaterialModel.XML_GROUPREPORT+ " text, "+
                MaterialModel.XML_LASTPRICE+ " text, "+
                MaterialModel.XML_UOM+ " text "+
                ")";
        db.execSQL(query);
        query = "create table " + ProjectOnGoingModel.TABLE_NAME+
                "(" +
                ProjectOnGoingModel.XML_PROJECTID+" text PRIMARY KEY, " +
                ProjectOnGoingModel.XML_DESCRIPTION+" text, "+
                ProjectOnGoingModel.XML_COMPANYGROUP+ " text, "+
                ProjectOnGoingModel.XML_DIVISI+ " text, "+
                ProjectOnGoingModel.XML_COMPANYCODE+ " text, "+
                ProjectOnGoingModel.XML_COMPANYDESCRIPTION+ " text, "+
                ProjectOnGoingModel.XML_BA+ " text, "+
                ProjectOnGoingModel.XML_BADESCRIPTION+ " text, "+
                ProjectOnGoingModel.XML_PO+ " text, "+
                ProjectOnGoingModel.XML_CAPACITY+ " text, "+
                ProjectOnGoingModel.XML_VENDORCODE+ " text, "+
                ProjectOnGoingModel.XML_VENDORNAME+ " text, "+
                ProjectOnGoingModel.XML_LOCATION+ " text, "+
                ProjectOnGoingModel.XML_QTY+ " text, "+
                ProjectOnGoingModel.XML_VALUE+ " text, "+
                ProjectOnGoingModel.XML_PROGRESS+ " text, "+
                ProjectOnGoingModel.XML_STARTED+ " text, "+
                ProjectOnGoingModel.XML_STATUS+ " text, "+
                ProjectOnGoingModel.XML_ENDED+ " text, "+
                ProjectOnGoingModel.XML_LASTUPDATE+ " text "+
                ")";
        db.execSQL(query);

        query = "create table " + ProjectOnGoingProgressModel.TABLE_NAME+
                "(" +
                ProjectOnGoingProgressModel.XML_ROWID+" text PRIMARY KEY, " +
                ProjectOnGoingProgressModel.XML_PROJECTID+ " text, "+
                ProjectOnGoingProgressModel.XML_DESCRIPTION+ " text, "+
                ProjectOnGoingProgressModel.XML_BOBOT+ " text, "+
                ProjectOnGoingProgressModel.XML_SCHEDULE+ " text, "+
                ProjectOnGoingProgressModel.XML_ACTUALWEEK84+ " text, "+
                ProjectOnGoingProgressModel.XML_DEVIASI+ " text, "+
                ProjectOnGoingProgressModel.XML_CREATEDDATE+ " text "+
                ")";
        db.execSQL(query);
        query = "create table " + ProjectOnGoingImagesModel.TABLE_NAME+
                "(" +
                ProjectOnGoingImagesModel.XML_ROWID+" text PRIMARY KEY, " +
                ProjectOnGoingImagesModel.XML_PROJECTID+ " text, "+
                ProjectOnGoingImagesModel.XML_IMAGEURL+ " text, "+
                ProjectOnGoingImagesModel.XML_IMAGEDESCRIPTION+ " text, "+
                ProjectOnGoingImagesModel.XML_CREATEDDATE+ " text "+
                ")";
        db.execSQL(query);
        query = "create table " + AppDetailModel.TABLE_NAME+
                "(" +
                AppDetailModel.XML_APPID+" text, " +
                AppDetailModel.XML_APPNAME+ " text, "+
                AppDetailModel.XML_APPDESC+ " text,"+
                AppDetailModel.XML_APPPACKAGE+ " text, "+
                AppDetailModel.XML_VERSIONCODE+ " text,"+
                AppDetailModel.XML_VERSIONNAME+ " text, "+
                AppDetailModel.XML_APKURL+ " text,"+
                AppDetailModel.XML_ASSETSICON+ " text, "+
                AppDetailModel.XML_ASSETSICONURL+ " text,"+
                AppDetailModel.XML_ACTIVE+ " text, "+
                AppDetailModel.XML_GROUPLEVEL+ " text,"+
                AppDetailModel.XML_USERAD+ " text "+
                ")";
        db.execSQL(query);
        query = "create table " + MaterialPrice.TABLE_NAME+
                "(" +
                MaterialPrice.XML_INSERTDATE+" text PRIMARY KEY, " +
                MaterialPrice.XML_MATGRP+ " text, "+
                MaterialPrice.XML_AREA+ " text,"+
                MaterialPrice.XML_MATNR+ " text, "+
                MaterialPrice.XML_MANDT+ " text,"+
                MaterialPrice.XML_MATGRPDESC+ " text, "+
                MaterialPrice.XML_LSTPRD+ " text,"+
                MaterialPrice.XML_TXZ01+ " text, "+
                MaterialPrice.XML_LSTPO+ " text,"+
                MaterialPrice.XML_LSTPOITEM+ " text, "+
                MaterialPrice.XML_WAERS+ " text,"+
                MaterialPrice.XML_LSTPRC+ " text, "+
                MaterialPrice.XML_LSTUOM+ " text, "+
                MaterialPrice.XML_TOTVAL+ " text "+
                ")";
        db.execSQL(query);
        query = "create table " + YTDPurchase.TABLE_NAME+
                "(" +
                YTDPurchase.XML_INSERTDATE+" text PRIMARY KEY, " +
                YTDPurchase.XML_COMP_GRP+ " text, "+
                YTDPurchase.XML_MATGRP+ " text,"+
                YTDPurchase.XML_MANDT+ " text,"+
                YTDPurchase.XML_MATGRPDESC+ " text, "+
                YTDPurchase.XML_LSTPRD+ " text,"+
                YTDPurchase.XML_LSTPRDTOT+ " text, "+
                YTDPurchase.XML_YTDTOT+ " text,"+
                YTDPurchase.XML_WAERS+ " text"+
                ")";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionOld, int versionNew) {
        String query;
        try{
            switch(versionOld){
                case 14:
                    // TAMBAH TABEL MATERIAL PRICE
                    query = "create table " + MaterialPrice.TABLE_NAME+
                            "(" +
                            MaterialPrice.XML_INSERTDATE+" text PRIMARY KEY, " +
                            MaterialPrice.XML_MATGRP+ " text, "+
                            MaterialPrice.XML_AREA+ " text,"+
                            MaterialPrice.XML_MATNR+ " text, "+
                            MaterialPrice.XML_MANDT+ " text,"+
                            MaterialPrice.XML_MATGRPDESC+ " text, "+
                            MaterialPrice.XML_LSTPRD+ " text,"+
                            MaterialPrice.XML_TXZ01+ " text, "+
                            MaterialPrice.XML_LSTPO+ " text,"+
                            MaterialPrice.XML_LSTPOITEM+ " text, "+
                            MaterialPrice.XML_WAERS+ " text,"+
                            MaterialPrice.XML_LSTPRC+ " text, "+
                            MaterialPrice.XML_LSTUOM+ " text, "+
                            MaterialPrice.XML_TOTVAL+ " text "+
                            ")";
                    db.execSQL(query);
                    query = "create table " + YTDPurchase.TABLE_NAME+
                            "(" +
                            YTDPurchase.XML_INSERTDATE+" text PRIMARY KEY, " +
                            YTDPurchase.XML_COMP_GRP+ " text, "+
                            YTDPurchase.XML_MATGRP+ " text,"+
                            YTDPurchase.XML_MANDT+ " text,"+
                            YTDPurchase.XML_MATGRPDESC+ " text, "+
                            YTDPurchase.XML_LSTPRD+ " text,"+
                            YTDPurchase.XML_LSTPRDTOT+ " text, "+
                            YTDPurchase.XML_YTDTOT+ " text,"+
                            YTDPurchase.XML_WAERS+ " text"+
                            ")";
                    db.execSQL(query);
                    break;
            }

        }catch(SQLiteException e){
            e.printStackTrace();
        }
    }


}