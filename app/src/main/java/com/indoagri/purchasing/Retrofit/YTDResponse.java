package com.indoagri.purchasing.Retrofit;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.purchasing.Retrofit.Model.MaterialModel;
import com.indoagri.purchasing.Retrofit.Model.MaterialPrice;
import com.indoagri.purchasing.Retrofit.Model.YTDPurchase;

import java.util.ArrayList;
import java.util.List;

public class YTDResponse {

    @SerializedName("ytdPurchase")
    @Expose
    ArrayList<YTDPurchase> ytdPurchaseList;

    public ArrayList<YTDPurchase> getYTDPurchaseList() {
        return ytdPurchaseList;
    }
}
