package com.indoagri.purchasing.Retrofit;

import android.content.Context;


import com.indoagri.purchasing.Apps;
import com.indoagri.purchasing.Retrofit.Model.MaterialModel;
import com.indoagri.purchasing.Retrofit.Model.UserModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface  ApiServices {
    Context context = Apps.getAppContext();
    @FormUrlEncoded
    @POST("account/loginpermission")
    Call<UsersLoginResponse> LoginPermission(@Header("X-API-KEY") String xapikey,
                                             @Field("userName") String username,
                                             @Field("userPassword") String password,
                                             @Field("Domain") String domain,
                                             @Field("DeviceID") String deviceid);

    @FormUrlEncoded
    @POST("account/logoutpermission")
    Call<UsersLoginResponse> LogoutPermission(@Header("X-API-KEY") String xapikey,
                                              @Field("userName") String username,
                                              @Field("userPassword") String password,
                                              @Field("Domain") String domain,
                                              @Field("DeviceID") String deviceid);
    @GET("mipmaterial/all")
    Call<MaterialResponse> getMaterials();
    @GET("miptoprank/all")
    Call<TopRankResponse> getRanks();
    @GET("mipongoing/all")
    Call<OnGoingResponse> getOnGoingProject();
    @GET("mipongoing/allimages")
    Call<OnGoingResponse> getOnGoingProjectImages();
    @GET("mipongoing/allprogress")
    Call<OnGoingResponse> getOnGoingProjectProgress();
    @GET("permission/checkapp")
    Call<AppDetailResponse> getAppDetail(@Query("userad") String userad,
                                         @Query("app") String app);

    // NEW //
    @GET("mipytd/sortirall")
    Call<YTDResponse> getYTDPURCHASE();
    @GET("mipmaterial/sortirall")
    Call<MaterialResponse> getMaterialPrice();

    // NEW //
    @GET("mip/ytd-search")
    Call<YTDResponse> GETDATAYTD(@Query("year") String year);
    @GET("mip/material-search")
    Call<MaterialResponse> GETDATAMATPRC(@Query("year") String year);
}
