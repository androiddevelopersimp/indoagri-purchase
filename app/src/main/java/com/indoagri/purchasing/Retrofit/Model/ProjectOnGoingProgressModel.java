package com.indoagri.purchasing.Retrofit.Model;

import com.google.gson.annotations.SerializedName;

public class ProjectOnGoingProgressModel {
    public static String TABLE_NAME = "PURCHASE_ONGOING_PROGRESS";
    public static String XML_ROWID= "ROWID";
    public static String XML_PROJECTID= "PROJECTID";
    public static String XML_DESCRIPTION= "DESCRIPTION";
    public static String XML_BOBOT= "BOBOT";
    public static String XML_SCHEDULE= "SCHEDULE";
    public static String XML_ACTUALWEEK84= "ACTUALWEEK84";
    public static String XML_DEVIASI= "DEVIASI";
    public static String XML_CREATEDDATE= "CREATEDDATE";

    @SerializedName("rowID")
    private String rowID;
    @SerializedName("projectID")
    private String projectID;
    @SerializedName("description")
    private String Description;
    @SerializedName("bobot")
    private String Bobot;
    @SerializedName("schedule")
    private String Schedule;
    @SerializedName("actualWeek84")
    private String ActualWeek84;
    @SerializedName("deviasi")
    private String Deviasi;
    @SerializedName("createdDate")
    private String CreatedDate;


    public ProjectOnGoingProgressModel() {
    }

    public String getRowID() {
        return rowID;
    }

    public void setRowID(String rowID) {
        this.rowID = rowID;
    }

    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getBobot() {
        return Bobot;
    }

    public void setBobot(String bobot) {
        Bobot = bobot;
    }

    public String getSchedule() {
        return Schedule;
    }

    public void setSchedue(String schedule) {
        Schedule = schedule;
    }

    public String getActualWeek84() {
        return ActualWeek84;
    }

    public void setActualWeek84(String actualWeek84) {
        ActualWeek84 = actualWeek84;
    }

    public String getDeviasi() {
        return Deviasi;
    }

    public void setDeviasi(String deviasi) {
        Deviasi = deviasi;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }
}
