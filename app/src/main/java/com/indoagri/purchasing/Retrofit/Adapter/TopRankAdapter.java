package com.indoagri.purchasing.Retrofit.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indoagri.purchasing.R;
import com.indoagri.purchasing.Retrofit.DB.DatabaseQuery;
import com.indoagri.purchasing.Retrofit.Model.MaterialModel;
import com.indoagri.purchasing.Retrofit.Model.TopRankModel;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class TopRankAdapter extends RecyclerView.Adapter<TopRankAdapter.TopRankViewHolder> {
    private static ClickListener clickListener;

    Context mContext;
    private List<TopRankModel> dataList;
    DatabaseQuery databaseQuery;

    public TopRankAdapter(Context mContext, List<TopRankModel> dataList) {
        databaseQuery = new DatabaseQuery(mContext);
        this.mContext = mContext;
        this.dataList = dataList;
    }

    @Override
    public TopRankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_material, parent, false);
        return new TopRankViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TopRankViewHolder holder, int position) {
        databaseQuery.openTransaction();
        String[] a = new String[1];
        a[0] = dataList.get(position).getMaterialCode();
        String query = "select * from PURCHASE_MATERIAL where MATERIALCODE IN(select  MATERIALCODE from PURCHASE_TOPRANKS where MATERIALCODE=?)";
        Object object = databaseQuery.getDataFirstRaw(query, MaterialModel.TABLE_NAME, a);
        databaseQuery.closeTransaction();
        MaterialModel materialModel = (MaterialModel) object;
        NumberFormat formatter = new DecimalFormat("#,###,###,###,###,###");

        if (materialModel != null) {
            if (materialModel.getMaterialDescription() != null) {
                double myNumber = Double.parseDouble((materialModel.getLastPrice()));
                String formattedNumber = formatter.format(myNumber);
                holder.txtCompanyGroup.setText(dataList.get(position).getCompanyGroup());
                holder.txtCompanyCode.setText(dataList.get(position).getCompanyCode());
                holder.txtMaterialGroup.setText(dataList.get(position).getMaterialGroup());
                holder.txtMaterialCode.setText(materialModel.getMaterialDescription());
                holder.txtMaterialPrice.setText("Rp. " + formattedNumber + " / " + materialModel.getUom());
                holder.txtTopRank.setText("# " + String.valueOf(dataList.get(position).getTopRank()));
            } else {
                double myNumber = Double.parseDouble((materialModel.getLastPrice()));
                String formattedNumber = formatter.format(myNumber);
                holder.txtCompanyGroup.setText(dataList.get(position).getCompanyGroup());
                holder.txtCompanyCode.setText(dataList.get(position).getCompanyCode());
                holder.txtMaterialGroup.setText(dataList.get(position).getMaterialGroup());
                holder.txtMaterialCode.setText("No Name" + " - " + materialModel.getLastPrice());
                holder.txtMaterialPrice.setText("Rp. " + formattedNumber + " / " + materialModel.getUom());
                holder.txtTopRank.setText("# " + String.valueOf(dataList.get(position).getTopRank()));
            }

        }

    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class TopRankViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView txtCompanyGroup, txtCompanyCode, txtMaterialCode, txtMaterialGroup, txtMaterialPrice, txtTopRank;

        public TopRankViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            txtCompanyGroup = (TextView) itemView.findViewById(R.id.txt_CompanyGroup);
            txtCompanyCode = (TextView) itemView.findViewById(R.id.txt_CompanyCode);
            txtMaterialGroup = (TextView) itemView.findViewById(R.id.txt_MaterialGroup);
            txtMaterialCode = (TextView) itemView.findViewById(R.id.txt_MaterialCode);
            txtMaterialPrice = (TextView) itemView.findViewById(R.id.txt_MaterialPrice);
            txtTopRank = (TextView) itemView.findViewById(R.id.txt_Rank);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        TopRankAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

        void onItemLongClick(int position, View v);
    }

}