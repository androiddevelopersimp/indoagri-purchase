package com.indoagri.purchasing.Retrofit.Model;

import com.google.gson.annotations.SerializedName;

public class ProjectOnGoingImagesModel {

    public static String TABLE_NAME = "PURCHASE_ONGOING_IMAGES";
    public static String XML_ROWID= "ROWID";
    public static String XML_PROJECTID= "PROJECTID";
    public static String XML_IMAGEURL= "IMAGEURL";
    public static String XML_IMAGEDESCRIPTION= "IMAGEDESCRIPTION";
    public static String XML_CREATEDDATE= "CREATEDDATE";


    @SerializedName("rowID")
    private String rowID;
    @SerializedName("projectID")
    private String projectID;
    @SerializedName("imageUrl")
    private String imageUrl;
    @SerializedName("imageDescription")
    private String imageDescription;
    @SerializedName("createdDate")
    private String createdDate;


    public ProjectOnGoingImagesModel() {
    }

    public ProjectOnGoingImagesModel(String rowID, String projectID, String imageUrl, String imageDescription, String createdDate) {
        this.rowID = rowID;
        this.projectID = projectID;
        this.imageUrl = imageUrl;
        this.imageDescription = imageDescription;
        this.createdDate = createdDate;
    }


    public String getRowID() {
        return rowID;
    }

    public void setRowID(String rowID) {
        this.rowID = rowID;
    }

    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
