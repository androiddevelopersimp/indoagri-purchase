package com.indoagri.purchasing.Retrofit.Adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.indoagri.purchasing.R;
import com.indoagri.purchasing.Retrofit.Model.MaterialPrice;

import java.util.ArrayList;
import java.util.List;

public class ListMatGroupAdapter extends ArrayAdapter<MaterialPrice> {

    List<MaterialPrice> materialPriceList,lst_temp;
    Context context;
    int layout;

    public ListMatGroupAdapter(Context context, int layout, List<MaterialPrice> materialPrice) {
        super(context, layout, materialPrice);
        this.context=context;
        this.layout=layout;
        this.materialPriceList=materialPrice;
    }


    @Override
    public View getView(int position, View v, ViewGroup parent) {
        MaterialPrice mat=materialPriceList.get(position);
        StudentHolder holder;
        if(v==null){
            LayoutInflater vi=((Activity)context).getLayoutInflater();
            //  v=vi.inflate(layout, parent,false);
            v = vi.inflate(layout, null);
            holder=new StudentHolder();
            holder.imageView=(ImageView) v.findViewById(R.id.imgClick);
            holder.code=(TextView) v.findViewById(R.id.txtItemCode);
            holder.code.setVisibility(View.GONE);
            holder.name=(TextView) v.findViewById(R.id.txtItemName);
            v.setTag(holder);
        }
        else{
            holder=(StudentHolder) v.getTag();
            holder.imageView=(ImageView) v.findViewById(R.id.imgClick);
            holder.code=(TextView) v.findViewById(R.id.txtItemCode);
            holder.code.setVisibility(View.GONE);
            holder.name=(TextView) v.findViewById(R.id.txtItemName);
        }
        holder.code.setText(mat.getMatgrpdesc()+" | ");
        holder.name.setText(mat.getMatgrpdesc());
        return v;
    }

    static class StudentHolder{
        ImageView imageView;
        TextView code;
        TextView name;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    materialPriceList = lst_temp;
                } else {
                    if(lst_temp == null){
                        lst_temp = new ArrayList<MaterialPrice>(materialPriceList);
                    }
                    List<MaterialPrice> filteredList = new ArrayList<>();
                    for (MaterialPrice row : lst_temp) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getMatgrpdesc().toLowerCase().contains(charString.toLowerCase()) || row.getMatgrpdesc().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    materialPriceList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = materialPriceList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                //materialPriceList = (ArrayList<MaterialPrice>) filterResults.values;
                materialPriceList = (ArrayList<MaterialPrice>)filterResults.values;
                notifyDataSetChanged();
                clear();
                int count = materialPriceList.size();
                for(int i = 0; i<count; i++){
                    add(materialPriceList.get(i));
                    notifyDataSetInvalidated();
                }
            }
        };
    }
    public void addData(MaterialPrice estateModel){
        boolean found = false;

        for(int i = 0; i < materialPriceList.size(); i++){
            if(estateModel.getMatgrpdesc().equalsIgnoreCase(materialPriceList.get(i).getMatgrpdesc())){
                found = true;
                break;
            }
        }

        if(!found){
            materialPriceList.add(estateModel);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return materialPriceList.size();
    }


    @Override
    public long getItemId(int pos) {
        return pos;
    }

}