package com.indoagri.purchasing.Retrofit.Adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.indoagri.purchasing.R;
import com.indoagri.purchasing.Retrofit.Model.MaterialPrice;
import com.indoagri.purchasing.Retrofit.Model.YTDPurchase;

import java.util.ArrayList;
import java.util.List;

public class ListYearYTDAdapter extends ArrayAdapter<YTDPurchase> {

    List<YTDPurchase> ytdPurchaseList,lst_temp;
    Context context;
    int layout;

    public ListYearYTDAdapter(Context context, int layout, List<YTDPurchase> ytdPurchaseList) {
        super(context, layout, ytdPurchaseList);
        this.context=context;
        this.layout=layout;
        this.ytdPurchaseList=ytdPurchaseList;
    }


    @Override
    public View getView(int position, View v, ViewGroup parent) {
        YTDPurchase mat=ytdPurchaseList.get(position);
        StudentHolder holder;
        if(v==null){
            LayoutInflater vi=((Activity)context).getLayoutInflater();
            //  v=vi.inflate(layout, parent,false);
            v = vi.inflate(layout, null);
            holder=new StudentHolder();
            holder.imageView=(ImageView) v.findViewById(R.id.imgClick);
            holder.code=(TextView) v.findViewById(R.id.txtItemCode);
            holder.code.setVisibility(View.GONE);
            holder.name=(TextView) v.findViewById(R.id.txtItemName);
            v.setTag(holder);
        }
        else{
            holder=(StudentHolder) v.getTag();
            holder.imageView=(ImageView) v.findViewById(R.id.imgClick);
            holder.code=(TextView) v.findViewById(R.id.txtItemCode);
            holder.code.setVisibility(View.GONE);
            holder.name=(TextView) v.findViewById(R.id.txtItemName);
        }
        holder.code.setText(String.valueOf(ytdPurchaseList.get(position)));
        holder.name.setText(mat.getLstprd().substring(0,4));
        return v;
    }

    static class StudentHolder{
        ImageView imageView;
        TextView code;
        TextView name;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    ytdPurchaseList = lst_temp;
                } else {
                    if(lst_temp == null){
                        lst_temp = new ArrayList<YTDPurchase>(ytdPurchaseList);
                    }
                    List<YTDPurchase> filteredList = new ArrayList<>();
                    for (YTDPurchase row : lst_temp) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getLstprd().toLowerCase().contains(charString.toLowerCase()) || row.getLstprd().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    ytdPurchaseList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = ytdPurchaseList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                //materialPriceList = (ArrayList<MaterialPrice>) filterResults.values;
                ytdPurchaseList = (ArrayList<YTDPurchase>)filterResults.values;
                notifyDataSetChanged();
                clear();
                int count = ytdPurchaseList.size();
                for(int i = 0; i<count; i++){
                    add(ytdPurchaseList.get(i));
                    notifyDataSetInvalidated();
                }
            }
        };
    }
    public void addData(YTDPurchase estateModel){
        boolean found = false;

        for(int i = 0; i < ytdPurchaseList.size(); i++){
            if(estateModel.getMatgrpdesc().equalsIgnoreCase(ytdPurchaseList.get(i).getLstprd())){
                found = true;
                break;
            }
        }

        if(!found){
            ytdPurchaseList.add(estateModel);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return ytdPurchaseList.size();
    }


    @Override
    public long getItemId(int pos) {
        return pos;
    }

}