package com.indoagri.purchasing.Fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.indoagri.purchasing.R;
import com.indoagri.purchasing.Retrofit.DB.DatabaseQuery;
import com.indoagri.purchasing.Retrofit.Model.ProjectOnGoingModel;

import java.text.DecimalFormat;
import java.text.NumberFormat;


public class DialogOnGoing extends DialogFragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private boolean mActivity;
    NumberFormat formatterValue = new DecimalFormat("#,###,###,###,###,###");
    private OnFragmentInteractionListener mListener;
    TextView txtProjectID,txtCompanyGroup,txtCompanyDescription,txtPO,txtBA,txtQty,txtDivisi,txtValue,txtProgress,txtStarted,txtStatus;
    DatabaseQuery query;
    ImageView imgClose;
    Button btnClose;
    View view;
    public DialogOnGoing() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static DialogOnGoing newInstance(Bundle bundle) {
        Bundle b =bundle;
        String Params1Received =  b.getString("params1");
        String Params2Received =  b.getString("params2");
        boolean activitystatus =  b.getBoolean("activity");
        DialogOnGoing fragment = new DialogOnGoing();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, Params1Received);
        args.putString(ARG_PARAM2,Params2Received);
        args.putBoolean(ARG_PARAM3,activitystatus);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        query = new DatabaseQuery(getContext());
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mActivity = getArguments().getBoolean(ARG_PARAM3);
        }
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.TransparentDialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Dialog customDialog;
        view = inflater.inflate(R.layout.dialog_ongoingdetail, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Baca view yang dibuat di XML
        txtProjectID = (TextView) view.findViewById(R.id.projectID);
        txtDivisi = (TextView) view.findViewById(R.id.divisi);
        txtCompanyGroup = (TextView) view.findViewById(R.id.companyGroup);
        txtBA = (TextView) view.findViewById(R.id.BA);
        txtQty = (TextView) view.findViewById(R.id.Qty);
        txtValue = (TextView) view.findViewById(R.id.Value);
        txtProgress = (TextView) view.findViewById(R.id.Progress);
        txtStarted = (TextView) view.findViewById(R.id.Start);
        txtStatus = (TextView) view.findViewById(R.id.Status);
        txtPO = (TextView) view.findViewById(R.id.PO);
        txtCompanyDescription = (TextView) view.findViewById(R.id.companyDescription);
        imgClose = (ImageView)view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(this);
        query.openTransaction();
        String[] a = new String[1];
        a[0] = mParam1;
        String queryString = "select * from PURCHASE_ONGOING where PROJECTID=?";
        Object object = query.getDataFirstRaw(queryString, ProjectOnGoingModel.TABLE_NAME,a);
        query.closeTransaction();
        ProjectOnGoingModel onGoingModel = (ProjectOnGoingModel)object;
        txtProjectID.setText(onGoingModel.getProjectID());
        txtDivisi.setText(onGoingModel.getDivisi());
        txtCompanyGroup.setText(onGoingModel.getCompanyGroup());
        txtBA.setText(onGoingModel.getBaDescription());
        txtPO.setText(onGoingModel.getPoNumber());
        txtProgress.setText(onGoingModel.getProgress());
        txtQty.setText(onGoingModel.getQty());
        double valD = Double.parseDouble(onGoingModel.getValue());
        String ValueRp = formatterValue.format(valD);
        txtValue.setText(ValueRp);
        txtStatus.setText(onGoingModel.getStatus());
        btnClose = view.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(this);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri,Bundle bundle) {
        if (mListener != null) {
            mListener.onFragmentInteractionDialogSingle(uri,bundle);
        }
        dismiss();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if(v==imgClose){
            Bundle bundle = new Bundle();
            bundle.putString("projectID",mParam1);
            onButtonPressed(null,bundle);
        }
        if(v==btnClose){
            Bundle bundle = new Bundle();
            bundle.putString("projectID",mParam1);
            onButtonPressed(null,bundle);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteractionDialogSingle(Uri uri,Bundle bundle);
    }

}
