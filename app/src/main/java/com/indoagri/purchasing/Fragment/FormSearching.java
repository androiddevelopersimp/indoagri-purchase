package com.indoagri.purchasing.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.indoagri.purchasing.R;
import com.indoagri.purchasing.Retrofit.DB.DatabaseQuery;
import com.indoagri.purchasing.Retrofit.Model.MaterialModel;
import com.indoagri.purchasing.Retrofit.SharePreference;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FormSearching.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FormSearching#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FormSearching extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View views;
    private OnFragmentInteractionListener mListener;
    Button btnSearch;
    DatabaseQuery query;
    Spinner spinnerGroupCompany;
    Spinner spinnerPT;
    Spinner spinnerBA;
    Spinner spinnerGroupReport;
    Spinner spinnerDivisi;

    public FormSearching() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FormSearching.
     */
    // TODO: Rename and change types and number of parameters
    public static FormSearching newInstance(String param1, String param2) {
        FormSearching fragment = new FormSearching();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        query = new DatabaseQuery(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        views = inflater.inflate(R.layout.fragment_form_searching, container, false);
        btnSearch = views.findViewById(R.id.btn_search);
        btnSearch.setOnClickListener(this);
        spinnerGroupCompany = views.findViewById(R.id.spinGroupCompany);
        spinnerPT = views.findViewById(R.id.spinPT);
        spinnerDivisi = views.findViewById(R.id.spinDivisi);
        spinnerBA = views.findViewById(R.id.spinBA);
        spinnerGroupReport = views.findViewById(R.id.spinGroupReport);
        // Inflate the layout for this fragment
        DataSourceGroupCompany();
        DataSourceDivisi();
        DataSourceGroupReport();
        return views;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String year) {
        if (mListener != null) {
            mListener.onFragmentInteraction(year);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if(v==btnSearch){
            getActivity().onBackPressed();
            onButtonPressed( new SharePreference(getActivity()).isFormYear());
        }
    }
    private void setGroupCompany(List<Object>list){
        List<String> company = new ArrayList<String>();
        for(int i=0;i<list.size();i++){
            MaterialModel materialModel =  (MaterialModel) list.get(i);
            company.add(materialModel.getCompanyGroup());
        }
        ArrayAdapter<String> dtCOmpany = new ArrayAdapter<String>(getActivity(), R.layout.item_spinner_sales, company);
        dtCOmpany.setDropDownViewResource(R.layout.spinner_dropdown);
        // attaching data adapter to spinner
        spinnerGroupCompany.setAdapter(dtCOmpany);
        spinnerGroupCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                String dataCompany = parentView1.getItemAtPosition(position).toString();
                new SharePreference(getActivity()).setFormGroupcompany(dataCompany);
                DataSourcePT(dataCompany);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
    private void setDivisi(List<Object>list){
        List<String> company = new ArrayList<String>();
        for(int i=0;i<list.size();i++){

            MaterialModel materialModel =  (MaterialModel) list.get(i);
            company.add(materialModel.getDivisi());
        }
        ArrayAdapter<String> dtCOmpany = new ArrayAdapter<String>(getActivity(), R.layout.item_spinner_sales, company);
        dtCOmpany.setDropDownViewResource(R.layout.spinner_dropdown);
        // attaching data adapter to spinner
        spinnerDivisi.setAdapter(dtCOmpany);
        spinnerDivisi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                String datadivisi = parentView1.getItemAtPosition(position).toString();
                new SharePreference(getActivity()).setFormDivisi(datadivisi);
                if(datadivisi.equalsIgnoreCase("Ref")){
                    spinnerBA.setEnabled(true);
                    DataSourceBA();
                }if(datadivisi.equalsIgnoreCase("Plant")){
                    spinnerBA.setEnabled(false);
                    spinnerBA.setAdapter(null);
                    new SharePreference(getActivity()).setBA(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
    private void setSpinnerGroupReport(List<Object>list){
        List<String> groupreport = new ArrayList<String>();
        for(int i=0;i<list.size();i++){

            MaterialModel materialModel =  (MaterialModel) list.get(i);
            groupreport.add(materialModel.getGroupReport());
        }
        ArrayAdapter<String> dtCOmpany = new ArrayAdapter<String>(getActivity(), R.layout.item_spinner_sales, groupreport);
        dtCOmpany.setDropDownViewResource(R.layout.spinner_dropdown);
        // attaching data adapter to spinner
        spinnerGroupReport.setAdapter(dtCOmpany);
        spinnerGroupReport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                String matgroup = parentView1.getItemAtPosition(position).toString();
                //new SharePreference(getActivity()).setFormCompany(dataCompany);
                    String name = matgroup;
                    new SharePreference(getActivity()).setFormMatgroup(name);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
    private void setSpinnerPT(List<Object>list){
        List<String> company = new ArrayList<String>();
        for(int i=0;i<list.size();i++){
            MaterialModel groupCompanyModel =  (MaterialModel) list.get(i);
            company.add(groupCompanyModel.getCompanyDescription());
        }
        ArrayAdapter<String> dtCOmpany = new ArrayAdapter<String>(getActivity(), R.layout.item_spinner_sales, company);
        dtCOmpany.setDropDownViewResource(R.layout.spinner_dropdown);
        // attaching data adapter to spinner
        spinnerPT.setAdapter(dtCOmpany);
        spinnerPT.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your cosde here

                String dataCompany = parentView1.getItemAtPosition(position).toString();

                new SharePreference(getActivity()).setFormCompanyDesc(dataCompany);
                query.openTransaction();
                String[] a = new String[1];
                a[0] = dataCompany;
                String queryString = "select COMPANYCODE from PURCHASE_MATERIAL where COMPANYDESCRIPTION =? GROUP BY COMPANYDESCRIPTION";
                Object object = query.getDataFirstRaw(queryString,MaterialModel.TABLE_NAME,a);
                MaterialModel hasil = (MaterialModel)object;
                query.closeTransaction();
                new SharePreference(getActivity()).setFormCompany(hasil.getCompanyCode());
                new SharePreference(getActivity()).setFormCompanyDesc(dataCompany);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void DataSourceGroupCompany(){
        query.openTransaction();
        List<MaterialModel> listTemp = new ArrayList<MaterialModel>();
        List<Object> listObject;
        String sqldb_query = "SELECT COMPANYGROUP,COMPANYCODE " +
                "                FROM PURCHASE_MATERIAL group by COMPANYGROUP order by COMPANYGROUP desc ";
        listObject = query.getListDataRawQuery(sqldb_query,MaterialModel.TABLE_NAME,null);
        query.closeTransaction();
        if(listObject.size() > 0){
            for(int i = 0; i < listObject.size(); i++){
                MaterialModel blk = (MaterialModel) listObject.get(i);
                listTemp.add(blk);

            }
            setGroupCompany(listObject);
        }
        else{
            //Toast.makeText(getActivity(),"Data TIdak ada",Toast.LENGTH_SHORT).show();
        }
    }

    private void DataSourceDivisi(){
        query.openTransaction();
        List<MaterialModel> listTempDivisi = new ArrayList<MaterialModel>();
        List<Object> listObject;
        String sqldb_query = "SELECT DIVISI FROM PURCHASE_MATERIAL group by DIVISI order by DIVISI asc ";
        listObject = query.getListDataRawQuery(sqldb_query,MaterialModel.TABLE_NAME,null);
        query.closeTransaction();
        if(listObject.size() > 0){
            for(int i = 0; i < listObject.size(); i++){
                MaterialModel blk = (MaterialModel) listObject.get(i);
                listTempDivisi.add(blk);

            }
            setDivisi(listObject);
        }
        else{
            //Toast.makeText(getActivity(),"Data TIdak ada",Toast.LENGTH_SHORT).show();
        }
    }

    private void DataSourceGroupReport(){
        query.openTransaction();
        List<MaterialModel> listTempDivisi = new ArrayList<MaterialModel>();
        List<Object> listObject;
        String sqldb_query = "SELECT GROUPREPORT FROM PURCHASE_MATERIAL group by GROUPREPORT order by GROUPREPORT asc ";
        listObject = query.getListDataRawQuery(sqldb_query,MaterialModel.TABLE_NAME,null);
        query.closeTransaction();
        if(listObject.size() > 0){
            for(int i = 0; i < listObject.size(); i++){
                MaterialModel blk = (MaterialModel) listObject.get(i);
                listTempDivisi.add(blk);

            }
            setSpinnerGroupReport(listObject);
        }
        else{
            //Toast.makeText(getActivity(),"Data TIdak ada",Toast.LENGTH_SHORT).show();
        }
    }


    private void DataSourcePT(String Group){
        query.openTransaction();
        List<MaterialModel> listTemp2 = new ArrayList<MaterialModel>();
        List<Object> listObject2;
        String[] a = new String[1];
        a[0] = Group;
        String sqldb_query = "select COMPANYDESCRIPTION,COMPANYCODE from PURCHASE_MATERIAL where COMPANYGROUP =? group by COMPANYCODE";
        listObject2 = query.getListDataRawQuery(sqldb_query,MaterialModel.TABLE_NAME,a);
        query.closeTransaction();
        if(listObject2.size() > 0){
            for(int i = 0; i < listObject2.size(); i++){
                MaterialModel blk = (MaterialModel) listObject2.get(i);
                listTemp2.add(blk);
            }
            setSpinnerPT(listObject2);
        }
        else{
            //Toast.makeText(getActivity(),"Data TIdak ada",Toast.LENGTH_SHORT).show();
        }
    }


    private void setUpSpinnerBA(List<Object>list) {
        List<String> product = new ArrayList<String>();
        for (int i = 0; i < list.size(); i++) {
            MaterialModel materialModel = (MaterialModel) list.get(i);
            product.add(materialModel.getBA());
        }
        ArrayAdapter<String> dtCOmpany = new ArrayAdapter<String>(getActivity(), R.layout.item_spinner_sales, product);
        dtCOmpany.setDropDownViewResource(R.layout.spinner_dropdown);
        // attaching data adapter to spinner
        spinnerBA.setAdapter(dtCOmpany);
        spinnerBA.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                String dataProduct = parentView1.getItemAtPosition(position).toString();
                    new SharePreference(getActivity()).setBA(dataProduct);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }


    private void DataSourceBA(){
        query.openTransaction();
        List<MaterialModel> listTemp3 = new ArrayList<MaterialModel>();
        List<Object> listObject3;

        String sqldb_query = "select BA from PURCHASE_MATERIAL WHERE DIVISI = 'Ref' group by BA";
        listObject3 = query.getListDataRawQuery(sqldb_query,MaterialModel.TABLE_NAME,null);
        query.closeTransaction();
        if(listObject3.size() > 0){
            for(int i = 0; i < listObject3.size(); i++){
                MaterialModel blk = (MaterialModel) listObject3.get(i);
                listTemp3.add(blk);
            }

            setUpSpinnerBA(listObject3);
        }
        else{
            new SharePreference(getActivity()).setBA(null);
            //    Toast.makeText(getActivity(),"Data TIdak ada",Toast.LENGTH_SHORT).show();
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String year);
    }
}
