package com.indoagri.purchasing.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.purchasing.MaterialPriceActivity;
import com.indoagri.purchasing.R;
import com.indoagri.purchasing.Retrofit.DB.DatabaseQuery;
import com.indoagri.purchasing.Retrofit.Model.MaterialModel;
import com.indoagri.purchasing.Retrofit.SharePreference;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchingMaterialize.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchingMaterialize#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchingMaterialize extends Fragment implements View.OnClickListener {
    int months = Calendar.getInstance().get(Calendar.MONTH);
    int years = Calendar.getInstance().get(Calendar.YEAR);
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View views;
    private OnFragmentInteractionListener mListener;
    private OnAreaListener mAreaListener;
    private OnGroupListener mGroupListener;
    private OnYearListener mYearListener;
    Button btnSearch;
    DatabaseQuery query;
    EditText et_Area;
    EditText et_year;
    EditText et_GroupReport;

    public SearchingMaterialize() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FormSearching.
     */
    // TODO: Rename and change types and number of parameters
    public static FormSearching newInstance(String param1, String param2) {
        FormSearching fragment = new FormSearching();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        query = new DatabaseQuery(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        views = inflater.inflate(R.layout.fragment_searching_materialize, container, false);
        btnSearch = views.findViewById(R.id.btn_search);
        btnSearch.setOnClickListener(this);
        et_year = views.findViewById(R.id.et_year);
        et_Area = views.findViewById(R.id.et_area);
        et_GroupReport = views.findViewById(R.id.et_groupreport);
        ((MaterialPriceActivity)getActivity()).setFragmentRefreshListener(new MaterialPriceActivity.FragmentRefreshListener() {
            @Override
            public void onRefresh() {
                if(new SharePreference(getActivity()).isMaterialGroup().equals("")) {
                    et_GroupReport.setText("SEARCH");
                }else{
                    et_GroupReport.setText(new SharePreference(getActivity()).isMaterialGroup());
                }
                if(new SharePreference(getActivity()).isFormArea().equals("")) {
                    et_Area.setText("SEARCH");
                }else{
                    et_Area.setText(new SharePreference(getActivity()).isFormArea());

                }
                if(new SharePreference(getActivity()).isFormYear().equals("")) {
                    et_year.setText("SEARCH");
                }else{
                    et_year.setText(new SharePreference(getActivity()).isFormYear());

                }
            }
        });

        et_Area.setOnClickListener(this);
        et_GroupReport.setOnClickListener(this);
        et_year.setOnClickListener(this);
        if(new SharePreference(getActivity()).isMaterialGroup().equals("")) {
            et_GroupReport.setText("SEARCH");
        }else{
            et_GroupReport.setText(new SharePreference(getActivity()).isMaterialGroup());
        }
        if(new SharePreference(getActivity()).isFormArea().equals("")) {
            et_Area.setText("SEARCH");
        }else{
            et_Area.setText(new SharePreference(getActivity()).isFormArea());

        }
        if(new SharePreference(getActivity()).isFormYear().equals("")) {
            et_year.setText("SEARCH");
        }else{
            et_year.setText(new SharePreference(getActivity()).isFormYear());

        }
        return views;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            mAreaListener = (OnAreaListener)context;
            mGroupListener = (OnGroupListener)context;
            mYearListener = (OnYearListener)context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mAreaListener = null;
        mGroupListener = null;
        mYearListener = null;
    }

    @Override
    public void onClick(View v) {
        if(v==btnSearch){
            if(et_Area.getText().toString().equalsIgnoreCase("SEARCH") || et_GroupReport.getText().toString().equalsIgnoreCase("SEARCH")
            || et_year.getText().toString().equalsIgnoreCase("SEARCH")){
                Toast.makeText(getActivity(), "Data Tidak Lengkap ", Toast.LENGTH_SHORT).show();
            }else{
              //  getActivity().onBackPressed();
                onButtonPressed();
            }
        }
        if(v==et_Area){
            onArea();
        }
        if(v==et_GroupReport){
            onGroup();
        }
        if(v==et_year){
            onYear();
        }
    }

    public void onArea() {
        if (mAreaListener != null) {
            mAreaListener.onFragmentInteractionArea();
        }
    }
    public void onGroup() {
        if (mGroupListener != null) {
            mGroupListener.onFragmentInteractionGroup();
        }
    }
    public void onYear() {
        if (mYearListener != null) {
            mYearListener.onFragmentInteractionYear();
        }
    }
    public interface OnAreaListener {
        // TODO: Update argument type and name
        void onFragmentInteractionArea();
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction();
    }
    public interface OnGroupListener {
        // TODO: Update argument type and name
        void onFragmentInteractionGroup();
    }
    public interface OnYearListener {
        // TODO: Update argument type and name
        void onFragmentInteractionYear();
    }
}