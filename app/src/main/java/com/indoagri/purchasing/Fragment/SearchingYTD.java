package com.indoagri.purchasing.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.indoagri.purchasing.R;
import com.indoagri.purchasing.Retrofit.DB.DatabaseQuery;
import com.indoagri.purchasing.Retrofit.SharePreference;
import com.indoagri.purchasing.YTDPurchaseActivity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchingYTD.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchingYTD#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchingYTD extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View views;
    private OnFragmentInteractionListener mListener;
    private OnCompanyListener mCompanyListener;
    private OnYearListener mYearListener;
    Button btnSearch;
    DatabaseQuery query;
    EditText et_Company;
    EditText et_year;

    public SearchingYTD() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FormSearching.
     */
    // TODO: Rename and change types and number of parameters
    public static FormSearching newInstance(String param1, String param2) {
        FormSearching fragment = new FormSearching();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        query = new DatabaseQuery(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        views = inflater.inflate(R.layout.fragment_search_ytd, container, false);
        btnSearch = views.findViewById(R.id.btn_search);
        btnSearch.setOnClickListener(this);
        et_Company = views.findViewById(R.id.et_Company);
        et_year = views.findViewById(R.id.et_year);
        ((YTDPurchaseActivity)getActivity()).setFragmentRefreshListener(new YTDPurchaseActivity.FragmentRefreshListener() {
            @Override
            public void onRefresh() {
                if(new SharePreference(getActivity()).isFormCompany().equals("")) {
                    et_Company.setText("SEARCH");
                }else{
                    et_Company.setText(new SharePreference(getActivity()).isFormCompany());
                }
                if(new SharePreference(getActivity()).isFormYear().equals("")) {
                    et_year.setText("SEARCH");
                }else{
                    et_year.setText(new SharePreference(getActivity()).isFormYear());
                }
            }
        });
        
        et_Company.setOnClickListener(this);
        et_year.setOnClickListener(this);
        if(new SharePreference(getActivity()).isFormCompany().equals("")) {
            et_Company.setText("SEARCH");
        }else{
            et_Company.setText(new SharePreference(getActivity()).isFormCompany());
        }
        if(new SharePreference(getActivity()).isFormYear().equals("")) {
            et_year.setText("SEARCH");
        }else{
            et_year.setText(new SharePreference(getActivity()).isFormYear());
        }
        return views;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            mCompanyListener = (OnCompanyListener)context;
            mYearListener = (OnYearListener)context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mCompanyListener = null;
        mYearListener = null;
    }

    @Override
    public void onClick(View v) {
        if(v==btnSearch){
            if(et_Company.getText().toString().equalsIgnoreCase("SEARCH") || et_year.getText().toString().equalsIgnoreCase("SEARCH")){
                Toast.makeText(getActivity(), "Data Tidak Lengkap", Toast.LENGTH_SHORT).show();
            }else{
                //  getActivity().onBackPressed();
                onButtonPressed();
            }
        }
        if(v==et_Company){
            onCompany();
        }
        if(v==et_year){
            onYear();
        }
    }

    public void onCompany() {
        if (mCompanyListener != null) {
            mCompanyListener.onFragmentInteractionCompany();
        }
    }
    public void onYear() {
        if (mYearListener != null) {
            mYearListener.onFragmentInteractionYear();
        }
    }
    public interface OnCompanyListener {
        // TODO: Update argument type and name
        void onFragmentInteractionCompany();
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction();
    }
    public interface OnYearListener {
        // TODO: Update argument type and name
        void onFragmentInteractionYear();
    }
}