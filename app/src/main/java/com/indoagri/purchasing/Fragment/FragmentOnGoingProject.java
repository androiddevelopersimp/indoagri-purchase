package com.indoagri.purchasing.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.indoagri.purchasing.Common.CircleTransform;
import com.indoagri.purchasing.R;
import com.indoagri.purchasing.Retrofit.DB.DatabaseQuery;
import com.indoagri.purchasing.Retrofit.Model.ProjectOnGoingImagesModel;
import com.indoagri.purchasing.Retrofit.SharePreference;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FragmentOnGoingProject extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "params1";
    private static final String ARG_PARAM2 = "params2";
    private static final String ARG_PARAM3 = "params3";

    // TODO: Rename and change types of parameters
    private String mParam1 = null;
    private String mParam2= null;
    private int mParam3 = 0;
    List<ProjectOnGoingImagesModel> onGoingImagesModels;
    private OnFragmentInteractionListener mListener;
    View view;
    FloatingActionButton fab;
    DatabaseQuery query;
    ImageView iv;
    public FragmentOnGoingProject() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FragmentOnGoingProject newInstance(String str1, String str2) {

        FragmentOnGoingProject fragment = new FragmentOnGoingProject();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, str1);
        args.putString(ARG_PARAM2, str2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        query = new DatabaseQuery(getActivity());
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_fragment_on_going_project, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Baca view yang dibuat di XML
        iv=(ImageView)view.findViewById(R.id.myimage);
        fab = (FloatingActionButton)view.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        if (getArguments() != null) {
            mParam1 = getArguments().getString("id");
            mParam2 = getArguments().getString("url");
        }
        CARIDATAWithBA();
    }

    private void CARIDATAWithBA(){
        query.openTransaction();
        List<Object> listImageObject = null;
        String[] a = new String[1];
        a[0] = new SharePreference(getActivity()).isProject();
        String sqldb_query = "SELECT * FROM PURCHASE_ONGOING_IMAGES where PROJECTID IN (select PROJECTID from PURCHASE_ONGOING where PROJECTID=?)";
        listImageObject = query.getListDataRawQuery(sqldb_query, ProjectOnGoingImagesModel.TABLE_NAME,a);
        query.closeTransaction();
        if(listImageObject.size() > 0){
            for(int i = 0; i < listImageObject.size(); i++){
                ProjectOnGoingImagesModel blk = (ProjectOnGoingImagesModel) listImageObject.get(i);
                Picasso.with(this.getContext())
                        .load(blk.getImageUrl())
                        .centerCrop()
                        .placeholder(getResources().getDrawable(R.mipmap.ic_placeholder))
                        .transform(new CircleTransform(50,0))
                        .fit()
                        .into(iv);
                mParam1 = blk.getProjectID();
                mParam2 = blk.getImageUrl();
                mParam3 = i;
            }

        }
        else{
            Toast.makeText(getActivity(), " DATA TIDAK ADA ", Toast.LENGTH_SHORT).show();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri,Bundle bundle) {
        if (mListener != null) {
            mListener.onFragmentInteractionOnGoing(uri,bundle);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if(v==fab){
            Bundle bundle = new Bundle();
            bundle.putString("projectID",mParam1);
            bundle.putString("url",mParam2);
            bundle.putInt("rowid",mParam3);
            onButtonPressed(null,bundle);
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteractionOnGoing(Uri uri,Bundle bundle);
    }
}