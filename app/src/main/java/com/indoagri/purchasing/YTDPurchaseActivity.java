package com.indoagri.purchasing;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.purchasing.Common.DateLocal;
import com.indoagri.purchasing.Dialog.ProgressDialogs;
import com.indoagri.purchasing.Fragment.SearchingYTD;
import com.indoagri.purchasing.Retrofit.Adapter.YTDPurchaseAdapter;
import com.indoagri.purchasing.Retrofit.ApiServices;
import com.indoagri.purchasing.Retrofit.DB.DatabaseQuery;
import com.indoagri.purchasing.Retrofit.Model.MaterialPrice;
import com.indoagri.purchasing.Retrofit.Model.YTDPurchase;
import com.indoagri.purchasing.Retrofit.NetClient;
import com.indoagri.purchasing.Retrofit.SharePreference;
import com.indoagri.purchasing.Retrofit.YTDResponse;
import com.indoagri.purchasing.widget.utils.DateLibs;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class YTDPurchaseActivity extends AppCompatActivity implements  SearchingYTD.OnFragmentInteractionListener,SearchingYTD.OnCompanyListener,
SearchingYTD.OnYearListener{
    private static final String TAGACTIVITY = "GET DATA YTD PURCHASE";
    int months = Calendar.getInstance().get(Calendar.MONTH);
    int years = Calendar.getInstance().get(Calendar.YEAR);
    DatabaseQuery query;
    Toolbar toolbar;
    TextView mTextToolbar;
    ArrayList<YTDPurchase> ytdPurchasesList = new ArrayList<YTDPurchase>();
    ApiServices apiServices;
    YTDPurchase ytdPurchase;
    YTDResponse ytdResponse;
    YTDPurchaseAdapter adapter;
    ImageView imgReload, imgSort, imgSearch;
    FrameLayout frame_display, searchFragment;
    boolean ShowSearch = false;
    TextView ReloadData;
    public static int COMPANIES = 22;
    public static int YEAR = 33;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    DividerItemDecoration dividerItemDecoration;
    TableLayout tableLayout,tableLayoutfooter;
    TextView txtInfoCompany,txtYear;
    List<Object> LASTORDER;
    public FragmentRefreshListener getFragmentRefreshListener() {
        return fragmentRefreshListener;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener fragmentRefreshListener) {
        this.fragmentRefreshListener = fragmentRefreshListener;
    }
    private FragmentRefreshListener fragmentRefreshListener;
    ProgressDialogs Progressalert;
    Dialog Pushdialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ytdpurchase);
        Pushdialog = new Dialog(YTDPurchaseActivity.this);
        Progressalert= new ProgressDialogs();
        Progressalert.ProgressDialogs(Pushdialog,YTDPurchaseActivity.this,"Please Wait");
        query = new DatabaseQuery(YTDPurchaseActivity.this);
        recyclerView = (findViewById(R.id.recycler_view));
        recyclerView.setHasFixedSize(true);
        frame_display = (findViewById(R.id.frame_view_display));
        searchFragment = (findViewById(R.id.fragment_container));
        FloatingActionButton fab = findViewById(R.id.fab);
        imgReload = (findViewById(R.id.imgReload));
        imgSort = (findViewById(R.id.imgSort));
        ReloadData = (findViewById(R.id.Reload));
        ReloadData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toList();
                showYearDialogReload();
            }
        });
        imgSort.setVisibility(View.GONE);
        imgSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(MaterialPriceActivity.this, "Sortir", Toast.LENGTH_SHORT).show();
            }
        });
        imgSearch = (findViewById(R.id.imgSearch));
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ShowSearch){
                    toList();
                }else{
                    toSearch();
                }
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        apiServices = NetClient.DataProduction().create(ApiServices.class);
        tableLayout = (TableLayout) findViewById(R.id.main_table);
        tableLayoutfooter = (TableLayout) findViewById(R.id.main_tableFooter);
        tableLayout.bringToFront();
        tableLayout.setStretchAllColumns(true);
        tableLayoutfooter.bringToFront();
        tableLayoutfooter.setStretchAllColumns(true);
        setUpToolbar();
        initActivity();
    }

    private void initActivity() {
        String year = com.indoagri.purchasing.Common.DateLibs.getTahunIni();
        if (deletetable(year)) {
            GetYTD(year);
        }

    }
    void InitForm(){
        txtInfoCompany = findViewById(R.id.txtCompany);
        txtYear = findViewById(R.id.txtYear);
        if(new SharePreference(YTDPurchaseActivity.this).isFormCompany().equalsIgnoreCase("ALL")) {
            txtInfoCompany.setText("ALL");
        }else{
            txtInfoCompany.setText(new SharePreference(YTDPurchaseActivity.this).isFormCompany());
        }
        if(new SharePreference(YTDPurchaseActivity.this).isFormYear().equalsIgnoreCase("ALL")) {
            txtYear.setText("ALL");
        }else{
            txtYear.setText(new SharePreference(YTDPurchaseActivity.this).isFormYear());
        }
    }

    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText("YTD TOTAL PURCHASE");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                if(ShowSearch){
                    toList();
                }else{
                    new SharePreference(YTDPurchaseActivity.this).setFormCompany("");
                    new SharePreference(YTDPurchaseActivity.this).setFormYear("");
                    onBackPressed();
                }
            }
        });
    }
    public void toList(){
        searchFragment.setVisibility(View.GONE);
        frame_display.setVisibility(View.VISIBLE);
        ShowSearch = false;
    }
    public void toSearch(){
        searchFragment.setVisibility(View.VISIBLE);
        frame_display.setVisibility(View.GONE);
        Fragment fragment = new SearchingYTD();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
        ShowSearch = true;
    }

    // GET YTD PRICE //

    private void GetYTD(String year) {
        Progressalert.showDialog();
        Log.d(TAGACTIVITY, "LOADPAGE: ");

        GetYTDPurchase(year).enqueue(new Callback<YTDResponse>() {
            @Override
            public void onResponse(Call<YTDResponse> call, Response<YTDResponse> response) {
                // Got data. Send it to adapter
                Progressalert.dismissDialog();
                if (response.code() == 200) {
                    ytdResponse = fetchYTD(response);
                    ytdPurchasesList = ytdResponse.getYTDPurchaseList();
                    insertMaterials();
                } else {
                    Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<YTDResponse> call, Throwable t) {
                t.printStackTrace();
                Progressalert.dismissDialog();
                // TODO: 08/11/16 handle failure
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private Call<YTDResponse> GetYTDPurchase(String year) {
        return apiServices.GETDATAYTD(year);
    }

    private YTDResponse fetchYTD(Response<YTDResponse> response) {
        YTDResponse ytdResponse = response.body();
        return ytdResponse;
    }

    private void insertMaterials() {
        query.openTransaction();
        for (int i = 0; i < ytdPurchasesList.size(); i++) {
            YTDPurchase materialModel = ytdPurchasesList.get(i);
            ContentValues values = new ContentValues();
            values.put(YTDPurchase.XML_INSERTDATE, materialModel.getInsertdate());
            values.put(YTDPurchase.XML_COMP_GRP, materialModel.getComPGRP());
            values.put(YTDPurchase.XML_MATGRP, materialModel.getMatgrp());
            values.put(YTDPurchase.XML_MANDT, materialModel.getMandt());
            values.put(YTDPurchase.XML_MATGRPDESC, materialModel.getMatgrpdesc());
            values.put(YTDPurchase.XML_LSTPRD, materialModel.getLstprd());
            values.put(YTDPurchase.XML_WAERS, materialModel.getWaers());
            if(materialModel.getLstprdtot()==null){
                values.put(YTDPurchase.XML_LSTPRDTOT, "0");
            }
            if(materialModel.getLstprdtot()!=null){
                values.put(YTDPurchase.XML_LSTPRDTOT, materialModel.getLstprdtot());
            }

            if(materialModel.getYtdtot()==null){
                values.put(YTDPurchase.XML_YTDTOT, "0");
            }
            if(materialModel.getYtdtot()!=null){
                values.put(YTDPurchase.XML_YTDTOT, materialModel.getYtdtot());
            }
            query.insertDataSQL(YTDPurchase.TABLE_NAME, values);
        }
        query.commitTransaction();
        query.closeTransaction();
        //GetDataTopRank();
      //  INITVIEWFIRST();
        toSearch();
    }

    private boolean deletetable(String year) {
        Progressalert.showDialog();
        boolean result = false;
        query.openTransaction();
        int rowID = 0;
        int rowID2 = 0;
        try {
            String[] ys = new String[1];
            ys[0] = year;
            query.deleteDataTemporary(YTDPurchase.TABLE_NAME, "substr(LSTPRD,1,4) =?", ys);
//            if (rowID > 0 || rowID == 0) {
//                query.deleteDataTable(YTDPurchase.TABLE_NAME,null,null);
//            }
        } finally {
            Progressalert.dismissDialog();
            query.commitTransaction();
            query.closeTransaction();
            result = true;
        }
        return result;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == COMPANIES) {
            if(resultCode == RESULT_OK) {
                String CODE=data.getStringExtra("CODE");
                String DESC=data.getStringExtra("DESC");
                if(CODE==null){

                }else{
                    new SharePreference(YTDPurchaseActivity.this).setFormCompany(CODE);
                    if(getFragmentRefreshListener()!=null){
                        getFragmentRefreshListener().onRefresh();
                    }
                }
            }
        }
        if (requestCode == YEAR) {
            if(resultCode == RESULT_OK) {
                String CODE=data.getStringExtra("CODE");
                String DESC=data.getStringExtra("DESC");
                if(CODE==null){

                }else{
                    new SharePreference(YTDPurchaseActivity.this).setFormYear(CODE);
                    if(getFragmentRefreshListener()!=null){
                        getFragmentRefreshListener().onRefresh();
                    }
                }
            }
        }
    }

    private void INITVIEWFIRST() {
        query.openTransaction();
        if (ytdPurchasesList.size() > 0 || ytdPurchasesList != null) {
            ytdPurchasesList.clear();
        }
        List<Object> listObject;
        String Year = new SharePreference(YTDPurchaseActivity.this).isFormYear();
        String[] aa = new String[1];
        aa[0] = Year;
            String sqldb_query2 = "select * from YTD_PURCHASE where SUBSTR(LSTPRD,1,4) = ? order by LSTPRD DESC limit 0,1";
            LASTORDER = query.getListDataRawQuery(sqldb_query2, YTDPurchase.TABLE_NAME, aa);
        YTDPurchase ytdPurchaseLAST = (YTDPurchase) LASTORDER.get(0);
        String[] ab = new String[1];
        ab[0] = ytdPurchaseLAST.getLstprd();
        String sqldb_query = "select * from YTD_PURCHASE where LSTPRD = ? group by matgrp order by CAST(ytdtot AS INTEGER) desc, LSTPRD DESC";
        listObject = query.getListDataRawQuery(sqldb_query, YTDPurchase.TABLE_NAME, aa);
        query.closeTransaction();
        List<YTDPurchase> listTemp = new ArrayList<YTDPurchase>();
        if(listObject==null){
            // tableLayout.removeAllViews();
        }
        else{
            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    YTDPurchase materialPrice= (YTDPurchase) listObject.get(i);
                    listTemp.add(materialPrice);
                }
                //startLoadData();
                AsyncDetail asyncDetail = new AsyncDetail(listObject,YTDPurchaseActivity.this);
                asyncDetail.execute();
            }else{
                tableLayout.removeAllViews();
                tableLayoutfooter.removeAllViews();
            }
        }
        InitForm();
    }

    private void INITVIEWFIRSTWITHCOMPANY() {
        query.openTransaction();
        if (ytdPurchasesList.size() > 0 || ytdPurchasesList != null) {
            ytdPurchasesList.clear();
        }
        List<Object> listObject;
        String Year = new SharePreference(YTDPurchaseActivity.this).isFormYear();
        String COMPANY = new SharePreference(YTDPurchaseActivity.this).isFormCompany();
        String[] aa = new String[2];
        aa[0] = Year;
        aa[1] = COMPANY;
            String sqldb_query2 = "select * from YTD_PURCHASE where SUBSTR(LSTPRD,1,4) = ? and COMP_GRP=? order by LSTPRD DESC limit 0,1";
            LASTORDER = query.getListDataRawQuery(sqldb_query2, YTDPurchase.TABLE_NAME, aa);
            YTDPurchase ytdPurchaseLAST = (YTDPurchase) LASTORDER.get(0);
        String[] ab = new String[2];
        ab[0] = ytdPurchaseLAST.getLstprd();
        ab[1] = COMPANY;
        String sqldb_query = "select * from YTD_PURCHASE WHERE LSTPRD = ? and COMP_GRP=? group by matgrp order by CAST(ytdtot AS INTEGER) desc, LSTPRD DESC";
        listObject = query.getListDataRawQuery(sqldb_query, YTDPurchase.TABLE_NAME, ab);
        List<YTDPurchase> listTemp = new ArrayList<YTDPurchase>();
        query.closeTransaction();
        if(listObject==null){
            // tableLayout.removeAllViews();
        }
        else{
            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    YTDPurchase materialPrice= (YTDPurchase) listObject.get(i);
                    listTemp.add(materialPrice);
                }
                //startLoadData();
                AsyncDetail asyncDetail = new AsyncDetail(listObject,YTDPurchaseActivity.this);
                asyncDetail.execute();
            }else{
                tableLayout.removeAllViews();
                tableLayoutfooter.removeAllViews();

            }
        }
        InitForm();
    }


    @Override
    public void onFragmentInteractionCompany() {
        Intent newIntent = new Intent(YTDPurchaseActivity.this,ListCompanyActivity.class);
        newIntent.putExtra("COMPANIES","COMPANIES");
        startActivityForResult(newIntent, COMPANIES);
    }

    @Override
    public void onFragmentInteraction() {

        String COMPANY = new SharePreference(YTDPurchaseActivity.this).isFormCompany();
        if(COMPANY.equalsIgnoreCase("ALL") || COMPANY.equalsIgnoreCase("SEARCH")){
            INITVIEWFIRST();
        }else{
            INITVIEWFIRSTWITHCOMPANY();
        }
        frame_display.setVisibility(View.VISIBLE);
        searchFragment.setVisibility(View.GONE);
        ShowSearch = false;
    }

    @Override
    public void onFragmentInteractionYear() {
        query.openTransaction();
        List<Object> listObject;
        ArrayList<YTDPurchase>sendYear = new ArrayList<YTDPurchase>();
        String sqldb_query ="SELECT * FROM  YTD_PURCHASE GROUP BY substr(LSTPRD,1,4)";
        listObject = query.getListDataRawQuery(sqldb_query, YTDPurchase.TABLE_NAME, null);
        query.closeTransaction();
        for(int i = 0; i < listObject.size(); i++){
            YTDPurchase ytdPurchase= (YTDPurchase) listObject.get(i);
            sendYear.add(ytdPurchase);
        }
        Intent newIntent = new Intent(YTDPurchaseActivity.this,ListYearActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("table", YTDPurchase.TABLE_NAME);
        bundle.putParcelableArrayList("data", sendYear);
        newIntent.putExtras(bundle);
        startActivityForResult(newIntent, YEAR);
    }

    public interface FragmentRefreshListener{
        void onRefresh();
    }

    public void showYearDialog() {

        final Dialog d = new Dialog(YTDPurchaseActivity.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_year);
        Button set = (Button) d.findViewById(R.id.btnOk);
        Button cancel = (Button) d.findViewById(R.id.btnCancel);
        TextView year_text = (TextView) d.findViewById(R.id.year_text);
        year_text.setText("Set Year");
        final NumberPicker yearpicker = (NumberPicker) d.findViewById(R.id.picker_year);

        yearpicker.setMinValue(2016);
        yearpicker.setMaxValue(2025);
        yearpicker.setWrapSelectorWheel(false);
        yearpicker.setValue(years);

        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Years = String.valueOf(yearpicker.getValue());
                new SharePreference(YTDPurchaseActivity.this).setFormYear(Years);
                d.dismiss();
//                if(getFragmentRefreshListener()!=null){
//                    getFragmentRefreshListener().onRefresh();
//                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    public void showYearDialogReload() {

        final Dialog d = new Dialog(YTDPurchaseActivity.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_year);
        Button set = (Button) d.findViewById(R.id.btnOk);
        Button cancel = (Button) d.findViewById(R.id.btnCancel);
        TextView year_text = (TextView) d.findViewById(R.id.year_text);
        year_text.setText("Set Year");
        final NumberPicker yearpicker = (NumberPicker) d.findViewById(R.id.picker_year);

        yearpicker.setMinValue(2016);
        yearpicker.setMaxValue(2025);
        yearpicker.setWrapSelectorWheel(false);
        yearpicker.setValue(years);

        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Years = String.valueOf(yearpicker.getValue());
                new SharePreference(YTDPurchaseActivity.this).setRefreshYear(Years);
                String y = new SharePreference(YTDPurchaseActivity.this).isRefreshYear();
                d.dismiss();
                if (deletetable(y)) {
                    GetYTD(y);
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    private class AsyncDetail extends AsyncTask<Void, Integer, Void> {
        List<Object> objectList;
        YTDPurchase materialPrice;
        public AsyncDetail(List<Object> objects, Context context) {

            objectList = objects;
        }
        @Override
        protected void onPreExecute() {
            Progressalert.showDialog();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void result) {
            Progressalert.dismissDialog();
            loadData(objectList);
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 0; i<objectList.size(); i++) {
                publishProgress(i);
                ytdPurchase = (YTDPurchase) objectList.get(i);
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }
    }
    public void loadData(List<Object> objectList) {

        int leftRowMargin=0;
        int topRowMargin=0;
        int rightRowMargin=0;
        int bottomRowMargin = 0;
        int textSize = 0, smallTextSize =0, mediumTextSize = 0;

        textSize = (int) getResources().getDimension(R.dimen.font_size_medium);
        smallTextSize = (int) getResources().getDimension(R.dimen.font_size_normal);
        mediumTextSize = (int) getResources().getDimension(R.dimen.font_size_medium);

        int totalrows = objectList.size();
        int rows = objectList.size();
        int rowsIden = 0;
        getSupportActionBar().setTitle("Price (" + String.valueOf(rows) + ")");
        TextView textSpacer = null;

        tableLayout.removeAllViews();

        NumberFormat PriceFormat = new DecimalFormat("#,###,###,###,###,###");
        double priceTotalFormat = 0;
        double  priceFormat = 0 ;
        double priceTotal2Format = 0;
        double  price2Format = 0 ;
        String DataTotalpriceFormattedNumber = null;
        String DatapriceFormattedNumber = null;
        String DataTotalprice2FormattedNumber = null;
        String Dataprice2FormattedNumber = null;
        String DataCategory = null;
        String hariini = DateLibs.getHariini();
        String BeforeMonth = DateLibs.beforeMonth(1);
        String BeforeYear = DateLibs.beforeYear(1);


        YTDPurchase ytdPurchaseLAST = (YTDPurchase) LASTORDER.get(0);

        String LASTPRD = ytdPurchaseLAST.getLstprd();
        String DATALASTPERIOD = ytdPurchaseLAST.getLstprd().substring(4,6);
        String DATAYTD = ytdPurchaseLAST.getLstprd().substring(0,4);
        DateLocal dateLocal = new DateLocal();
        String NamaBulan  = dateLocal.getMonthAlphabet(DATALASTPERIOD)+" "+DATAYTD;
        int TotalFooter = 1;
        // -1 means heading row
        for (rowsIden= -1; rowsIden < objectList.size(); rowsIden++) {
            if (rowsIden > -1) {
                ytdPurchase = (YTDPurchase) objectList.get(rowsIden);
                if(ytdPurchase.getLstprdtot()== null || ytdPurchase.getLstprdtot().equals(null) || ytdPurchase.getLstprdtot().isEmpty()){
                    priceFormat= 0;
                    DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                }else{
                    if(!LASTPRD.equals(ytdPurchase.getLstprd())){
                        priceFormat= 0;
                        DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                    }else{
                        priceFormat= Double.parseDouble(ytdPurchase.getLstprdtot())/1000000;
                        DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                    }
                }
                if(ytdPurchase.getYtdtot()== null || ytdPurchase.getYtdtot().equals(null) || ytdPurchase.getYtdtot().isEmpty()){
                    price2Format= 0;
                    Dataprice2FormattedNumber = PriceFormat.format(price2Format);
                }else{
                    price2Format= Double.parseDouble(ytdPurchase.getYtdtot())/1000000;
                    Dataprice2FormattedNumber = PriceFormat.format(price2Format);
                }
                if (ytdPurchase.getMatgrpdesc().length() >= 50) {
                    DataCategory = ytdPurchase.getMatgrpdesc().substring(0, 40)+ "...";
                } else {
                    DataCategory = ytdPurchase.getMatgrpdesc();
                }
                priceTotalFormat += priceFormat;
                DataTotalpriceFormattedNumber= PriceFormat.format(priceTotalFormat);
                priceTotal2Format += price2Format;
                DataTotalprice2FormattedNumber= PriceFormat.format(priceTotal2Format);
            }else {
                textSpacer = new TextView(this);
                textSpacer.setText("");
            }
            final TextView tvCategory = new TextView(this);
            if (rowsIden == -1) {
                tvCategory.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                tvCategory.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            } else {
                tvCategory.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvCategory.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }

            tvCategory.setGravity(Gravity.LEFT);

            tvCategory.setPadding(15, 15, 0, 15);
            if (rowsIden == -1) {
                tvCategory.setText("Category");
                tvCategory.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                tvCategory.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvCategory.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            }else {
                tvCategory.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                tvCategory.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvCategory.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
                tvCategory.setText(DataCategory);
            }

            final LinearLayout layLastPeriod = new LinearLayout(this);
            layLastPeriod.setOrientation(LinearLayout.VERTICAL);
            layLastPeriod.setGravity(Gravity.RIGHT);
            layLastPeriod.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));



            final TextView tvLastPeriod = new TextView(this);
            tvLastPeriod.setPadding(5, 15, 15, 15);
            if (rowsIden == -1) {
                tvLastPeriod.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                layLastPeriod.setBackgroundColor(Color.parseColor("#f7f7f7"));
            } else {
                tvLastPeriod.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                layLastPeriod.setBackgroundColor(Color.parseColor("#ffffff"));
            }
            tvLastPeriod.setGravity(Gravity.RIGHT);
            if (rowsIden == -1) {
                tvLastPeriod.setText(NamaBulan);
                tvLastPeriod.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                tvLastPeriod.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvLastPeriod.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            } else {
                tvLastPeriod.setBackgroundColor(getResources().getColor(R.color.colorWhiteinit));
                tvLastPeriod.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvLastPeriod.setText(DatapriceFormattedNumber);
                tvLastPeriod.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }

            layLastPeriod.addView(tvLastPeriod);

            final LinearLayout layYTDTOT = new LinearLayout(this);
            layYTDTOT.setOrientation(LinearLayout.VERTICAL);
            layYTDTOT.setGravity(Gravity.RIGHT);
            layYTDTOT.setBackgroundColor(Color.parseColor("#f8f8f8"));

            final TextView tvYTDTOT = new TextView(this);

            tvYTDTOT.setPadding(5, 15, 15, 15);
            if (rowsIden == -1) {
                tvYTDTOT.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                layYTDTOT.setBackgroundColor(Color.parseColor("#f7f7f7"));
            } else {
                tvYTDTOT.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                layYTDTOT.setBackgroundColor(Color.parseColor("#ffffff"));
            }

            tvYTDTOT.setGravity(Gravity.RIGHT);

            if (rowsIden == -1) {
                tvYTDTOT.setText("YTD "+DATAYTD);
                tvYTDTOT.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvYTDTOT.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
                tvYTDTOT.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            } else {
                tvYTDTOT.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                tvYTDTOT.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvYTDTOT.setText(Dataprice2FormattedNumber);
                tvYTDTOT.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }
            layYTDTOT.addView(tvYTDTOT);

            // add table row
            final TableRow tr = new TableRow(this);
            TableRow.LayoutParams trParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT);
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
            tr.setPadding(0,0,0,0);
            trParams.span = 3;

            tr.setLayoutParams(trParams);

            //tr.addView(tv);
            tr.addView(tvCategory);
            tr.addView(layLastPeriod);
            tr.addView(layYTDTOT);
            if (rowsIden > -1) {
                tr.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        TableRow tr = (TableRow) v;
                        //do whatever action is needed
                        //   Toast.makeText(SalesBilling.this,"Test Toast",Toast.LENGTH_SHORT).show();
                    }
                });
            }
            tableLayout.addView(tr, trParams);
        }
        tableLayoutfooter.removeAllViews();
        for(int rowsFooter = 0;rowsFooter<TotalFooter;rowsFooter++){
            final TextView tvCategory = new TextView(this);
            if (rowsFooter == -1) {
                tvCategory.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                tvCategory.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            } else {
                tvCategory.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvCategory.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                tvCategory.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            }

            tvCategory.setGravity(Gravity.LEFT);

            tvCategory.setPadding(15, 15, 0, 15);
            if (rowsFooter == -1) {
                tvCategory.setText("Category");
                tvCategory.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvCategory.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            }else {
                tvCategory.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvCategory.setText("Total");
            }

            final LinearLayout layLastPeriod = new LinearLayout(this);
            layLastPeriod.setOrientation(LinearLayout.VERTICAL);
            layLastPeriod.setGravity(Gravity.RIGHT);
            layLastPeriod.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));

            final TextView tvLastPeriod = new TextView(this);
            tvLastPeriod.setPadding(5, 15, 15, 15);
            if (rowsFooter == -1) {
                tvLastPeriod.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                layLastPeriod.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            } else {
                tvLastPeriod.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                layLastPeriod.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            }
            tvLastPeriod.setGravity(Gravity.RIGHT);
            if (rowsFooter == -1) {
                tvLastPeriod.setText("PO Value");
                tvLastPeriod.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                tvLastPeriod.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvLastPeriod.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            } else {
                tvLastPeriod.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvLastPeriod.setText(DataTotalpriceFormattedNumber);
                tvLastPeriod.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            }

            layLastPeriod.addView(tvLastPeriod);

            final LinearLayout layYTDTOT = new LinearLayout(this);
            layYTDTOT.setOrientation(LinearLayout.VERTICAL);
            layYTDTOT.setGravity(Gravity.RIGHT);

            final TextView tvYTDTOT = new TextView(this);

            tvYTDTOT.setPadding(5, 15, 15, 15);
            if (rowsFooter == -1) {
                tvYTDTOT.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                layYTDTOT.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            } else {
                tvYTDTOT.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                layYTDTOT.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            }

            tvYTDTOT.setGravity(Gravity.RIGHT);

            if (rowsFooter == -1) {
                tvYTDTOT.setText("YTD 2019");
                tvYTDTOT.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvYTDTOT.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            } else {
                tvYTDTOT.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvYTDTOT.setText(DataTotalprice2FormattedNumber);
                tvYTDTOT.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            }
            layYTDTOT.addView(tvYTDTOT);

            // add table row
            final TableRow tr = new TableRow(this);
            TableRow.LayoutParams trParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT);
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
            tr.setPadding(0,0,0,0);
            trParams.span = 6;

            tr.setLayoutParams(trParams);

            //tr.addView(tv);
            tr.addView(tvCategory);
            tr.addView(layLastPeriod);
            tr.addView(layYTDTOT);
            tableLayoutfooter.addView(tr, trParams);
        }
    }
}
