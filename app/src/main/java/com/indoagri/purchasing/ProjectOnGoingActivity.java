package com.indoagri.purchasing;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.purchasing.Fragment.DialogOnGoing;
import com.indoagri.purchasing.Fragment.FormSearchingOnGoing;
import com.indoagri.purchasing.Fragment.FragmentOnGoingProject;
import com.indoagri.purchasing.Retrofit.Adapter.OnGoingSlideAdapter;
import com.indoagri.purchasing.Retrofit.ApiServices;
import com.indoagri.purchasing.Retrofit.DB.DatabaseQuery;
import com.indoagri.purchasing.Retrofit.Model.ProjectOnGoingImagesModel;
import com.indoagri.purchasing.Retrofit.Model.ProjectOnGoingModel;
import com.indoagri.purchasing.Retrofit.Model.ProjectOnGoingProgressModel;
import com.indoagri.purchasing.Retrofit.NetClient;
import com.indoagri.purchasing.Retrofit.OnGoingResponse;
import com.indoagri.purchasing.Retrofit.SharePreference;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProjectOnGoingActivity extends AppCompatActivity implements FragmentOnGoingProject.OnFragmentInteractionListener, DialogOnGoing.OnFragmentInteractionListener, FormSearchingOnGoing.OnFragmentInteractionListener, View.OnClickListener {
    private static final String TAGACTIVITY = "GET DATA PROJECT";
    ApiServices apiServices;
    Toolbar toolbar;
    TextView mTextToolbar;
    ProgressBar progressBar;
    DatabaseQuery query;
    OnGoingResponse onGoingResponse;
    List<ProjectOnGoingImagesModel> onGoingImagesModels;
    List<ProjectOnGoingProgressModel> onGoingProgressModels;
    List<ProjectOnGoingModel> onGoingModel;

    boolean ShowSearch = false;
    // VIEW PAGER //
    ViewPager pager;
    List<Fragment> fragments;
    OnGoingSlideAdapter adapter;
    ImageView imgSearch;
    LinearLayout linearImage;
    FrameLayout searchFragment;
    LinearLayout linearHome;
    LinearLayout linearProgress;
    ImageView imgGallery,imgProgress;
    String ProjectsID;
    ImageView imgClose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_on_going);
        query = new DatabaseQuery(ProjectOnGoingActivity.this);
        FloatingActionButton fab = findViewById(R.id.fab);
        progressBar = (ProgressBar)findViewById(R.id.progress_bar);
        pager = (ViewPager) findViewById(R.id.viewPager);
        linearImage = (findViewById(R.id.LinearImage));
        linearHome = (findViewById(R.id.LinearHome));
        linearProgress = (findViewById(R.id.LinearProgress));
        searchFragment = (findViewById(R.id.fragment_container));
        imgProgress = (ImageView)linearHome.findViewById(R.id.progressOnGoing);
        imgClose = (ImageView)linearProgress.findViewById(R.id.close);
        imgGallery = (ImageView)linearHome.findViewById(R.id.pictureOnGoing);
        imgProgress.setOnClickListener(this);
        imgGallery.setOnClickListener(this);
        imgClose.setOnClickListener(this);
        fab.hide();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        apiServices = NetClient.DataProduction().create(ApiServices.class);
        setUpToolbar();
        initActivity();
        imgSearch = (findViewById(R.id.imgSearch));
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ShowSearch){
                    onBackPressed();
                }else{
                    toSearch();
                }
            }
        });
    }

    public void toSearch(){
        searchFragment.setVisibility(View.VISIBLE);
        linearImage.setVisibility(View.GONE);
        linearHome.setVisibility(View.GONE);
        Fragment fragment = new FormSearchingOnGoing();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
        ShowSearch = true;
    }

    @Override
    public void onBackPressed() {
        if(searchFragment.getVisibility()==View.VISIBLE){
            searchFragment.setVisibility(View.GONE);
            linearImage.setVisibility(View.GONE);
            linearHome.setVisibility(View.VISIBLE);
            linearProgress.setVisibility(View.GONE);
        }
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            new SharePreference(ProjectOnGoingActivity.this).setFormGroupcompany("");
            new SharePreference(ProjectOnGoingActivity.this).setFormCompany("");
            new SharePreference(ProjectOnGoingActivity.this).setBA("");
            new SharePreference(ProjectOnGoingActivity.this).setFormYear("");
            new SharePreference(ProjectOnGoingActivity.this).setFormProject("");
        } else {
            getSupportFragmentManager().popBackStack();
        }
        ShowSearch = false;
    }

    @Override
    public void onFragmentInteraction(String year) {
        //     Toast.makeText(SalesBilling.this,year, Toast.LENGTH_SHORT).show();
        ProjectsID = new SharePreference(ProjectOnGoingActivity.this).isProject();
        linearImage.setVisibility(View.GONE);
        searchFragment.setVisibility(View.GONE);
        linearProgress.setVisibility(View.GONE);
        linearHome.setVisibility(View.VISIBLE);
        Toast.makeText(this, "Kirim "+ProjectsID, Toast.LENGTH_SHORT).show();
    }
    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar= (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText("Project On Going");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    private void initActivity(){
        boolean StatusDelete = false;
        StatusDelete = deletetable();
        if(StatusDelete){
            GetDataOnGoing();
        }

    }
    private boolean deletetable(){
        progressBar.setVisibility(View.VISIBLE);
        boolean result = false;
        query.openTransaction();
        int rowID = 0;
        int rowID2 = 0;
        int rowID3 = 0;
        try{
            rowID =  query.deleteDataTemporary(ProjectOnGoingModel.TABLE_NAME,null,null);
            if(rowID>0 || rowID==0){
                rowID2 = query.deleteDataTemporary(ProjectOnGoingImagesModel.TABLE_NAME,null,null);
                if(rowID2>0||rowID2==0) {
                    query.deleteDataTemporary(ProjectOnGoingProgressModel.TABLE_NAME, null, null);
                }
            }
        }finally {
            query.commitTransaction();
            query.closeTransaction();
            result = true;
        }
        return result;
    }

    /* ONGOING */
        private void GetDataOnGoing(){
            if (progressBar.getVisibility() == View.GONE) {
                progressBar.setVisibility(View.VISIBLE);
            }
            Log.d(TAGACTIVITY, "LOADPAGE: ");

            GetOnGoingProject().enqueue(new Callback<OnGoingResponse>() {
                @Override
                public void onResponse(Call<OnGoingResponse> call, Response<OnGoingResponse> response) {
                    // Got data. Send it to adapter
                    progressBar.setVisibility(View.GONE);
                    if(response.code()==200){
                        onGoingResponse = fetchOnGoingResult(response);
                        onGoingModel = onGoingResponse.getOnGoing();
                        insertOnGoing();

                    }else{
                        Toast.makeText(getApplicationContext(),"Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<OnGoingResponse> call, Throwable t) {
                    t.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                    // TODO: 08/11/16 handle failure
                    Toast.makeText(getApplicationContext(),t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        private OnGoingResponse fetchOnGoingResult(Response<OnGoingResponse> response) {
            OnGoingResponse materialResponse = response.body();
            return materialResponse;
        }
        private Call<OnGoingResponse> GetOnGoingProject() {
            return apiServices.getOnGoingProject();
        }

        private void insertOnGoing(){
            query.openTransaction();
            for (int i = 0; i < onGoingModel.size(); i++) {
                ProjectOnGoingModel ONGOING=onGoingModel.get(i);
                ContentValues values = new ContentValues();
                values.put(ProjectOnGoingModel.XML_PROJECTID,ONGOING.getProjectID());
                values.put(ProjectOnGoingModel.XML_DESCRIPTION,ONGOING.getDescription());
                values.put(ProjectOnGoingModel.XML_COMPANYGROUP,ONGOING.getCompanyGroup());
                values.put(ProjectOnGoingModel.XML_DIVISI,ONGOING.getDivisi());
                values.put(ProjectOnGoingModel.XML_COMPANYCODE,ONGOING.getCompanyCode());
                values.put(ProjectOnGoingModel.XML_COMPANYDESCRIPTION,ONGOING.getCompanyDescription());
                values.put(ProjectOnGoingModel.XML_BA,ONGOING.getBa());
                values.put(ProjectOnGoingModel.XML_BADESCRIPTION,ONGOING.getBaDescription());
                values.put(ProjectOnGoingModel.XML_CAPACITY,ONGOING.getCapacity());
                values.put(ProjectOnGoingModel.XML_VENDORCODE,ONGOING.getVendorCode());
                values.put(ProjectOnGoingModel.XML_VENDORNAME,ONGOING.getVendorName());
                values.put(ProjectOnGoingModel.XML_LOCATION,ONGOING.getLocation());
                values.put(ProjectOnGoingModel.XML_QTY,ONGOING.getQty());
                values.put(ProjectOnGoingModel.XML_VALUE,ONGOING.getValue());
                values.put(ProjectOnGoingModel.XML_PROGRESS,ONGOING.getProgress());
                values.put(ProjectOnGoingModel.XML_STARTED,ONGOING.getStarted());
                values.put(ProjectOnGoingModel.XML_ENDED,ONGOING.getEnded());
                values.put(ProjectOnGoingModel.XML_LASTUPDATE,ONGOING.getLastUpdated());
                query.insertDataSQL(ProjectOnGoingModel.TABLE_NAME,values);
            }
            query.commitTransaction();
            query.closeTransaction();
            GetDataOnGoingImages();

        }


    /* IMAGES */
        private void GetDataOnGoingImages(){
            if (progressBar.getVisibility() == View.GONE) {
                progressBar.setVisibility(View.VISIBLE);
            }
            Log.d(TAGACTIVITY, "LOADPAGE: ");

            GetOnGoingProjectImages().enqueue(new Callback<OnGoingResponse>() {
                @Override
                public void onResponse(Call<OnGoingResponse> call, Response<OnGoingResponse> response) {
                    // Got data. Send it to adapter
                    progressBar.setVisibility(View.GONE);
                    if(response.code()==200){
                        onGoingResponse = fetchOnGoingResultImages(response);
                        onGoingImagesModels = onGoingResponse.getOnGoingImages();
                        insertOnGoingImages();

                    }else{
                        Toast.makeText(getApplicationContext(),"Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<OnGoingResponse> call, Throwable t) {
                    t.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                    // TODO: 08/11/16 handle failure
                    Toast.makeText(getApplicationContext(),t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        private OnGoingResponse fetchOnGoingResultImages(Response<OnGoingResponse> response) {
            OnGoingResponse materialResponse = response.body();
            return materialResponse;
        }
        private Call<OnGoingResponse> GetOnGoingProjectImages() {
            return apiServices.getOnGoingProjectImages();
        }

        private void insertOnGoingImages(){
            query.openTransaction();
            for (int i = 0; i < onGoingImagesModels.size(); i++) {
                ProjectOnGoingImagesModel on=onGoingImagesModels.get(i);
                ContentValues values = new ContentValues();
                values.put(ProjectOnGoingImagesModel.XML_ROWID,on.getRowID());
                values.put(ProjectOnGoingImagesModel.XML_PROJECTID,on.getProjectID());
                values.put(ProjectOnGoingImagesModel.XML_IMAGEURL,on.getImageUrl());
                values.put(ProjectOnGoingImagesModel.XML_IMAGEDESCRIPTION,on.getImageDescription());
                values.put(ProjectOnGoingImagesModel.XML_CREATEDDATE,on.getCreatedDate());
                query.insertDataSQL(ProjectOnGoingImagesModel.TABLE_NAME,values);
            }
            query.commitTransaction();
            query.closeTransaction();
            GetDataOnGoingProgress();
        }

    /* PROGRESS */
        private void GetDataOnGoingProgress(){
            if (progressBar.getVisibility() == View.GONE) {
                progressBar.setVisibility(View.VISIBLE);
            }
            Log.d(TAGACTIVITY, "LOADPAGE: ");

            GetOnGoingProgress().enqueue(new Callback<OnGoingResponse>() {
                @Override
                public void onResponse(Call<OnGoingResponse> call, Response<OnGoingResponse> response) {
                    // Got data. Send it to adapter
                    progressBar.setVisibility(View.GONE);
                    if(response.code()==200){
                        onGoingResponse = fetchProgres(response);
                        onGoingProgressModels = onGoingResponse.getOnGoingProgress();
                        insertOnGoingProgress();

                    }else{
                        Toast.makeText(getApplicationContext(),"Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<OnGoingResponse> call, Throwable t) {
                    t.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                    // TODO: 08/11/16 handle failure
                    Toast.makeText(getApplicationContext(),t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        private OnGoingResponse fetchProgres(Response<OnGoingResponse> response) {
            OnGoingResponse progressField = response.body();
            return progressField;
        }
        private Call<OnGoingResponse> GetOnGoingProgress() {
            return apiServices.getOnGoingProjectProgress();
        }

        private void insertOnGoingProgress(){
            query.openTransaction();
            for (int i = 0; i < onGoingProgressModels.size(); i++) {
                ProjectOnGoingProgressModel onProgress=onGoingProgressModels.get(i);
                ContentValues values = new ContentValues();
                values.put(ProjectOnGoingProgressModel.XML_ROWID,onProgress.getRowID());
                values.put(ProjectOnGoingProgressModel.XML_PROJECTID,onProgress.getProjectID());
                values.put(ProjectOnGoingProgressModel.XML_DESCRIPTION,onProgress.getDescription());
                values.put(ProjectOnGoingProgressModel.XML_BOBOT,onProgress.getBobot());
                values.put(ProjectOnGoingProgressModel.XML_SCHEDULE,onProgress.getSchedule());
                values.put(ProjectOnGoingProgressModel.XML_ACTUALWEEK84,onProgress.getActualWeek84());
                values.put(ProjectOnGoingProgressModel.XML_DEVIASI,onProgress.getDeviasi());
                values.put(ProjectOnGoingProgressModel.XML_CREATEDDATE,onProgress.getCreatedDate());
                query.insertDataSQL(onProgress.TABLE_NAME,values);
            }
            query.commitTransaction();
            query.closeTransaction();
            searchFragment.setVisibility(View.GONE);
            //linearHome.setVisibility(View.GONE);
//            CARIDATAWithBA();
            toSearch();
        }



    @Override
    public void onFragmentInteractionOnGoing(Uri uri, Bundle bundle) {
        Bundle b = bundle;

//        if(b!=null){
//            String projectID = b.getString("projectID","");
//            String url = b.getString("url","");
//            int rowID = b.getInt("rowid",0);
//            bundle.putString("params1",projectID);
//            bundle.putString("params2",url);
//            bundle.putInt("rowid",rowID);
//            FragmentManager fm = getSupportFragmentManager();
//            DialogOnGoing alertDialog = DialogOnGoing.newInstance(bundle);
//            alertDialog.show(fm, "fragment_alert");
//            Toast.makeText(this, projectID, Toast.LENGTH_SHORT).show();
//        }
        linearHome.setVisibility(View.VISIBLE);
        linearImage.setVisibility(View.GONE);
        linearProgress.setVisibility(View.GONE);
    }

    @Override
    public void onFragmentInteractionDialogSingle(Uri uri, Bundle bundle) {
        Bundle b = bundle;

        if(b!=null){
            String projectID = b.getString("projectID","");
            Toast.makeText(this, projectID, Toast.LENGTH_SHORT).show();
        }
    }

    private void CARIDATAWithBA(String PID){
        query.openTransaction();
        if(onGoingImagesModels.size()>0 || onGoingImagesModels!=null){
            onGoingImagesModels.clear();
        }
        String GroupCompany = new SharePreference(ProjectOnGoingActivity.this).isFormGroupCompany();
        String PT = new SharePreference(ProjectOnGoingActivity.this).isFormCompany();
        String Divisi = new SharePreference(ProjectOnGoingActivity.this).isFormDIVISI();
        String BA = new SharePreference(ProjectOnGoingActivity.this).isBA();
        String PROJECTS = new SharePreference(ProjectOnGoingActivity.this).isProject();
        List<Object> listImageObject = null;
        String[] a = new String[3];
        a[0] = GroupCompany;
        a[1] = BA;
        a[2] = PID;
        String sqldb_query = "SELECT * FROM PURCHASE_ONGOING_IMAGES where PROJECTID IN (select PROJECTID from PURCHASE_ONGOING where COMPANYGROUP=? AND " +
                "BA = ? and PROJECTID=?)";
        listImageObject = query.getListDataRawQuery(sqldb_query, ProjectOnGoingImagesModel.TABLE_NAME,a);
        query.closeTransaction();
        onGoingImagesModels.clear();
        if(listImageObject.size() > 0){
            for(int i = 0; i < listImageObject.size(); i++){
                ProjectOnGoingImagesModel blk = (ProjectOnGoingImagesModel) listImageObject.get(i);
                onGoingImagesModels.add(blk);
                fragments=getFragments(onGoingImagesModels);
            }
            adapter=new OnGoingSlideAdapter(getSupportFragmentManager(),fragments);
            pager.setAdapter(adapter);
            adapter.updatePAger(fragments);
            Toast.makeText(ProjectOnGoingActivity.this, "ada", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(ProjectOnGoingActivity.this, " DATA TIDAK ADA ", Toast.LENGTH_SHORT).show();
        }
    }

    private List<Fragment> getFragments(List<ProjectOnGoingImagesModel>projectOnGoingImagesModels) {
        List<Fragment> fList = new ArrayList<Fragment>();
        Bundle bundle = new Bundle();
        for (int i = 0; i < projectOnGoingImagesModels.size(); i++) {
            String url = projectOnGoingImagesModels.get(i).getImageUrl();
            String projectID = projectOnGoingImagesModels.get(i).getProjectID();
            bundle.putString("id", projectID);
            bundle.putString("url", url);
            FragmentOnGoingProject fragobj = new FragmentOnGoingProject();
            fragobj.setArguments(bundle);
            fList.add(fragobj);
        }
        return fList;
    }

    @Override
    public void onClick(View v) {
        if(v==imgProgress){
            Toast.makeText(this, "Progress", Toast.LENGTH_SHORT).show();
            linearProgress.setVisibility(View.VISIBLE);
            linearImage.setVisibility(View.GONE);
            linearHome.setVisibility(View.GONE);
        }
        if(v==imgGallery){
            CARIDATAWithBA(ProjectsID);
            linearHome.setVisibility(View.GONE);
            linearImage.setVisibility(View.VISIBLE);
            linearProgress.setVisibility(View.GONE);
        }
        if(v==imgClose){
            linearHome.setVisibility(View.VISIBLE);
            linearImage.setVisibility(View.GONE);
            linearProgress.setVisibility(View.GONE);
        }
    }
}
